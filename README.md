### What is this repository for? ###

* Rigapollo: Pairwise SVM-dependent pairwise HMM for protein alignment
* version 0.0 

### overview ###

Rigapollo is a SVM-dependent pairwise HMM framework. Here we propose an implementation of its application to protein alignments. The tool takes as input two MSA generated with HMMer, build the features vectors (that include backbone dynamics, secondary structure propensities, evolutionary information and chemical-physical characteristics of the amino acids) and computes the pairwise Viterbi. 

### How do I get set up? ###

dependences (Everything can be installed with pip):
python2.7
scikit-learn
numpy
scipy

### guidelines ###
Rigapollo needs two trimmed MSAs to work. You can find an example of the format in the "input_example" folder (the first line represent the input sequence, the others the homologs retrieved by HMMer).
to run the example, run rigapollo.py input_example/s1.hmmer input_example/s2.hmmer

### Troubleshooting ###
- The tool is meant to work on linux systems. 
- If you get internal errors, please try to update your scikit-learn library to version 0.18

### Who do I talk to? ###

if you have any question about the code, write me an email at orlando.gabriele89@gmail.com