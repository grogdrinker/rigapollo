#!/usr/bin/env python2
# -*- coding: utf-8 -*-
from propensity import *
import numpy
def norm_prop(propertyTable):
	normal={}
	for i in propertyTable.keys():
		normal[i]=[]
	for j in range(len(propertyTable['A'])): #for each feature
		maxim=-float('inf')
		minim=float('inf')
		for i in propertyTable.keys():
			if propertyTable[i][j]>maxim:
				maxim=propertyTable[i][j]
			if propertyTable[i][j]<minim:
				minim=propertyTable[i][j]
		for i in propertyTable.keys():
			normal[i]+=[(propertyTable[i][j]-minim)/(maxim-minim)]
	return normal
def compare(v1,v2):
	vf=[]
	COMP='diff'
	#print v1,v2
	if COMP=='diff':
		
		if '-' in v1:
			vf=[]
			for i in v2:
				if type(i)!=str:
					vf+=[i]
			return vf
		elif '-' in v2:
			vf=[]
			for i in v1:
				if type(i)!=str:
					vf+=[i]
			return vf
		else:
		
			for i in range(len(v1)):
				
				if type(v1[i])!=str:
					vf+=[abs(v1[i]-v2[i])]
				
				else:
					if blosum.has_key((v1[i].upper(),v2[i].upper())):
						
						vf+=[blosum[(v1[i].upper(),v2[i].upper())]]
					else:
						
						vf+=[blosum[(v2[i].upper(),v1[i].upper())]]
	if COMP=='vicini':
		if '-' in v1:
			vf=[]
			for i in v2:
				if type(i)!=str:
					vf+=[i]
			return vf
		elif '-' in v2:
			vf=[]
			for i in v1:
				if type(i)!=str:
					vf+=[i]
			return vf
		else:
		
			for i in range(len(v1)):
				
				if type(v1[i])!=str:
					vf+=[v1[i]-v2[i]]
				
				else:
					if blosum.has_key((v1[i].upper(),v2[i].upper())):
						
						vf+=[blosum[(v1[i].upper(),v2[i].upper())]]
					else:
						
						vf+=[blosum[(v2[i].upper(),v1[i].upper())]]
	
	return vf
	
def vettore(aln_seq,position,typ,sw,nomeseq=None):#i take the aligned seq and the position in order to build the vector with sliding window values too
	
	fin=[]
	posEffettiva=0
	#propertyTable=norm_prop(pr)
	#propertyTable=pr
	i=0
	while i<position:
		if aln_seq[i]!='-' and aln_seq[i]!='.':
		
			posEffettiva+=1
		i+=1
	seq=aln_seq.replace('-','').replace('.','')
	if 'amino_blos' in typ.split(','):
		for i in range(-sw2,sw2+1):
			if posEffettiva+i<0:
				fin+=['A']
				
			elif posEffettiva+i>=len(seq):
				fin+=['A']
			else:
				fin+=[seq[i+posEffettiva]]
		
	if 'prop' in typ.split(','):
		nfeatures=len(propertyTable['A'])
		for i in range(-sw,sw+1):
			if posEffettiva+i<0:
				fin+=[0]*nfeatures
			elif posEffettiva+i>=len(seq):
				fin+=[0]*nfeatures
			else:
				fin+=propertyTable[seq[posEffettiva+i].upper()]

	if 'aa' in typ.split(','):
		nfeatures=20
		for i in range(-sw,sw+1):
			if posEffettiva+i<0:
				fin+=[0]*nfeatures
			elif posEffettiva+i>=len(seq):
				fin+=[0]*nfeatures
			else:
				fin+=aminoacids[seq[posEffettiva+i]]
	if 'compressed' in typ.split(','):
		nfeatures=6
		for i in range(-sw,sw+1):
			if posEffettiva+i<0:
				fin+=[0]*nfeatures
			elif posEffettiva+i>=len(seq):
				fin+=[0]*nfeatures
			else:
				fin+=compressed[seq[posEffettiva+i]]
	if 'plosone_ind' in typ.split(','):
		nfeatures=16
		for i in range(-sw,sw+1):
			if posEffettiva+i<0:
				fin+=[0]*nfeatures
			elif posEffettiva+i>=len(seq):
				fin+=[0]*nfeatures
			else:
				fin+=plosone_dict[seq[posEffettiva+i]]
	if 'hidrophob1' in typ.split(','):
		nfeatures=1
		for i in range(-sw,sw+1):
			if posEffettiva+i<0:
				fin+=[1]*nfeatures
			elif posEffettiva+i>=len(seq):
				fin+=[1]*nfeatures
			else:
				fin+=[hidrophobicity1[seq[posEffettiva+i]]]
	if 'hidrophob_rose' in typ.split(','):
		nfeatures=1
		for i in range(-sw,sw+1):
			if posEffettiva+i<0:
				fin+=[0.7]*nfeatures
			elif posEffettiva+i>=len(seq):
				fin+=[0.7]*nfeatures
			else:
				fin+=[hidrophob_rose[seq[posEffettiva+i]]]
	if 'hidrophob_kyte' in typ.split(','):
		nfeatures=1
		for i in range(-sw,sw+1):
			if posEffettiva+i<0:
				fin+=[1]*nfeatures
			elif posEffettiva+i>=len(seq):
				fin+=[1]*nfeatures
			else:
				fin+=[hidrophob_rose[seq[posEffettiva+i]]]
	return fin
def buildVector(seq,TYPE,sw,args={}):
	vet=[]
	last=0
	seq_nogap=seq.replace('-','')
	for i in range(len(seq)):
		vet+=[[]]
	from dynaMine.predictor.wrappers import RunDynaMinePrediction
	nfeatures=0	
	if 'dyna_coil' in TYPE.split(','):
		D = RunDynaMinePrediction()
		v_dyna=D.runDynaMineSeqs(seq_nogap,'2',dataType='coil')
		effett=0
		#v_dyna=numpy.gradient(numpy.array(v_dyna))
		
		for i in range(len(vet)):
			if seq[i]!='-':
				#print i
				for s in range(-sw,sw+1):
					if effett+s<0:
						vet[i]+=[0]
					elif effett+s>=len(seq_nogap):
						vet[i]+=[0]
					else:
						vet[i]+=[v_dyna[effett+s]]
				effett+=1
				last=i
	if 'psipred_single' in TYPE.split(','):
		
		os.system('/home/gabriele/HD1/promals/psipred/runpsipred_single -b')
		#v_dyna=numpy.gradient(numpy.array(v_dyna))
		
		for i in range(len(vet)):
			if seq[i]!='-':
				#print i
				for s in range(-sw,sw+1):
					if effett+s<0:
						vet[i]+=[0]
					elif effett+s>=len(seq_nogap):
						vet[i]+=[0]
					else:
						vet[i]+=[v_dyna[effett+s]]
				effett+=1
				last=i
	if 'dyna_coil_grad' in TYPE.split(','):
		D = RunDynaMinePrediction()
		v_dyna=D.runDynaMineSeqs(seq_nogap,'2',dataType='coil')
		effett=0
		v_dyna=numpy.gradient(numpy.array(v_dyna))
		
		for i in range(len(vet)):
			if seq[i]!='-':
				#print i
				for s in range(-sw,sw+1):
					if effett+s<0:
						vet[i]+=[0]
					elif effett+s>=len(seq_nogap):
						vet[i]+=[0]
					else:
						vet[i]+=[v_dyna[effett+s]]
				effett+=1
				last=i	
	if 'dyna_helix_grad' in TYPE.split(','):
		D = RunDynaMinePrediction()
		v_dyna=D.runDynaMineSeqs(seq_nogap,'2',dataType='helix')
		effett=0
		v_dyna=numpy.gradient(numpy.array(v_dyna))
		
		for i in range(len(vet)):
			if seq[i]!='-':
				#print i
				for s in range(-sw,sw+1):
					if effett+s<0:
						vet[i]+=[0]
					elif effett+s>=len(seq_nogap):
						vet[i]+=[0]
					else:
						vet[i]+=[v_dyna[effett+s]]
				effett+=1
				last=i	
	if 'dyna_helix' in TYPE.split(','):
		D = RunDynaMinePrediction()
		v_dyna=D.runDynaMineSeqs(seq_nogap,'2',dataType='helix')
		effett=0
		#v_dyna=numpy.gradient(numpy.array(v_dyna))
		
		for i in range(len(vet)):
			if seq[i]!='-':
				#print i
				for s in range(-sw,sw+1):
					if effett+s<0:
						vet[i]+=[0]
					elif effett+s>=len(seq_nogap):
						vet[i]+=[0]
					else:
						vet[i]+=[v_dyna[effett+s]]
				effett+=1
				last=i	
	if 'dyna_sheet' in TYPE.split(','):
		D = RunDynaMinePrediction()
		v_dyna=D.runDynaMineSeqs(seq_nogap,'2',dataType='sheet')
		effett=0
		#v_dyna=numpy.gradient(numpy.array(v_dyna))
		
		for i in range(len(vet)):
			if seq[i]!='-':
				#print i
				for s in range(-sw,sw+1):
					if effett+s<0:
						vet[i]+=[0]
					elif effett+s>=len(seq_nogap):
						vet[i]+=[0]
					else:
						vet[i]+=[v_dyna[effett+s]]
				effett+=1
				last=i	
	if 'dyna_sheet_grad' in TYPE.split(','):
		D = RunDynaMinePrediction()
		v_dyna=D.runDynaMineSeqs(seq_nogap,'2',dataType='sheet')
		effett=0
		v_dyna=numpy.gradient(numpy.array(v_dyna))
		
		for i in range(len(vet)):
			if seq[i]!='-':
				#print i
				for s in range(-sw,sw+1):
					if effett+s<0:
						vet[i]+=[0]
					elif effett+s>=len(seq_nogap):
						vet[i]+=[0]
					else:
						vet[i]+=[v_dyna[effett+s]]
				effett+=1
				last=i
	if 'dyna_side_grad' in TYPE.split(','):
		D = RunDynaMinePrediction()
		v_dyna=D.runDynaMineSeqs(seq_nogap,'2',dataType='sidechain')
		effett=0
		v_dyna=numpy.gradient(numpy.array(v_dyna))
		
		for i in range(len(vet)):
			if seq[i]!='-':
				#print i
				for s in range(-sw,sw+1):
					if effett+s<0:
						vet[i]+=[0]
					elif effett+s>=len(seq_nogap):
						vet[i]+=[0]
					else:
						vet[i]+=[v_dyna[effett+s]]
				effett+=1
				last=i	
	if 'dyna_side' in TYPE.split(','):
		D = RunDynaMinePrediction()
		v_dyna=D.runDynaMineSeqs(seq_nogap,'2',dataType='sidechain')
		effett=0
		#v_dyna=numpy.gradient(numpy.array(v_dyna))
		
		for i in range(len(vet)):
			if seq[i]!='-':
				#print i
				for s in range(-sw,sw+1):
					if effett+s<0:
						vet[i]+=[0]
					elif effett+s>=len(seq_nogap):
						vet[i]+=[0]
					else:
						vet[i]+=[v_dyna[effett+s]]
				effett+=1
				last=i	
	if 'dyna_back' in TYPE.split(','):
		D = RunDynaMinePrediction()
		v_dyna=D.runDynaMineSeqs(seq_nogap,'2',dataType='backbone')
		effett=0
		#v_dyna=numpy.gradient(numpy.array(v_dyna))
		
		for i in range(len(vet)):
			if seq[i]!='-':
				#print i
				for s in range(-sw,sw+1):
					if effett+s<0:
						vet[i]+=[0]
					elif effett+s>=len(seq_nogap):
						vet[i]+=[0]
					else:
						vet[i]+=[v_dyna[effett+s]]
				effett+=1
				last=i	
	if 'dyna_coil_grad' in TYPE.split(','):
		D = RunDynaMinePrediction()
		v_dyna=D.runDynaMineSeqs(seq_nogap,'2',dataType='coil')
		effett=0
		v_dyna=numpy.gradient(numpy.array(v_dyna))
		
		for i in range(len(vet)):
			if seq[i]!='-':
				#print i
				for s in range(-sw,sw+1):
					if effett+s<0:
						vet[i]+=[0]
					elif effett+s>=len(seq_nogap):
						vet[i]+=[0]
					else:
						vet[i]+=[v_dyna[effett+s]]
				effett+=1
				last=i	
	if 'dyna_back_grad' in TYPE.split(','):
		D = RunDynaMinePrediction()
		v_dyna=D.runDynaMineSeqs(seq_nogap,'2',dataType='backbone')
		effett=0
		v_dyna=numpy.gradient(numpy.array(v_dyna))
		
		for i in range(len(vet)):
			if seq[i]!='-':
				#print i
				for s in range(-sw,sw+1):
					if effett+s<0:
						vet[i]+=[0]
					elif effett+s>=len(seq_nogap):
						vet[i]+=[0]
					else:
						vet[i]+=[v_dyna[effett+s]]
				effett+=1
				last=i	
	if 'pssm' in TYPE.split(','):
		f=open(args['align_file'])
		ami=[0.0]*20
		pssm=[]
		for i in seq_nogap:
			pssm+=[ami[:]]
		#print numpy.array(pssm).shape
		num=[0.0]*len(seq_nogap)
		aln1=f.readlines()
		aln=[]
		for i in range(len(aln1)):
			aln+=[list(aln1[i].strip('\n'))]
		aln=numpy.array(aln)
		ef=0
		for i in range(len(aln[0])):
			for j in aln[:,i]:
				if j!='-':
					if amino_index.has_key(j):
						pssm[ef]
						amino_index[j]
						pssm[ef][amino_index[j]]+=1					
						num[ef]+=1
			ef+=1
		'''
		for i in f:
			
		
			a=i.strip('\n')#.replace('-','')
			for j in range(len(a)):
				if a[j]!='-':
					#print amino_index[a[j]]
					pssm[j][amino_index[a[j]]]+=1.0
					num[j]+=1.0
		'''			
		
		for i in range(len(pssm)):
			for j in range(len(pssm[i])):
				if num[j]>0:
					pssm[i][j]=pssm[i][j]/num[i]	
				else:
					pssm[i][j]=0
		effett=0	
		for i in range(len(vet)):
			if seq[i]!='-':
				for s in range(-sw,sw+1):
					if effett+s<0:
						vet[i]+=[0]*20
					elif effett+s>=len(seq_nogap):
						vet[i]+=[0]*20
					else:
						vet[i]+=pssm[effett+s]
				effett+=1
				last=i
	if 'entropy' in TYPE.split(','):
		import math
		#from math import log2
		assert nomeseq!=None
		#jh_eval0.0001_.hmmer
		#f=open(DIR_ALN+nomeseq+'fa.hhE0.0001.a3m.hh')
		#alns=f.readlines
		f=open(DIR_ALN+nomeseq+'_jh_eval0.0001_.hmmer')
		ami=[0.0]*20
		pssm=[]
		for i in seq.replace('-',''):
			pssm+=[ami[:]]
		#print numpy.array(pssm).shape
		num=[0.0]*len(seq)
		
		aln1=f.readlines()
		aln1[0]=aln1[0].replace('O-ID\n','')
		
		aln=[]
		for i in range(len(aln1)):
			aln+=[list(aln1[i][0:len(aln1[0])].strip('\n'))]
		aln=numpy.array(aln)
		ef=0
		
		for i in range(len(aln[0])):
			if aln[0][i]=='-':
				continue
			
			for j in aln[:,i]:
				if j!='-':
					if amino_index.has_key(j):
						
						pssm[ef][amino_index[j]]+=1
					#print pssm[ef][amino_index[j]]
						num[ef]+=1
			ef+=1
		entropy=[]
		for i in range(len(pssm)):
			for j in range(len(pssm[i])):
				pssm[i][j]=float(pssm[i][j])/float(num[i])
		for i in pssm:
			en=0
			for j in i:
				if j>0:
					
					en+=j*math.log(j,2)
				else:
					j=0.000000000000000001
					en+=j*math.log(j,2)
			#print en		
			entropy+=[-en]
		effett=0	
		for i in range(len(vet)):
			if seq[i]!='-':
				for s in range(-sw,sw+1):
					if effett+s<0:
						vet[i]+=[0]*20
					elif effett+s>=len(seq_nogap):
						vet[i]+=[0]*20
					else:
						vet[i]+=[entropy[effett+s]]
				effett+=1
				last=i
	nfeatures=len(vet[last])+len(vettore(seq_nogap,0,TYPE,sw))
	for i in range(len(seq)):
		if seq[i]=='-' or seq[i]=='.':
			vet[i]+=['-']*nfeatures
		else:
			vet[i]+=vettore(seq,i,TYPE,sw)
	return vet
def fai_vettore_pairwise(s1,s2):
	v1=fai_vettore_tuo(s1,'aa')
	v2=fai_vettore_tuo(s2,'aa')
	fin=[]
	for i in range(len(v1)):
		fin+=[compare(v1[i],v2[i])]
	return fin
		
if __name__ == '__main__':
	#print sliding_w([[0,2],[3,4],[6,6]])#[[1,2],[1,3],[1,2]]
	#TYPE='prop'
	#k = RunDynaMinePrediction()
	#print k.runDynaMineSeqs('RRLMTDRSVGNCIACHEVTEMADAQFPGTVGPS---------------LDGVAARYP-----EAMIRGILVNSKNV----FPETVMPAYYRVEGFNRPGIAFTSKPIEGEIRPLMTAGQIEDVVAYLMTLT'.replace('-',''),'1')[0][40]
	#print  fai_vettore_tuo('NLFAAAAACCCCCCCCAAAAAA','1abo_A')[0]
	#s1='AAAAA'#'MSSTQFNKGPSYGLSAEVKNRLLSKYDPQKEAELRTWIEGLTGLSIGPDFQKGLKDGTILCTLMNKLQPGSVPKINRSMQNWHQLENLSNFIKAMVSYGMNPVDLFEANDLFESGNMTQVQVSLLALAGKAKTKGLQSGVDIGVKYSEKQERNFDDATMKAGQCVIGLQMGTNKCASQSGMTAYGTRRHLYDPKNHILPPMDHSTISLQMGTNKCASQVGMTAPGTRRHIYDTKLGTDKCDNSSMSLQMGYTQGANQSGQVFGLGRQIYDPKYCPQGTVADGAPSGTGDCPDPGEVPEYPPYYQEEAGY'
	for i in range(100):
		s2='DSLVLYNRVAVQGDVVRELKAKKAPKEDVDAAVKQLLSLKAEYKEKTGQEYKPGNPPDSLVLYNRVAVQGDVVRELKAKKAPKEDVDAAVKQLLSLKAEYKEKTGQEYKPGNPPDSLVLYNRVAVQGDVVRELKAKKAPKEDVDAAVKQLLSLKAEYKEKTGQEYKPGNPPDSLVLYNRVAVQGDVVRELKAKKAPKEDVDAAVKQLLSLKAEYKEKTGQEYKPGNPPDSLVLYNRVAVQGDVVRELKAKKAPKEDVDAAVKQLLSLKAEYKEKTGQEYKPGNPP'
		a=fai_vettore_tuo(s2,'dyna_back',0,nomeseq='d1ail__-d1fyja_2')
	print len(a[0])#a#=numpy.array(a)
	s=0.0
	#for i in a:
	#	print i
		
	#print a[0]
	#print fai_vettore_pairwise(s1,s2)
	#print sliding_w('AAACCY')
