#
#
# Note: this file is a modified copy from pdbe/analysis/Graphs.py from the internal EBI repository.
#
# The functions below are pulled from other files and included here.
#

try:
  import rpy2.rpy_classic as rpy
  rpy.set_default_mode(rpy.BASIC_CONVERSION)
except:
  import rpy

# From pdbe/analysis/Stats.py in internal EBI repository
def setOutlierLimits(valueList,outlierRange = 3.0):

  #from rpy import r

  bp = rpy.r.boxplot(valueList,range = outlierRange, plot = False)

  return (bp['stats'][0][0],bp['stats'][-1][0])

def getSplineStats(valueList,smoothed=False,runSecondDerivative=False):

  r = rpy.r
  
  xdata = range(len(valueList))
  ydata = valueList
  
  firstDerivatives = []
  secondDerivatives = []
   
  # Normal spline
  if not smoothed:
    spline = r.spline(xdata,ydata)
    spf = r.splinefun(xdata,ydata)
    
    firstDerivFunc = lambda x: spf(x,deriv=1)
    secondDerivFunc = lambda x: spf(x,deriv=2)
    
  # Smoothing spline
  else:  
    #rpy.set_default_mode(rpy.NO_CONVERSION)
    smoothSpline = r.get('smooth.spline')
    #rpy.set_default_mode(rpy.BASIC_CONVERSION)

    spline = smoothSpline(xdata,ydata)  # Lots of options here, using default

    firstDerivFunc = lambda x: r.predict(spline,x,deriv=1)['y']
    secondDerivFunc = lambda x: r.predict(spline,x,deriv=2)['y']

  for x in xdata:
    firstDerivatives.append(firstDerivFunc(x))
    
  if runSecondDerivative:
    secondDerivatives.append(secondDerivFunc(x))

  return (firstDerivatives,secondDerivatives)

# From memops/universal/Util.py in CCPN SourceForge repository
def frange(start, end=None, inc=None):

  "A simple range function that accepts float increments. Taken from ASPN: http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/66472"

  if end == None:
    end = start + 0.0
    start = 0.0

  if inc == None:
    inc = 1.0

  L = []

  while 1:
    next = start + len(L) * inc
    if inc > 0 and next >= end:
      break
    elif inc < 0 and next <= end:
      break
    L.append(next)

  return L

#
# Below is code from the original Graphs.py file, excluding imports for above functions
#

import random

#
# Functions to plot graphs. Based on R (could shift this to other package, like biggles).
#

def getColourList(colourListType,numEntries):

  #from rpy import r

  if colourListType == 'standard':
    colourList = ['red','blue','green','purple','brown','orange','black','pink2','seagreen','wheat','olivedrab','navy','grey16','grey49','grey85','cyan2','maroon1','aquamarine','darkgoldenrod2','chartreuse']
    colourList = colourList[:numEntries]

  elif colourListType == 'spectral':

    rgbList = (
        (158,1,66),
        (213,62,79),
        (244,109,67),
        (253,174,97),
        (254,224,139),
        (255,255,191),
        (230,245,152),
        (171,221,164),
        (102,194,165),
        (50,136,189),
        (94,79,162)
    )

    colourList = [rpy.r.rgb(rgbList[0][0],rgbList[0][1],rgbList[0][2],maxColorValue=255)]

    if numEntries > 2:
      floatStep = 9.0 / (numEntries - 2)
      for i in frange(1,numEntries+floatStep,floatStep):
        index = int(i + 0.5)
        colourList.append(rpy.r.rgb(rgbList[index][0],rgbList[index][1],rgbList[index][2],maxColorValue=255))

    colourList.append(rpy.r.rgb(rgbList[-1][0],rgbList[-1][1],rgbList[-1][2],maxColorValue=255))

  return colourList

def plotGraph(fileName,
              list1,
              list2,
              main = 'Graph',
              xlab = '',
              ylab = '',
              xlim = None,
              ylim = None,
              yErrorTop = None,
              yErrorBottom = None,
              colour='black',
              errorColour = 'red',
              connectPoints=False,
              connectPointsLineWidth=1,
              fileType = 'png',
              resolution = 200,
              correlate = True,
              addRegressionLine = False,
              diagonal = False,
              pointSize = 'normal',
              xRandomize = None,
              yRandomize = None,
              randomizeZero=False,
              plotDensityLists = None,
              plotDensityColours = ('purple','red'),
              plotDensityStep = 0.5,
              plotDensityAll = False,
              plotDensityTexts = None,
              xRangeForYValue = None,
              plotXRangeForYValue = True, # Only activated if xRangeForYValue given!
              plotKeywds = None,
              titleKeywds = None,
              width = None,
              height = None

              ):

  #from rpy import r

  #
  # Set output type
  #

  outputKeywds = {}
  if width:
    outputKeywds['width'] = width
  if height:
    outputKeywds['height'] = height

  if fileType == 'ps':
    rpy.r.postscript(fileName, paper = 'A4', **outputKeywds)
  elif fileType == 'png':
    rpy.r.bitmap(fileName, res=resolution, **outputKeywds)

  #
  # Set some parameters for the plot (point type, ...)
  # See rss.acs.unt.edu/Rdoc/library/graphics/html/parpy.r.html for more info!
  #

  #rpy.r.par(family = 'HersheySans')
  #rpy.r.par(font = 2)

  # Set the point size in graph. Note: can also use cex (further down) to control this.
  if pointSize == 'normal':
    pch = 20
  elif pointSize == 'small':
    pch = '.'

  numPoints = len(list1)

  rpy.r.par(adj = 0.5, pch = pch)

  #
  # Set specific keywords for plot
  #

  if not plotKeywds:
    plotKeywds = {}

  if not xlim:
    summary = rpy.r.summary(list1)
    xMin = summary['Min.']
    xMax = summary['Max.']
    xlim = (xMin,xMax)
  else:
    (xMin,xMax) = xlim

  plotKeywds['xlim'] = xlim

  if not ylim:
    if not yErrorTop:
      summary = rpy.r.summary(list2)
    else:
      summary = rpy.r.summary(yErrorTop + yErrorBottom)

    yMin = summary['Min.']
    yMax = summary['Max.']
    ylim = (yMin,yMax)
  else:
    (yMin,yMax) = ylim

  plotKeywds['ylim'] = ylim

  if xRangeForYValue:
    yBinMax = ylim[-1]
    yBinMin = ylim[0]

    yBin = yBinMax / (xRangeForYValue - 1)

    yBinsForXRange = {}
    for i in range(xRangeForYValue):
      yBinsForXRange[i] = []

  #
  # Make sure title keywords set up
  #

  if not titleKeywds:
    titleKeywds = {}

  #
  # Do a standard linear correlation if required
  #

  subText = ""

  if correlate:
    # Spearman is more flexible
    # Pearson is 'traditional'
    subTexts = []
    for correlationMethod in ('spearman','pearson'):
      p = rpy.r.cor(list1,list2,method = correlationMethod)
      subTexts.append("%s: %.3f" % (correlationMethod.capitalize(),p))
    subText = "Correlations: %s" % ', '.join(subTexts)

  #
  # Get the information for the density plots, if required
  #

  if plotDensityTexts and plotDensityLists:

    densityColour = 'blue'

    # Print info on scaling!
    numPoints = len(list1)

    if subText:
      subText += ', density for'
    else:
      subText += 'Density for'

    if plotDensityAll:
      subText += ' all (%s),' % densityColour

    for subIndex in range(len(plotDensityLists)):
      if plotDensityLists[subIndex]:
        numSubPoints = len(plotDensityLists[subIndex])
        factor = numSubPoints * 1.0 / numPoints

        subText += " %s (%s, scale %.2f)," % (plotDensityTexts[subIndex],plotDensityColours[subIndex], 1 / factor)

    subText = subText[:-1] + '.'

  #
  # Plot the graph
  #
  # Note: rpy.r.plot.window() will plot to screen!
  #

  rpy.r.plot([],[],type='p', ann=False, **plotKeywds)

  rpy.r.title(main=main, sub=subText, xlab=xlab, ylab=ylab, **titleKeywds)

  for i in range(numPoints):

    x = list1[i]
    y = list2[i]

    #
    # Note: only for float numbers! Randomize is a float value, will be randomized as (-value,+value).
    # Values below randomize limit are not randomized (this is to prevent zero from being randomized)
    #

    if xRandomize and (x > xRandomize or randomizeZero):
      x += random.uniform(-xRandomize,xRandomize)
    if yRandomize and (y > yRandomize or randomizeZero):
      y += random.uniform(-yRandomize,yRandomize)

    rpy.r.points(x,y,cex = 1,col=colour)

    if xRangeForYValue and y < yBinMax:
      i = int(y / yBin)
      yBinsForXRange[i].append(x)

    # Note: connectPoints does NOT work with randomize!
    if connectPoints and i + 1 < numPoints:
      rpy.r.segments(list1[i], list2[i], list1[i+1], list2[i+1],col=colour, lwd=connectPointsLineWidth)

  #
  # Plot error bars if required
  #

  if yErrorTop and yErrorBottom:
    errorLineWidth = 0.5
    errorBarWidth = 0.02

    xw = errorBarWidth / 2.0

    for i in range(len(list1)):
      x = list1[i]

      rpy.r.lines((x,x),(yErrorTop[i],yErrorBottom[i]),lwd=errorLineWidth,col=errorColour)
      rpy.r.lines((x - xw,x+xw),(yErrorTop[i],yErrorTop[i]),lwd=errorLineWidth,col=errorColour)
      rpy.r.lines((x - xw,x+xw),(yErrorBottom[i],yErrorBottom[i]),lwd=errorLineWidth,col=errorColour)

  #
  # Plot diagonal line if required
  #

  if diagonal:
    rpy.r.abline(0,1)

  #
  # Add a linear regression line, based on least squares
  #

  if addRegressionLine:
    coeffData = rpy.r.lm(r("y ~ x"), data = rpy.r.data_frame(x=list1, y=list2))['coefficients']
    gradient = coeffData['x']
    yintercept = coeffData['(Intercept)']

    rpy.r.abline(a=yintercept, b=gradient, col="red")
    rpy.r.text(xlim[1],ylim[0],"a=%.3f,b=%.3f" % (gradient,yintercept),pos=2,col='red')

  # Quick hack to plot customised lines
  #rpy.r.abline(0,0.33333, col='red')
  #rpy.r.abline(0,-0.33333, col='red')

  #
  # Plot density - is based on input list!!!
  #

  if plotDensityLists:

    # Also do for exposure! Do this first, should be buried underneath other plots.
    yPlotDensityStep = 0.5
    limits = frange(yMin-yPlotDensityStep-yRandomize,yMax+yPlotDensityStep+yRandomize,yPlotDensityStep)

    (xx,yy) = getFrequencyPolygonPoints(r,list2, limits, xlim[-1], valueMin=xlim[0], valueScale = 0.3, setZeroX = True)

    if xx and yy:
      rpy.r.lines(yy, xx, lwd=1, col = 'grey')

    # TODO is technically a FREQUENCY POLYGON!
    # See http://addictedtorpy.r.free.fr/graphiques/RGraphGallery.php?graph=101

    limits = frange(xMin-(plotDensityStep*2),xMax+(plotDensityStep*2),plotDensityStep)

    for subIndex in range(len(plotDensityLists)):

      if plotDensityLists[subIndex]:
        # Draw for subdensity list
        (xx,yy) = getFrequencyPolygonPoints(r,plotDensityLists[subIndex], limits, ylim[-1])

        # draw the histogram
        #rpy.r.hist(plotDensityList, prob = rpy.r.TRUE, xlim=rpy.r.range(xx), border="gray", col="gray90")
        # adds the frequency polygon
        if xx and yy:
          rpy.r.lines(xx, yy, lwd=1, col=plotDensityColours[subIndex])

    if plotDensityAll:
      # Also do for all points!
      (xx,yy) = getFrequencyPolygonPoints(r,list1, limits, ylim[-1])
      if xx and yy:
        rpy.r.lines(xx, yy, lwd=1, col = densityColour)

  #
  # Now try to plot limits based on box... divide total y range by factor, then determine ranges for each.
  #
  # TODO: should I include this into plots at all? Is there a better way to visualise this?
  #

  if xRangeForYValue and plotXRangeForYValue:

    delimiterLen = (ylim[1] - ylim[0]) / 150.0
    curColour = 'red'

    for i in yBinsForXRange.keys():
      if yBinsForXRange[i]:
        (xMinTmp,xMaxTmp) = setOutlierLimits(yBinsForXRange[i],outlierRange=1.0)
        yPos = i*yBin

        rpy.r.lines((yBinMax,xMaxTmp),(yPos,yPos),lwd=0.1,col=curColour)
        rpy.r.lines((yBinMax,yBinMax),(yPos+delimiterLen,yPos-delimiterLen),lwd=2,col=curColour)
        rpy.r.lines((xMaxTmp,xMaxTmp),(yPos+delimiterLen,yPos-delimiterLen),lwd=2,col=curColour)

  #
  # Turn off this print - otherwise end up with a load of different threads.
  #

  rpy.r.dev_off()

  #
  # Return information if necessary...
  #

  if xRangeForYValue:
    return (yBinsForXRange, yBin)
  else:
    return None

def plotGraphMultiColour(fileName,
                         lists1,
                         lists2,
                         colourKeys = None,
                         colourList = None,
                         colourListType = 'standard', # Also possible: spectral (for gradations)
                         main = 'Graph',
                         xlab = '',
                         ylab = '',
                         xlim = None,
                         ylim = None,
                         fileType = 'png',
                         resolution = 200,
                         units='px',
                         textPointsize = 9,
                         legendLocation = 'topleft',
                         legendTitle = "Legend",
                         legendOutside = False,
                         legendRelativeTextSize = 1,
                         diagonal = False,
                         zeroLines=False,
                         addLines=None,
                         verticalLines=None,
                         anyLines=None,
                         arrows=None,
                         rectangles=None,
                         texts=None,
                         connectPoints = False,
                         connectPointsLineWidth=1,
                         connectPointsMaxSeparationX=None,
                         pointSize = 'normal',
                         scatterPoints = False,
                         xRandomize = None,
                         yRandomize = None,
                         randomizeZero=False,
                         plotDensityLists = None,
                         plotDensityColours = ('purple','red'),
                         plotDensityStep = 0.5,
                         plotDensityStepY = 0.5,
                         plotDensityLimitExtend = 2,
                         plotDensityYlimits = None,
                         plotDensityRemoveFirstLast = False,
                         plotDensityAll = False,
                         plotDensityTexts = None,
                         plotKeywds = None,
                         titleKeywds = None,
                         xCustomAxisTicks = None,
                         xCustomAxisLabels = None,
                         xCustomAxisOrientation = 'perpendicular', # Can also be 'parallel'
                         xCustomAxisLabelSize = 1.0,
                         xCustomAxisPackLabels = False,  # If true will 'pack' labels so can be closer together
                         width = 500,
                         height = 300
                         ):

  #from rpy import r

  if not colourList:
    colourList = getColourList(colourListType,len(lists1))
    # print colourList, len(lists1)

  legendTexts = []

  if colourKeys:
    for i in range(len(lists1)):
      if i >= len(colourKeys):
        print "  Warning: not plotting %s - not enough colours!" % colourKeys[i]
        continue
      legendTexts.append(colourKeys[i])

  if legendTexts:
    colourList = colourList[:len(legendTexts)]

  #
  # Some validation
  #

  if connectPoints and (xRandomize or yRandomize):
    print "Warning: cannot use connectPoints with randomize - randomization values set to zero!"
    xRandomize = yRandomize = None

  #
  # Set output type
  #
  outputKeywds = {}
  if width:
    outputKeywds['width'] = width
  if height:
    outputKeywds['height'] = height
  if fileType == 'pdf':
    rpy.r.pdf(fileName, paper = 'A4r', **outputKeywds)
  elif fileType == 'ps' or fileType == 'eps':
    rpy.r.postscript(fileName, paper = 'A4', **outputKeywds)
  elif fileType == 'png':
    if textPointsize:
      outputKeywds['pointsize'] = textPointsize
    if resolution:
      outputKeywds['res'] = resolution
    if units:
      outputKeywds['units'] = units
    rpy.r.png(fileName, **outputKeywds)

  else:
    print "Warning: non supported file type! -> %s" % (fileType)
    return

  #
  # Set some parameters for the plot (point type, ...)
  # See rss.acs.unt.edu/Rdoc/library/graphics/html/parpy.r.html for more info!
  #

  #rpy.r.par(family = 'HersheySans')
  #rpy.r.par(font = 2)

  parKeywds = {'adj': 0.5}

  # Set the point size in graph. Note: can also use cex to control this.
  if pointSize == 'normal':
    parKeywds['pch'] = 20
  elif pointSize == 'small':
    parKeywds['pch'] = '.'

  if legendOutside:
    legendLocation = 'topright'
    parKeywds['mar'] = (8.1, 4.1, 4.1, 8.1)
    parKeywds['xpd'] = True

  rpy.r.par(**parKeywds)

  #
  # Set specific keywords for plot
  #

  if not plotKeywds:
    plotKeywds = {}

  if not xlim:
    xlim = [None,None]
    for list1 in lists1:
      maxList = max(list1)
      minList = min(list1)
      if xlim[0] == None or xlim[0] > minList:
        xlim[0] = minList
      if xlim[1] == None or xlim[1] < maxList:
        xlim[1] = maxList

  plotKeywds['xlim'] = xlim

  if not ylim:
    ylim = [None,None]
    for list2 in lists2:
      maxList = max(list2)
      minList = min(list2)
      if ylim[0] == None or ylim[0] > minList:
        ylim[0] = minList
      if ylim[1] == None or ylim[1] < maxList:
        ylim[1] = maxList

  plotKeywds['ylim'] = ylim

  #
  # Make sure title keywords set up
  #

  if not titleKeywds:
    titleKeywds = {}

  #
  # Get the information for the density plots, if required
  #

  subText = ""

  if plotDensityLists:

    densityColour = 'black'

    allList1 = []
    allList2 = []
    for list1 in lists1:
      allList1.extend(list1)
    for list2 in lists2:
      allList2.extend(list2)

    if plotDensityTexts:
      # Print info on scaling!
      numPoints = len(allList1)

      if subText:
        subText += ', density for'
      else:
        subText += 'Density for'

      if plotDensityAll:
        subText += ' all (%s),' % densityColour

      for subIndex in range(len(plotDensityLists)):
        if plotDensityLists[subIndex]:
          numSubPoints = len(plotDensityLists[subIndex])
          factor = numSubPoints * 1.0 / numPoints

          subText += " %s (%s, scale %.2f)," % (plotDensityTexts[subIndex],plotDensityColours[subIndex], 1 / factor)

      subText = subText[:-1] + '.'

  #
  # Turn of x axis if customised...
  #

  if xCustomAxisTicks:
    plotKeywds['xaxt'] = 'n'

  #
  # Plot the main graph, no points yet
  #
  # Note: rpy.r.plot.window() will plot to screen!
  #

  rpy.r.plot('BLANK', ylab="", xlab="", type='p', ann=False, **plotKeywds)
  rpy.r.title(main=main, sub=subText, xlab=xlab, ylab=ylab, **titleKeywds)

  #
  # Plot rectangles first so don't shade plots
  #

  if rectangles:
    for (xleft,ybottom,xright,ytop,color,borderColor) in rectangles:
      rpy.r.rect(xleft,ybottom,xright,ytop,col=rpy.r.rgb(*color),border=rpy.r.rgb(*borderColor),lwd=0.5)

  #
  # Custom axis ticks
  #

  if xCustomAxisTicks:
    if xCustomAxisOrientation == 'perpendicular':
      las = 2
    elif xCustomAxisOrientation == 'parallel':
      las = 0

    axisKeywds = {'cex.axis': xCustomAxisLabelSize, 'padj': 0.4}

    if xCustomAxisPackLabels:

      for start in range(2):
        tmpAxisTicks = []
        tmpAxisLabels = []
        for i in range(start,len(xCustomAxisTicks),2):
          tmpAxisTicks.append(xCustomAxisTicks[i])
          tmpAxisLabels.append(xCustomAxisLabels[i])

        rpy.r.axis(1,at=tmpAxisTicks,labels=tmpAxisLabels,las=las,**axisKeywds)

    else:
      rpy.r.axis(1,at=xCustomAxisTicks,labels=xCustomAxisLabels,las=las,**axisKeywds)

  #
  # Randomises plotting so clearer overview of number of points of each colour
  #

  if scatterPoints:

    if connectPoints:
      print "  Warning: connecting points not possible when scattering point plotting!"

    scatteredPoints = []

    for i in range(len(lists1)):
      list1 = lists1[i]
      list2 = lists2[i]

      colour = colourList[i]
      numPoints = len(list1)
      for j in range(numPoints):
        # Ignore points that have None values
        if list1[j] == None or list2[j] == None:
          print "  Warning: None point removed"
          continue

        pointIndex = random.randint(0,len(scatteredPoints))

        scatteredPoints.insert(pointIndex,(list1[j], list2[j], colour))

    for (x,y,colour) in scatteredPoints:

      #
      # Note: only for float numbers! Randomize is a float value, will be randomized as (-value,+value).
      # Values below randomize limit are not randomized (this is to prevent zero from being randomized!)
      #

      if xRandomize and (x > xRandomize or randomizeZero):
        x += random.uniform(-xRandomize,xRandomize)
      if yRandomize and (y > yRandomize or randomizeZero):
        y += random.uniform(-yRandomize,yRandomize)

      rpy.r.points(x,y, col=colour, type="o")


  else:
    for i in range(len(lists1)):
      list1 = lists1[i]
      list2 = lists2[i]

      colour = colourList[i]
      numPoints = len(list1)
      for j in range(numPoints):
        # Ignore points that have None values
        if list1[j] == None or list2[j] == None:
          print "  Warning: None point removed"
          continue

        #
        # Note: only for float numbers! Randomize is a float value, will be randomized as (-value,+value).
        # Values below randomize limit are not randomized (this is to prevent zero from being randomized!)
        #

        x = list1[j]
        y = list2[j]

        if xRandomize and (x > xRandomize or randomizeZero):
          x += random.uniform(-xRandomize,xRandomize)
        if yRandomize and (y > yRandomize or randomizeZero):
          y += random.uniform(-yRandomize,yRandomize)

        rpy.r.points(x, y, col=colour, type="o")

        # Note: connectPoints does NOT work with randomize!
        if connectPoints and j + 1 < numPoints:
          if not connectPointsMaxSeparationX or abs(list1[j] - list1[j+1]) <= connectPointsMaxSeparationX:
            rpy.r.segments(list1[j], list2[j], list1[j+1], list2[j+1],col = colour, lwd=connectPointsLineWidth)

  #
  # Plot density - is based on input list!!!
  #

  if plotDensityLists:

    xMin = min(allList1)
    xMax = max(allList1)

    yMin = min(allList2)
    yMax = max(allList2)

    # Also do for exposure! Do this first, should be buried underneath other plots.
    limits = frange(yMin-plotDensityStepY,yMax+plotDensityStepY,plotDensityStepY)

    (xx,yy) = getFrequencyPolygonPoints(rpy.r,allList2, limits, xlim[-1], valueMin=xlim[0], valueScale = 0.3, setZeroX = False)

    if xx and yy:
      rpy.r.lines(yy, xx, lwd=1, col = 'grey')

    # TODO is technically a FREQUENCY POLYGON!
    # See http://addictedtorpy.r.free.fr/graphiques/RGraphGallery.php?graph=101

    limits = frange(xMin-(plotDensityStep*plotDensityLimitExtend),xMax+(plotDensityStep*plotDensityLimitExtend),plotDensityStep)
    limitsMin = limits[0]
    limitsMax = limits[-1]

    for subIndex in range(len(plotDensityLists)):

      if plotDensityLists[subIndex]:
        # Draw for subdensity list

        if plotDensityYlimits:
          valueMax = plotDensityYlimits[1]
          valueMin = plotDensityYlimits[0]
        else:
          valueMax = ylim[-1]
          valueMin = ylim[0]

        # Do some cleanup of lists if required
        if min(plotDensityLists[subIndex]) < limitsMin or max(plotDensityLists[subIndex]) > limitsMax:
          pointsRemoved = 0
          pdIndex = 0
          while pdIndex < len(plotDensityLists[subIndex]):
            if limitsMin < plotDensityLists[subIndex][pdIndex] < limitsMax:
              pdIndex += 1
            else:
              plotDensityLists[subIndex].pop(pdIndex)
              pointsRemoved += 1

          print "  Warning: removed {} point for density plot.".format(pointsRemoved)

        (xx,yy) = getFrequencyPolygonPoints(rpy.r,plotDensityLists[subIndex], limits, valueMax, valueMin= valueMin)

        # draw the histogram
        #rpy.r.hist(plotDensityList, prob = rpy.r.TRUE, xlim=rpy.r.range(xx), border="gray", col="gray90")
        # adds the frequency polygon
        if xx and yy:
          if plotDensityRemoveFirstLast:
            xx = xx[1:-1]
            yy = yy[1:-1]

          rpy.r.lines(xx, yy, lwd=1, col=plotDensityColours[subIndex])

    # Also do for all points!
    if plotDensityAll:
      (xx,yy) = getFrequencyPolygonPoints(rpy.r,allList1, limits, ylim[-1])
      if xx and yy:
        rpy.r.lines(xx, yy, lwd=1, col = densityColour)

  #
  # Plot diagonal line if required
  #

  if diagonal:
    rpy.r.abline(0,1)

  #
  # Plot zero lines
  #

  if zeroLines:
    rpy.r.abline(h=0,v=0)

  #
  # Plot other lines, arrows, ...
  #

  if addLines:
    for (a,b) in addLines:
      rpy.r.abline(a=a,b=b)

  if verticalLines:
    for (coord,color) in verticalLines:
      rpy.r.abline(v=coord,col=color)

  if anyLines:
    for (x1,y1,x2,y2,color) in anyLines:
      rpy.r.lines((x1,x2),(y1,y2),col=color)

  if arrows:
    for (x0,y0,x1,y1,length,angle,color,lwd) in arrows:
      rpy.r.arrows(x0,y0,x1=x1,y1=y1,length=length,angle=angle,col=color,code=2, lwd=lwd) # code 1 draws arrow at x0,y0, 2 at x1,y1, 3 at both

  if texts:
    for (x,y,text,pos,col) in texts:
      rpy.r.text(x,y,text,pos=pos,col=col)

  #
  # Always do legend last...
  #

  legendKeywds = {}
  if legendTitle:
    legendKeywds['title'] = legendTitle

  legendKeywds['col'] = colourList
  legendKeywds['lty'] = 0
  legendKeywds['pch'] = 15
  legendKeywds['cex'] = legendRelativeTextSize

  if legendOutside:
    legendKeywds['inset'] = (-0.22,0)

  if legendTexts:
    rpy.r.legend(legendLocation, legendTexts, **legendKeywds)

  #
  # Turn off this print - otherwise end up with a load of different threads.
  #

  rpy.r.dev_off()

def plotHisto(fileName,dataLists,histRange,main,xlab,ylab,
              density = False,
              bandWidth = 1,
              fileType= 'png',
              width = 800,
              height = 800,
              resolution=None,
              units='px',
              pointsize = 12,
              showData = False,
              truncateZero = False,
              colourRgbList = None,
              onlyDensity = False,
              legendLocation = 'topleft',
              legendTexts = [],
              legendTitle = None,
              ylim = None):

  #from rpy import r

  #
  # Fix if just one list given
  #

  if type(dataLists[0]) not in (type([]),type(tuple())):
    dataLists = [dataLists]

  #
  # Set output type
  #

  if fileType == 'ps':
    rpy.r.postscript(fileName,paper = 'A4')
  elif fileType == 'png':
    pngKywds = {}
    if width:
      pngKywds['width'] = width
    if height:
      pngKywds['height'] = height
    if pointsize:
      pngKywds['pointsize'] = pointsize
    if resolution:
      pngKywds['res'] = resolution
    if units:
      pngKywds['units'] = units

    rpy.r.png(fileName, **pngKywds)

  #
  # If no data colours available, pick 'em - note that this has to be input for rpy.r.rgb, so includes transparancy!!!
  #

  if not colourRgbList:
    colourRgbList = [(1.0,0.0,0.0,0.5),(0.0,1.0,0.0,0.5),(0.0,0.0,1.0,0.5)]  # Red, green, blue

  #
  # Plot density (or not)
  # When prob = rpy.r.TRUE, will use the density as Y rather than number of times it occurs!
  #

  if density:
    probFlag = rpy.r.TRUE
  else:
    probFlag = rpy.r.FALSE

  #
  # Truncate the amount of zero values if excessive, so better representation...
  # note that this is a division factor, and keep what's left after division!
  #

  if truncateZero:

    for tmpDataList in dataLists:
      if not tmpDataList:
        continue

      numZeros = tmpDataList.count(0.0)

      for i in range(numZeros - int(numZeros * 1.0/truncateZero)):
        tmpDataList.pop(tmpDataList.index(0.0))

  #
  # Set ylim if required...
  #

  if not ylim and len(dataLists) > 1:
    ylim = -9
    for dataList in dataLists:
      #print len(dataList), dataLists.index(dataList)
      histData = rpy.r.hist(dataList,breaks=histRange,plot=False)
      if max(histData['counts']) > ylim:
        ylim = max(histData['counts'])

    ylim = (0,ylim)

  #
  # Create the histogram
  #

  if not onlyDensity:
    width = 1.0 / len(dataLists)
    rpy.r.hist(dataLists[0],breaks=histRange,width = width,col=rpy.r.rgb(*colourRgbList[0]), main = main, xlab = xlab, ylab=ylab, ylim=ylim)
    if len(dataLists) > 1:
      for j in range(1,len(dataLists)):
        otherDataList = dataLists[j]
        rpy.r.hist(otherDataList,breaks=histRange,width = width, offset= width * j, col=rpy.r.rgb(*colourRgbList[j]), add=rpy.r.TRUE)

  #
  # Using density is a 'clearer' way to plot the graph. The bw is the bandwidth (smoothes the function)
  #

  if density:
    if not onlyDensity:
      for i in range(len(dataLists)):
        dataList = dataLists[i]
        rpy.r.lines(rpy.r.density(dataList, bw = bandWidth),col=rpy.r.rgb(*colourRgbList[i]))
    else:
      # Only working for single data set at the moment
      rpy.r.plot(rpy.r.density(dataLists[0], bw = bandWidth),main = main, xlab = xlab, ylab=ylab, type='l')

  #
  # If truncated, put break on zero values
  #

  if truncateZero:
    rpy.r.lines((98,102),(48,50))

  #
  # Show data points
  #

  if showData:
    for i in range(len(dataLists)):
      dataList = dataLists[i]
      rpy.r.rug(dataList,col=rpy.r.rgb(*colourRgbList[0]))

  #
  # Always do legend last...
  #

  if legendTitle:

    legendKeywds = {}
    if legendTitle:
      legendKeywds['title'] = legendTitle

    legendKeywds['col'] = [rpy.r.rgb(*colourRgb) for colourRgb in colourRgbList]
    legendKeywds['lty'] = 0
    legendKeywds['pch'] = 15

    rpy.r.legend(legendLocation, legendTexts, **legendKeywds)

  #
  # Turn off this print - otherwise end up with a load of different threads.
  #

  rpy.r.dev_off()


def plotImageGraph(fileName,
                   x,
                   y,
                   matrix,
                   main = 'Graph',
                   xlab = '',
                   xlim = None,
                   ylab = '',
                   ylim = None,
                   fileType = 'png',
                   resolution = 200,
                   colourList = None,
                   breakList = None,
                   legendLocation = 'topleft',
                   legendTexts = [],
                   legendTitle = None):

  #from rpy import r

  #
  # Set output type
  #

  if fileType == 'ps':
    rpy.r.postscript(fileName,paper = 'A4')
  elif fileType == 'png':
    rpy.r.bitmap(fileName, res=resolution)

  #
  # Colourlist and breaklist - there have to be i+i elements in breaklist!!
  # Only used if both given.
  #

  plotKeywds = {}

  if colourList and breakList and (len(colourList) + 1 == len(breakList)):

    plotKeywds['col'] = colourList
    plotKeywds['breaks'] = breakList

  else:

    print "Warning: not using breakList and colourList - len(breakList) %d has to be len(colourList) %d +1!" % (len(breakList),len(colourList))

  #
  # Plot limits
  #

  if xlim:
    plotKeywds['xlim'] = xlim

  if ylim:
    plotKeywds['ylim'] = ylim

  #
  # Plot the graph
  #
  # Note: rpy.r.plot.window() will plot to screen!
  #

  rpy.r.image(x,y,matrix, main = main, xlab = xlab, ylab = ylab, **plotKeywds)

  #
  # Always do legend last...
  #

  if legendTitle:

    legendKeywds = {}
    if legendTitle:
      legendKeywds['title'] = legendTitle

    legendKeywds['col'] = colourList
    legendKeywds['lty'] = 0
    legendKeywds['pch'] = 15

    if not legendTexts:
      legendTexts = []
      for i in range(len(breakList)-1):
        legendTexts.append("%s-%s" % (str(breakList[i]),str(breakList[i+1])))

    rpy.r.legend(legendLocation, legendTexts, **legendKeywds)

  #
  # Turn off this print - otherwise end up with a load of different threads.
  #

  rpy.r.dev_off()


def getFrequencyPolygonPoints(r,valueList,valueRange,valueMax,valueMin=0.0,valueScale=0.90,setZeroX = False):

  if len(valueRange) <= 3:
    print "  Warning: no frequence polygon written, value range is too small."
    return (None,None)

  h = rpy.r.hist(valueList, valueRange, plot=rpy.r.FALSE) # , prob=rpy.r.TRUE is not used on mammoth... is it important?

  # compute the frequency polygon
  diffBreaks = h['mids'][1] - h['mids'][0]
  xx = rpy.r.c( h['mids'][0]-diffBreaks, h['mids'], h['mids'][-1]+diffBreaks )
  yy = rpy.r.c(0, h['density'], 0)

  if setZeroX:
    xCorr = 0 - h['mids'][0]
  else:
    xCorr = 0.0

  # Scale
  yyMax = max(yy)
  multiplication = (valueMax-valueMin) * valueScale / yyMax
  for i in range(len(yy)):
    yy[i] *= multiplication
    yy[i] += valueMin
    xx[i] += xCorr

  return (xx,yy)

