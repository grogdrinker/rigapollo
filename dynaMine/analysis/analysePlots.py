try:
  import rpy
except:
  import rpy2.rpy_classic as rpy
  rpy.set_default_mode(rpy.BASIC_CONVERSION)
  
r = rpy.r
from scipy.optimize import minimize

import os

class CompareDynaMinePlots:

  def superimposePlots(self,plotPredLists,applyCorrection=True,onlyResidueIndexes=None):
  
    # Requires rpy and scipy
    # Return a list of superimposed predicted values, the correction values and the rms. Only works for same sequence length fragments
  
    # First determine the average of each point in the profile to be superimposed; None values are removed for this 
    self.avgList = []
    for i in range(len(plotPredLists[0])):
      valuesAtIndexList = []
      for values in plotPredLists:
        if values[i] != None:
          valuesAtIndexList.append(values[i])
          
      self.avgList.append(r.summary(valuesAtIndexList)['Mean'])
  
    # Now superimpose and return values related to superimposition
    plotPredListsMinimized = []
    corrections = []
    rmsPerPlot = []
    rmsPerResidue = []
    
    # This is maximum value for correction, can only go down with current settings, not sure if these are sensible.
    c = 1.0 
    for self.actualList in plotPredLists:
    
      if applyCorrection:
        res = minimize(self.rmsCorrection, c, method='nelder-mead', options={'xtol': 1e-8, 'disp': True})
        correction = res.x[0]
      else:
        correction = 0.0
      
      corrections.append(correction)
      
      plotPredListsMinimized.append([])
      for value in self.actualList:
        if value != None:
          plotPredListsMinimized[-1].append(value + correction)
        else:
          plotPredListsMinimized[-1].append(None)

      # Get the RMS of this plot to the average
      self.actualList = plotPredListsMinimized[-1]
      rmsPerPlot.append(self.rmsCorrection(correction))
    
    # Also calculate RMS per point after global correction
    if onlyResidueIndexes:
      indexList = onlyResidueIndexes
    else:
      indexList = range(len(plotPredLists[0]))

    for i in indexList:
      rmsPerResidue.append(self.rmsPerResidue(i,plotPredLists,corrections))
      
    return (plotPredListsMinimized,corrections,rmsPerPlot,rmsPerResidue)
      
  def rmsCorrection(self,c):
  
    #
    # This function is called by minimize
    #
  
    rms = 0
    dataPoints = 0
    for i in range(len(self.avgList)):
      if self.actualList[i] != None:
        dataPoints += 1
        rms += (self.actualList[i] - self.avgList[i] + c) ** 2
        
    if dataPoints:
      rms = (rms / dataPoints) ** 0.5

    return rms
  
  def rmsPerResidue(self,seqIndex,plotPredLists,corrections):
  
    rms = 0
    dataPoints = 0
    for listIndex in range(len(plotPredLists)):
      actualList = plotPredLists[listIndex]
      if actualList[seqIndex] != None:
        dataPoints += 1
        rms += (actualList[seqIndex] - self.avgList[seqIndex] + corrections[listIndex]) ** 2
    
    if dataPoints:
      rms = (rms / dataPoints) ** 0.5
    
    return rms


class MultiProfileHandler:

  """
  This class enables dealing with multiple profiles (backbone, helix, ...) in the same script
  as long as self.dataType is set to one of ('backbone','helix','sheet','coil','sidechain')
  """
  
  ssDataTypes = ('helix','sheet','coil')
  defaultDataDir = 'data'

  def readPredictedDataValues(self,predictionFileName,predictions,normalise=False):
  
    if predictionFileName:
      if self.dataType in ('backbone','sidechain'):
        self.dmp.readPredictionData(predictionFileName)
      elif self.dataType in self.ssDataTypes:
        ssPred = self.dmp.readSsPredictionData(predictionFileName,setSsAsMainPredictionData=self.dataType)
    else:
      self.dmp.setPredictionData(predictions)
      
    if normalise and self.dmp.predictionData:
      
      seqIndexes = self.dmp.predictionData.keys()
      seqIndexes.sort()
      
      values = [self.dmp.predictionData[seqIndex][1] for seqIndex in seqIndexes]
      #minValue = min(values)
      maxValue = max(values)
      correction = 1.0 - maxValue
      
      for seqIndex in seqIndexes:
        #self.dmp.predictionData[seqIndex] = (self.dmp.predictionData[seqIndex][0],(self.dmp.predictionData[seqIndex][1] - minValue) / (maxValue - minValue))
        self.dmp.predictionData[seqIndex] = (self.dmp.predictionData[seqIndex][0],(self.dmp.predictionData[seqIndex][1] + correction))
          
  def getFileDataType(self):

    if self.dataType in self.ssDataTypes:
      fileDataType = 'secStruc'
    else:
      fileDataType = self.dataType
      
    return fileDataType
          
  def setDataTypeSpecific(self,dataType,singlePredictionFile):
  
    self.singlePredictionFile = singlePredictionFile
    if singlePredictionFile:
      self.predictionValuesBySeqId = {}
      
    self.dataType = dataType
    self.yAxisLabel = 'S2 prediction'
    if dataType != 'backbone':
      self.graphDir = os.path.join(self.defaultGraphDir,dataType)
      self.dataDir = os.path.join(self.defaultDataDir,dataType)
      self.yAxisLabel = "{} prediction".format(dataType)
    else:
      self.graphDir = self.defaultGraphDir
      self.dataDir = self.defaultDataDir
    
    # Make sure directories exist
    if not os.path.exists(self.graphDir):
      os.mkdir(self.graphDir)
    if not os.path.exists(self.dataDir):
      os.mkdir(self.dataDir)
    
  
