import os, glob, math

try:
  import rpy2.rpy_classic as rpy
  rpy.set_default_mode(rpy.BASIC_CONVERSION)
except:
  import rpy

r = rpy.r

from dynaMine.analysis.makePlot import MakeDynaMinePlot

from dynaMine.analysis.analysePlots import CompareDynaMinePlots, MultiProfileHandler
from dynaMine.analysis.parsers import DynaMineRelatedFileParsers

from dynaMine.analysis.Util import plotGraph

from dynaMine.predictor.wrappers import RunDynaMinePrediction

"""

Still to address:

- Flexible colour scheme (currently based on absolute values, should get better idea of how it is most informative)
- Flexible graphs (something grace based? split into sections?)
- Relate to SwissProt annotations? Check which regions purely structural, which ones more about specificity, does that correlate with graphs?
- Write out matrix with sequence identity between conserved regions (?)
- Write out average correction per sequence (?)



- INCLUDE boxplot spread of first derivative; gives an idea of variability of profile!


- Does dynamine value against slope say something about type of protein? Get functional info about sequences for uniprot? Or elsewhere? PDB codes available as well...


"""

# Wim taken out RunDynaMinePrediction, not really good to have that class in here.
class ProteinFamilyCompare(CompareDynaMinePlots,DynaMineRelatedFileParsers,MultiProfileHandler):

  # This local
  dynaMineResultsDir = 'dynaMineResults'
    
  insertCodeFromFile = '.'
  
  seqLogoGapCode = '-'
  
  defaultGraphDir = 'graphs'
  defaultDataDir = 'data'
  alignmentsDir = 'alignments'
  covariationDir = 'covar'
  
  xAxisLabelSeqOffset = 0
  
  def __init__(self,identifier):
  
    self.identifier = identifier

  def initialiseAlignFile(self,alignFile):
  
    (pathName,baseName) = os.path.split(alignFile)

    self.familyName = baseName[:-4]
    self.graphPathIdentifier = "{}_{}".format(self.identifier,self.familyName)
    self.graphTitleText = "{}, {}".format(self.identifier,self.familyName)

    print self.familyName

  def setCustomAxisLabel(self,relativeSeqIndex,alignSeqCode):
  
    # This aligns by the numbering in the alignment itself
    self.customAxisLabels[relativeSeqIndex] = alignSeqCode

  def filterSeqAlignments(self,seqAlignments):
  
    return seqAlignments

  def getPredictionFileName(self,seqId):
  
    resultsSubDir = self.getResultsSubDir()
    fileIdentifier = self.getFileIdentifier(seqId)
    
    fileDataType = self.getFileDataType()
        
    predictionFileName = os.path.join(self.dynaMineResultsDir,resultsSubDir,"{}_{}.pred".format(fileIdentifier,fileDataType))
    
    return predictionFileName

  def getResultsSubDir(self):
  
    return self.identifier
    
  def getFileIdentifier(self,seqId):
  
    # TODO should probably be in generic function somewhere for consistency!
    return seqId.replace("|","_").replace(":","_").replace(";","_").replace("-","_").replace("/","_")

  def setDynaMinePlot(self):

    if self.dataType == 'backbone':
      self.dmp = MakeDynaMinePlot()
    elif self.dataType in self.ssDataTypes:
      from dynaMine.analysis.makePlotBeta import MakeDynaMinePlotBeta
      self.dmp = MakeDynaMinePlotBeta()
    
  def compareProfilesMultipleDataTypes(self,testMode=False,
                                       ignoreSharedGaps=True,         # Ignore full gaps in the plot (i.e. nothing available in alignment, can happen for subset!)
                                       seqAlignRange=None,            # List/tuple of the sequence codes to include in the analysis
                                       separateRanges=True,           # Put space between separated sequence code ranges in the output
                                       plotIndividualInfo=True,       # Plot the main graphs
                                       plotSharedRegionInfo=True,     # Plot the aligned sequence blocks
                                       analyseByWindow=None,          # Do an analysis of the variation by a sequence window - give a list of values!
                                       plotsOn=True,                  # Make plots
                                       normalise=False,               # Normalise absolute values between 0 and 1
                                       fullAnalysis=True,             # Set to false if only want to get original data (i.e. parsed MSAs with dynamine values)
                                       singlePredictionFile=False,    # Set to True if all predictions in one file
                                       dataTypes=['backbone','helix','sheet','coil']):          # List of data types to plot; default is dynaMine, but can be helix/sheet/coil from ss prediction, or sidechain
    
    for dataType in dataTypes:
      self.compareProfiles(testMode=testMode,ignoreSharedGaps=ignoreSharedGaps,seqAlignRange=seqAlignRange,separateRanges=separateRanges,
                           plotIndividualInfo=plotIndividualInfo,plotSharedRegionInfo=plotSharedRegionInfo,analyseByWindow=analyseByWindow,
                           plotsOn=plotsOn,dataType=dataType,fullAnalysis=fullAnalysis)                          
  
  def compareProfiles(self,testMode=False,
                           generatePredictions=True,
                           ignoreSharedGaps=True,         # Ignore full gaps in the plot (i.e. nothing available in alignment, can happen for subset!)
                           seqAlignRange=None,            # List/tuple of the sequence codes to include in the analysis
                           separateRanges=True,           # Put space between separated sequence code ranges in the output
                           plotIndividualInfo=True,       # Plot the main graphs
                           plotSharedRegionInfo=True,     # Plot the aligned sequence blocks
                           analyseByWindow=None,          # Do an analysis of the variation by a sequence window - give the length
                           plotsOn=True,                  # Make plots
                           normalise=False,               # Normalise absolute values between 0 and 1
                           bypassBadSequences=False,
                           fullAnalysis=True,             # Set to false if only want to get original data (i.e. parsed MSAs with dynamine values)
                           calculateEntropy=False,        # Only relevant if fullAnalysis == False. Set to True if also want to calculate entropy values
                           singlePredictionFile=False,    # Set to True if all predictions in one file
                           dataType='backbone'):          # Data type to plot; default is backbone, but can be helix/sheet/coil from ss prediction, or sidechain
  
    for pathName in (self.defaultGraphDir,self.alignmentsDir,self.covariationDir):
      if not os.path.exists(pathName):
        os.mkdir(pathName)
    
    # Make subdir if not standard backbone plots  
    self.plotsOn = plotsOn
    
    # Normalise the values for each sequence to values between 0 and 1
    self.normalise = normalise
    
    # Bypass sequences with residue mismatches, use with care because means problems with underlying data
    self.bypassBadSequences = bypassBadSequences
    
    # Initialise data type related stuff, in MultiProfileHandler
    self.setDataTypeSpecific(dataType,singlePredictionFile)
      
    # Get alignment file(s)
    alignFiles = self.getAlignFiles()
    
    # Track data only
    if not fullAnalysis:
      allSeqAlignments = {}
    
    validAlignFiles = 0
      
    for alignFile in alignFiles:
    
      print "\n*** Reading alignment file... ***\n"
      print alignFile
    
      #
      # Read the sequence alignment, and clean up if required (can create custom families and filter them)
      #

      self.initialiseAlignFile(alignFile)
      
      seqAlignments = self.readAlignments(alignFile)      
      seqAlignments = self.filterSeqAlignments(seqAlignments)
      
      # Could be all filtered out...
      if not seqAlignments or len(seqAlignments) == 1:
        print "\nERROR: not enough data, reading next alignment...\n"
        continue
        
      #
      # Test mode, read at least three good alignment files
      #
      
      if testMode:
        if validAlignFiles > 3:
          break
          
        validAlignFiles += 1

      #
      # Organise the sequence IDs for the alignments, and track
      #
      
      seqIds = seqAlignments.keys()
      seqIds.sort()
      self.seqIds = seqIds[:]
      
      #
      # Generate fastas and predictions if required
      #
      
      if generatePredictions:
        (alignDir,alignFileName) = os.path.split(alignFile)
        
        predictionDir = os.path.join(self.dynaMineResultsDir,self.identifier,self.alignRefSeqID)                
        if not os.path.exists(predictionDir):
          os.makedirs(predictionDir)
        
        outFastaFile = os.path.join(predictionDir,'{}.fasta'.format(self.alignRefSeqID))
        self.writeFastaFromAlignment(alignFile,outFastaFile)
        
        rdmp = RunDynaMinePrediction()
        rdmp.runDynaMineAll(outFastaFile,self.alignRefSeqID,workDir=predictionDir,overWrite=False) # windowSize=None,
      
      #
      # Can 'zoom in' on region by using seqAlignRange - ONLY WORKS FOR SINGLE ALIGNMENT FILE PROBLEMS!
      #
      
      if not seqAlignRange:
        self.useSeqAlignRange = range(1,len(seqAlignments[seqIds[0]])+1)
      else:
        self.useSeqAlignRange = seqAlignRange
        
      #
      # Now map where the regions are without insertions, and also track sequence positions where there are only gaps
      #
      # This is always done for the full alignment, in order to track absolute indexes correctly
      #
      # alignSeqIndex is the index in the alignment, alignSeqCode the sequence code to plot on the graph (so starting at 1)
      #
      
      seqInsertInfo = self.mapInsertionsAndGaps(seqAlignments,seqIds,ignoreSharedGaps,separateRanges)
      
      #
      # Get the MakeDynaMinePlot class; does all kinds of things, but mostly for graph generation
      #
      
      self.setDynaMinePlot()
      
      #
      # Track values for using in fully aligned blocks plotting later on, also slopes and averaged plots...
      # Also sets self.infoBySeqId and self.validSeqIds
      #
      # Note that seqAlignmentInfo is reset later on for conserved region matching!
      #
      
      print "\n*** Tracking values... ***\n"
      seqAlignmentInfo = self.trackDynaMineValues(seqAlignments,seqIds)
      
      if not fullAnalysis:
      
        allSeqAlignments[alignFile] = {
        
            'seqAlignmentInfo': seqAlignmentInfo,
            'seqIds': self.seqIds,
            'dynaMineValuesByAlignment': self.dynaMineValuesByAlignment
            
            }
      
      
      if fullAnalysis or calculateEntropy:
      
        #
        # Get sequence entropy info, and plot graph if required
        #

        print "\n*** Sequence entropy... ***\n"
        if not os.path.exists(self.alignmentsDir):
          os.mkdir(self.alignmentsDir)

        allAlignFile = "{}/seqLogo_{}.align".format(self.alignmentsDir,self.graphPathIdentifier)
        self.writeSeqs(allAlignFile,seqAlignmentInfo)

        if plotIndividualInfo or calculateEntropy:
          seqLogoFile = "{}/seqLogoAll_{}.png".format(self.graphDir,self.graphPathIdentifier)

          seqCodeEntropy = self.dmp.writeSequenceLogo(allAlignFile,
                                                    seqLogoFile,"{} {}".format(self.analysisName,self.graphTitleText),
                                                    seqLen=len(seqAlignmentInfo[self.seqIds[0]]),
                                                    resolution=600,
                                                    logoHeight = 400)
                                                    
          if calculateEntropy:
            allSeqAlignments[alignFile]['seqCodeEntropy'] = seqCodeEntropy

      if fullAnalysis:
      
        #
        # Get local covariation data
        #

        print "\n*** Covariation... ***\n"
        mipOutputFile = os.path.join(self.covariationDir,"covar_{}.covar".format(self.graphPathIdentifier))   

        from sbb.adatah.MIp import MIpLocal

        try:
          mip = MIpLocal()      
          mip.getMIpData(allAlignFile,mipOutputFile)

          self.seqCodeLocalCovariation = mip.calculateLocalCovariation() # Tracking this to re-use in fully conserved blocks - only values for those
        except:
          print "Error: No local covariation calculation possible for {}, ignoring.".format(alignFile)
          self.seqCodeLocalCovariation = {}

        #
        # Arrange entropy and local covariation as density lists
        #

        plotDensityLists = self.getDensityLists(seqCodeEntropy,self.seqCodeLocalCovariation)

        #
        # Plot graph with individual full profiles in colour as calculated by DynaMine
        #

        print "\n*** Individual graphs... ***\n"
        if plotIndividualInfo and self.plotsOn:
          self.plotIndividualGraph(plotDensityLists,seqInsertInfo)

        #
        # Plot/analyse taking full alignment into account and using a window to go over the sequence and superimpose
        #

        if analyseByWindow:
          print "\n*** By window analyses.. ***\n"
          self.analyseFullAlignmentByWindow(seqCodeEntropy,0,plotDensityLists)
          for windowSize in analyseByWindow:
            self.analyseFullAlignmentByWindow(seqCodeEntropy,windowSize,plotDensityLists)

          # NOTE: this also modifies dmp. -> check if causing problems for next step!

        #
        # Plot/analyse only regions where there is information for ALL sequences in the alignment
        #

        print "\n*** Shared region plots... ***\n"
        if plotSharedRegionInfo:
          self.plotSharedRegionGraphs()

        #break
      
    if not fullAnalysis:
      return allSeqAlignments      
            
  def getSeqIdLabel(self,seqId):
  
    # Can be tweaked...
    return seqId

  def mapInsertionsAndGaps(self,seqAlignments,seqIds,ignoreSharedGaps,separateRanges):
      
    self.alignSeqIndexRange = range(len(seqAlignments[seqIds[0]]))

    self.alignSeqCodesToIgnore = []      
    self.alignedSeqCodeBlocks = []

    previousAlignSeqIndex = None  # This for tracking blocks with full alignment
    previousAlignSeqCode = None   # This for tracking sequence code range jumps

    self.alignSeqCodeToValidResidueIndex = {}  # This is to track the alignment sequence code to the index of the lists tracking valid residue values (after filtering out seqCodes to ignore and seqalignrange)
    validResidueIndex = 0
 
    self.alignSeqCodeToXAxisIndex = {}  # This is to remove gaps from the plots appropriately
    
    # This is relevant for covariance analysis; starts counting at first res of the first sequence
    self.refSeqId = seqIds[0]
    self.refSeqIdCodes = {} 
    refSeqIdCode = 1
    
    xAxisIndex = 0

    seqInsertInfo = ""

    # Loop over the alignment
    for alignSeqIndex in self.alignSeqIndexRange:

      alignSeqCode = alignSeqIndex + 1

      # Track the number of insertion symbols for this position
      numInserts = 0
      for seqId in seqIds:
        if seqAlignments[seqId][alignSeqIndex] == self.insertCodeFromFile:
          numInserts += 1
          
      # Track sequence codes for reference sequence
      if seqAlignments[self.refSeqId][alignSeqIndex] != self.insertCodeFromFile:
        self.refSeqIdCodes[alignSeqCode] = refSeqIdCode
        refSeqIdCode += 1

      # If no inserts, has full alignment, otherwise not. Track these individually and also as blocks of sequence codes
      if not numInserts:
        if previousAlignSeqIndex == None or previousAlignSeqIndex != alignSeqIndex - 1:
          self.alignedSeqCodeBlocks.append([])

        self.alignedSeqCodeBlocks[-1].append(alignSeqCode)
        previousAlignSeqIndex = alignSeqIndex

      # If full gaps have to be ignored in the plot, then track them here.
      if ignoreSharedGaps and numInserts == len(seqIds):
        self.alignSeqCodesToIgnore.append(alignSeqCode)
        print 'Skipping {}...'.format(alignSeqCode)


      # If no inserts, and this part not ignored, then add codes to seqInsertInfo. LEAVE AT END!
      # Also means that the seqInsertInfo stays in sync with the x axis index.
      if alignSeqCode not in self.alignSeqCodesToIgnore and alignSeqCode in self.useSeqAlignRange:

        # Space out selected ranges if required
        if separateRanges and previousAlignSeqCode and previousAlignSeqCode != alignSeqCode - 1:
          xAxisIndex += 2
          seqInsertInfo += '11' # Still has to match overall numbering or won't work!

        xAxisIndex += 1
        self.alignSeqCodeToXAxisIndex[alignSeqCode] = xAxisIndex
        
        self.alignSeqCodeToValidResidueIndex[alignSeqCode] = validResidueIndex
        validResidueIndex += 1

        if not numInserts:
          seqInsertInfo += '0'
        else:
          seqInsertInfo += '1'

        previousAlignSeqCode = alignSeqCode
    
    return seqInsertInfo
    
  def trackDynaMineValues(self,seqAlignments,seqIds):
  
    self.infoBySeqId = {}
    self.validSeqIds = []

    self.dynaMineValuesByAlignment = [] # This is for analysis by a sequence window!
    self.dynaMineSlopesByAlignment = []

    seqAlignmentInfo = {}

    # This tracks the actual sequence index of each sequence so can number based on a reference, if so desired
    #self.alignSeqCodeToActualSeqCode = {}
    
    #
    # Read in the single prediction file if appropriate
    #
    
    if self.singlePredictionFile:
      predictionFileName = self.getPredictionFileName(self.identifier)
      print predictionFileName
      predictions = self.readDynaMineMultiEntry(predictionFileName)
    else:
      predictions = None

    #
    # Loop over the sequences
    #

    for seqId in seqIds:

      #
      # Initialise and read the DynaMine prediction values - the indexes in there will likely differ from the alignment.
      #

      #self.alignSeqCodeToActualSeqCode[seqId] = {}
      
      fileSeqId = self.convertSeqIdForFile(seqId)

      if not self.singlePredictionFile:
        predictionFileName = self.getPredictionFileName(fileSeqId)
        currentPred = None

        if not os.path.exists(predictionFileName):
          print "File {} does not exist, ignoring".format(predictionFileName)
          continue
          

      else:
        predictionFileName = None
        currentPred = predictions[fileSeqId]

      self.readPredictedDataValues(predictionFileName,currentPred,normalise=self.normalise)
        
      if not self.dmp.predictionData:
        print "No data for {}, ignoring".format(seqId)
        self.seqIds.pop(self.seqIds.index(seqId))
        continue

      # Get slopes and average (smoothed) value
      (slopes,averages) = self.dmp.setSlopesAndAvg()

      self.validSeqIds.append(seqId)
      seqAlignmentInfo[seqId] = ""
      aaCodes = ""

      # Now essentially hacking addPredictionData() to put make sure sequence numbering matches for all...
      alignment = seqAlignments[seqId]        

      # Set colour key info for this sequence
      self.dmp.colourKeys.append(self.getSeqIdLabel(seqId))

      # Initialise the sequence index (this is the actual, non-gapped sequence!)
      # Note that this STARTS AT 1!!
      seqCode = 0

      self.dmp.xLists.append([])
      self.dmp.yLists.append([])

      self.dmp.slopesLists.append([])
      self.dmp.averageLists.append([])
      
      self.dynaMineValuesByAlignment.append([])
      self.dynaMineSlopesByAlignment.append([])

      #
      # Now go over the alignment, but only track the relevant info at the end.
      #
      
      ignoreSequence = False

      for alignSeqIndex in self.alignSeqIndexRange:

        alignChar = alignment[alignSeqIndex]
        alignSeqCode = alignSeqIndex + 1

        # This always has to happen to keep the alignment in sync with the actual sequence
        if alignChar != self.insertCodeFromFile:        
          seqCode += 1            

        # But now ignore what is not relevant for this particular plot
        if alignSeqCode in self.alignSeqCodesToIgnore or alignSeqCode not in self.useSeqAlignRange:
          print "Skipping {}...".format(alignSeqCode)
          continue

        xAxisIndex = self.alignSeqCodeToXAxisIndex[alignSeqCode]

        if alignChar != self.insertCodeFromFile:

          try:
            (aaCode,predS2_value) = self.dmp.predictionData[seqCode]

            if aaCode:
              if not self.bypassBadSequences:
                assert alignChar == aaCode, "Amino acid mismatch in {}: {}.{} vs {}.{}!".format(seqId,alignSeqCode,alignChar,seqCode,aaCode)
              elif alignChar != aaCode:
                print "Amino acid mismatch in {}: {}.{} vs {}.{}! Ignoring.".format(seqId,alignSeqCode,alignChar,seqCode,aaCode)
                ignoreSequence = True
                break
            else:
              # Ignore, from other predictor
              aaCode = alignChar

            slope = slopes[seqCode]
            averagedValue = averages[seqCode]

          except:
            print seqId, alignSeqCode
            print self.dmp.predictionData.keys()
            print seqCode
            if seqCode > 5:
              print "".join([self.dmp.predictionData[si][0] for si in range(seqCode-5,seqCode+6)])
              print alignment[alignSeqCode-5:alignSeqCode+5]
            raise

          self.dynaMineValuesByAlignment[-1].append(predS2_value)
          self.dynaMineSlopesByAlignment[-1].append(slope)
          
          self.dmp.xLists[-1].append(xAxisIndex)
          self.dmp.yLists[-1].append(predS2_value)

          self.dmp.seqIndexToSeqCode[xAxisIndex] = alignSeqCode

          self.dmp.slopesLists[-1].append(slope)
          self.dmp.averageLists[-1].append(averagedValue)

          if xAxisIndex not in self.dmp.aaNames.keys():
            self.dmp.aaNames[xAxisIndex] = aaCode
          elif aaCode not in self.dmp.aaNames[xAxisIndex]:
            self.dmp.aaNames[xAxisIndex] += "/{}".format(aaCode)

          self.dmp.xValues.add(xAxisIndex)
          self.dmp.yValues.add(predS2_value)

          #self.alignSeqCodeToActualSeqCode[seqId][alignSeqCode] = seqCode
          #alignSeqCodeAaInfo[alignSeqCode]['total'] += 1
          #if aaCode not in alignSeqCodeAaInfo[alignSeqCode]['uniqueAa']:
          #  alignSeqCodeAaInfo[alignSeqCode]['uniqueAa'].append(aaCode)

          seqAlignmentInfo[seqId] += aaCode
          aaCodes += aaCode

        else:

          seqAlignmentInfo[seqId] += self.seqLogoGapCode
          self.dynaMineValuesByAlignment[-1].append(None)
          self.dynaMineSlopesByAlignment[-1].append(None)



      if ignoreSequence:
        self.seqIds.remove(seqId)
        self.validSeqIds.pop(-1)
        del(seqAlignmentInfo[seqId])
        self.dmp.colourKeys.pop(-1)
        self.dmp.xLists.pop(-1)
        self.dmp.yLists.pop(-1)
        
        self.dmp.slopesLists.pop(-1)
        self.dmp.averageLists.pop(-1)

        self.dynaMineValuesByAlignment.pop(-1)
        self.dynaMineSlopesByAlignment.pop(-1)

      else:

        self.infoBySeqId[seqId] = {

          'aaCodes': aaCodes,
          'xValues': self.dmp.xLists[-1],
          'yValues': self.dmp.yLists[-1],
          'slopes': self.dmp.slopesLists[-1],
          'averageValues': self.dmp.averageLists[-1]

        }

    return seqAlignmentInfo

  def analyseFullAlignmentByWindow(self,seqCodeEntropy,windowLength,plotDensityLists):
  
    from pdbe.analysis.Util import getPickledDict, createPickledDict

    
    #
    # Run window over the alignment, get rms values for the central residue. Starts with 'half' window at edges.
    #
    
    """
    
    TODO HERE:
    
    - Remove 10 (or so) N/C terminal residues because of bias there? Need to do that in trackDynaMineValues() probably.
    - Also track ALL values over ALL alignments?! But what does that mean...
    
    """

    xDataTypes = ('cons','covar') # Conservation and covariance
    yDataTypes = ('value','slope')
        
    if windowLength:
      windowKeyword = "window_{}".format(windowLength)
      applyCorrection = True
      halfWindow = windowLength / 2
    else:
      windowKeyword = "absolute"
      applyCorrection = False
      halfWindow = 1
    
    windowGraphPath = os.path.join(self.graphDir,windowKeyword)
    if not os.path.exists(windowGraphPath):
      os.mkdir(windowGraphPath)
    
    #
    # Get window RMS data
    #
    
    picklePath = os.path.join(self.dataDir,"{}_{}.pp".format(self.graphPathIdentifier,windowKeyword))
    
    if os.path.exists(picklePath):

      #
      # Load data from pickle if exists      
      #
      
      pickledDir = getPickledDict(picklePath)
      
      xListInfo = pickledDir['xListInfo']
      yListInfo = pickledDir['yListInfo']
    
   
    else:

      #
      # Otherwise calculate values and pickle
      #

      fullLengthIndex = len(self.alignSeqCodeToXAxisIndex)

      #yDataSubTypes = ('abs','rel') # abs is the absolute Rms value for the residue, rel is the relative one (after superimposition of the window)

      xListInfo = {'numContribs': [], 'xAxisIndex': [], 'infoStartCode': None} # This initialised separately
      yListInfo = {}
      for dataType in xDataTypes:
        xListInfo[dataType] = []
        yListInfo[dataType] = {}
        for yDataType in yDataTypes:
          yListInfo[dataType][yDataType] = []
          #for yDataSubType in yDataSubTypes:
          #  yListInfo[dataType][yDataType][yDataSubType] = []

      #xListNumberContributions = []
      #yList = []
      #yListAbs = []
      #xList_covar = []
      #yList_covar = []
      #yList_covarAbs = []

      # This was to produce a 'minimized/averaged graph, but is too confusing!
      #dmpReplace_yLists = []
      #for i in range(len(self.dmp.yLists)):
      #  dmpReplace_yLists.append([])

      # Need to make sure that the RMS codes track the full sequence!    
      xAxisValues = list(self.dmp.xValues)
      xAxisValues.sort()

      seqCodeEntropyIndex = 1

      for alignSeqIndex in self.alignSeqIndexRange:
        alignSeqCode = alignSeqIndex + 1

        # Here ignoring shared gaps in the sequence; possible when using a subset of the full alignment during filtering
        if alignSeqCode in self.alignSeqCodesToIgnore or alignSeqCode not in self.useSeqAlignRange:
          continue

        listIndex = self.alignSeqCodeToValidResidueIndex[alignSeqCode]
        xAxisIndex = self.alignSeqCodeToXAxisIndex[alignSeqCode]

        if xListInfo['infoStartCode'] == None:
          xListInfo['infoStartCode'] = xAxisIndex

        # Get sequence entropy value
        entropyValue = seqCodeEntropy[seqCodeEntropyIndex]
        seqCodeEntropyIndex += 1

        # Local covariation
        localCovariation = None
        if alignSeqCode in self.refSeqIdCodes.keys():
          localCovarSeqCode = self.refSeqIdCodes[alignSeqCode]
          if localCovarSeqCode in self.seqCodeLocalCovariation.keys():
            localCovariation = self.seqCodeLocalCovariation[localCovarSeqCode]

        # Create the list of values for superimposing
        wSeqIndexStart = max(0,listIndex-halfWindow)
        wSeqIndexEnd = min(fullLengthIndex,listIndex+halfWindow)

        # Getting the predictions out
        plotPredLists = [dynaMineValues[wSeqIndexStart:wSeqIndexEnd] for dynaMineValues in self.dynaMineValuesByAlignment]
        plotPredSlopes = [slopeValues[wSeqIndexStart:wSeqIndexEnd] for slopeValues in self.dynaMineSlopesByAlignment]

        # Determine the central index; doublechecked, seems correct, is really only necessary at beginning of sequence!
        centralIndex = min(listIndex - wSeqIndexStart,len(plotPredLists[-1])-1)

        # Now determine minimization for each (do all residues for now)
        try:
          #(plotPredListsMinimized,corrections,rmsAbsolutePerPlot,rmsAbsolutePerResidue) = self.superimposePlots(plotPredLists,onlyResidueIndexes=[centralIndex])
          (plotPredListsMinimized,corrections,rmsPerPlot,rmsPerResidue) = self.superimposePlots(plotPredLists,onlyResidueIndexes=[centralIndex],applyCorrection=applyCorrection)

          #(plotPredSlopesMinimized,correctionsSlopes,rmsAbsolutePerPlotSlopes,rmsAbsolutePerResidueSlopes) = self.superimposePlots(plotPredSlopes,onlyResidueIndexes=[centralIndex],applyCorrection=False)
          (plotPredSlopesMinimized,correctionsSlopes,rmsPerPlotSlopes,rmsPerResidueSlopes) = self.superimposePlots(plotPredSlopes,onlyResidueIndexes=[centralIndex],applyCorrection=applyCorrection)
        except:
          print plotPredLists
          print plotPredSlopes
          print halfWindow
          print fullLengthIndex
          print centralIndex
          raise

        #
        # Set the x and y values for the box plots
        #

        xListInfo['cons'].append(entropyValue)
        xListInfo['numContribs'].append(len(plotPredLists))  # Track number of points that contributed, important for box plots, don't want single points in there, skews plot!
        xListInfo['xAxisIndex'].append(xAxisIndex)

        yListInfo['cons']['value'].append(rmsPerResidue[0])            # This is the rms variation of the predicted values to the average value for the central residue AFTER superimposition (correction)
        #yListInfo['cons']['value']['abs'].append(rmsAbsolutePerResidue[0]) # This is the rms variation of the predicted values to the average value for the central residue without applying a correction
        yListInfo['cons']['slope'].append(rmsPerResidueSlopes[0])            # This is the rms variation of the predicted values to the average value for the central residue AFTER superimposition (correction)
        #yListInfo['cons']['slope']['abs'].append(rmsAbsolutePerResidueSlopes[0]) # This is the rms variation of the predicted values to the average value for the central residue without applying a correction

        if localCovariation != None:
          xListInfo['covar'].append(localCovariation)

          yListInfo['covar']['value'].append(rmsPerResidue[0])            # This is the rms variation of the predicted values to the average value for the central residue AFTER superimposition (correction)
          #yListInfo['covar']['value']['abs'].append(rmsAbsolutePerResidue[0]) # This is the rms variation of the predicted values to the average value for the central residue without applying a correction
          yListInfo['covar']['slope'].append(rmsPerResidueSlopes[0])            # This is the rms variation of the predicted values to the average value for the central residue AFTER superimposition (correction)
          #yListInfo['covar']['slope']['abs'].append(rmsAbsolutePerResidueSlopes[0]) # This is the rms variation of the predicted values to the average value for the central residue without applying a correction

        # Create a 'minimized' version of the plot: individual profiles not retained, but gives an averaged view
        #for i in range(len(self.dmp.yLists)):
        #  yValue = plotPredListsMinimized[i][centralIndex]
        #  if yValue != None:
        #    dmpReplace_yLists[i].append(yValue)

        pickledDir = {'xListInfo': xListInfo, 'yListInfo': yListInfo}
        
        createPickledDict(picklePath,pickledDir)

    #
    # Plot float comparison graphs and box plots
    #

    if self.plotsOn:
      
      #
      # Plot rms for window rms profile and gradient of profile with respect to each other
      #
    
      plotGraph("{}/{}_cons_rmsValueSlope.png".format(windowGraphPath,self.graphPathIdentifier),yListInfo['cons']['value'],yListInfo['cons']['slope'],
                main="{} {}".format(self.analysisName,self.graphTitleText),xlab="RMS profile",ylab='RMS slope',pointSize = 'small')
   
      #
      # Produce float comparison and box plot for conservation/covariation
      #

      for xDataType in xDataTypes:
        if not xListInfo[xDataType]:
          continue

        if xDataType == 'cons':
          xLabel = 'Conservation'
        elif xDataType == 'covar':
          xLabel = 'Local covariation'

        boxPlotLabels = list(set(xListInfo[xDataType]))
        boxPlotLabels.sort()

        boxPlotContribNumberPoints = list(set(xListInfo['numContribs']))
        boxPlotMaxContrib = max(xListInfo['numContribs'])

        for yDataType in yDataTypes:
          #for yDataSubType in yDataSubTypes:

            if not windowLength:
              yDataSubLabel = 'absolute'
            else:
              yDataSubLabel = 'relative'

            # Float plot
            plotGraph("{}/{}_{}_{}.png".format(windowGraphPath,self.graphPathIdentifier,xDataType,yDataType),xListInfo[xDataType],yListInfo[xDataType][yDataType],
                      main="{} {}".format(self.analysisName,self.graphTitleText),xlab=xLabel,ylab='DynaMine {} {} RMS'.format(yDataSubLabel,yDataType),
                      pointSize = 'small')

            # Do the box plot only for conservation, initialise
            if xDataType == 'cons':
              subTypes = ('all','block')
              boxPlotData = {}
              for subType in subTypes:
                boxPlotData[subType] = []

              for label in boxPlotLabels:
                for subType in subTypes:
                  boxPlotData[subType].append([])

              # Put values in; don't count single points, also track aligned blocks only
              for i in range(len(yListInfo[xDataType][yDataType])):
                if xListInfo['numContribs'][i] > 1:
                  boxPlotIndex = boxPlotLabels.index(xListInfo[xDataType][i])    
                  boxPlotData['all'][boxPlotIndex].append(yListInfo[xDataType][yDataType][i])      

                  if xListInfo['numContribs'][i] == boxPlotMaxContrib:
                    boxPlotData['block'][boxPlotIndex].append(yListInfo[xDataType][yDataType][i])      

              # Plot box plots
              boxPlotGraphLabels = ["{}".format(boxPlotLabel) for boxPlotLabel in boxPlotLabels]
              for subType in subTypes:
                r.bitmap("{}/{}_box_{}_{}_{}.png".format(windowGraphPath,self.graphPathIdentifier,xDataType,yDataType,subType))
                r.boxplot(boxPlotData[subType], las = 2,  names = boxPlotGraphLabels, title="Window size {}".format(windowLength)) # par(mar = c(12, 5, 4, 2)+ 0.1),
                r.dev_off()

      #
      #
      #
      #
      # TODO: Also superimpose graphs somehow. Use average value, somehow preserve individual profile as much as possible?
      # Or just use average value, correct each point based on the superimposition, plot that. Should be best to give consensus view.
      #
      #
      #

      plotDensityStep = 1
      plotDensityTexts = ['AA conservation','local covariation']    

      #yDataTypes = ('value','slope') Could do slope as well but need different colour coding (and no main graph in that case)
      perResidueRmsCodes = self.getWindowResidueRmsColourCodes(xListInfo['xAxisIndex'],xListInfo['numContribs'],yListInfo['cons']['value'])

      perResidueRmsCodesText = "".join(perResidueRmsCodes[1:])

      colourKeys = self.dmp.colourKeys
      self.dmp.colourKeys = []

      self.dmp.writePlot("{}/{}_rms.png".format(windowGraphPath,self.graphPathIdentifier),
                        "{} {}".format(self.analysisName,self.graphTitleText),
                        infoCodes=perResidueRmsCodesText,
                        plotDensityLists=plotDensityLists,
                        plotDensityStep=plotDensityStep,
                        plotDensityTexts=plotDensityTexts,
                        minElementLength=1,infoStartCode=xListInfo['infoStartCode'],
                        xAxisLabelSeqOffset=self.xAxisLabelSeqOffset,
                        yAxisLabel=self.yAxisLabel)


#      yListsOrig = self.dmp.yLists
#      self.dmp.yLists = dmpReplace_yLists
#
#      self.dmp.writePlot("{}/{}_full_rms_min_{}.png".format(self.graphDir,self.graphPathIdentifier,windowLength),
#                        "{} {}".format(self.analysisName,self.graphTitleText),
#                        infoCodes=perResidueRmsCodesText,
#                        plotDensityLists=plotDensityLists,
#                        plotDensityStep=plotDensityStep,
#                        plotDensityTexts=plotDensityTexts,
#                        minElementLength=1,infoStartCode=infoStartCode,
#                        xAxisLabelSeqOffset=self.xAxisLabelSeqOffset,
#                        yAxisLabel=self.yAxisLabel)
#
#
#      self.dmp.yLists = yListsOrig

      self.dmp.colourKeys = colourKeys

  def getDensityLists(self,seqCodeEntropy,seqCodeLocalCovariation):
  
    plotDensityLists = [[],[]]

    seqCodeEntropyIndex = 1
    for alignSeqIndex in self.alignSeqIndexRange:
      alignSeqCode = alignSeqIndex + 1
      if alignSeqCode in self.alignSeqCodesToIgnore or alignSeqCode not in self.useSeqAlignRange:
        continue

      # Sequence entropy
      plotDensityLists[0].extend([self.alignSeqCodeToXAxisIndex[alignSeqCode]] * int(10 * seqCodeEntropy[seqCodeEntropyIndex]))        
      seqCodeEntropyIndex += 1
      
      # Local covariation
      if alignSeqCode in self.refSeqIdCodes.keys():
        localCovarSeqCode = self.refSeqIdCodes[alignSeqCode]
        if localCovarSeqCode in seqCodeLocalCovariation.keys() and seqCodeLocalCovariation[localCovarSeqCode] and not math.isnan(seqCodeLocalCovariation[localCovarSeqCode]):
          plotDensityLists[1].extend([self.alignSeqCodeToXAxisIndex[alignSeqCode]] * int(10 * seqCodeLocalCovariation[localCovarSeqCode]))

    return plotDensityLists

  def plotIndividualGraph(self,plotDensityLists,seqInsertInfo):

    # Make fake density data for the x-axis entropy scale

    #assert False, "HERE have to fix the alignSeqCode stuff coming from seqCodeEntropy - has to be aligned with self.dmp.xValues, use self.alignSeqCodeToActualSeqIndex or inverse!"

    plotDensityStep = 1
    plotDensityTexts = ['AA conservation','local covariation']

    self.dmp.writePlot("{}/{}.png".format(self.graphDir,self.graphPathIdentifier),
                  "{} {}".format(self.analysisName,self.graphTitleText),
                  infoCodes=seqInsertInfo,
                  plotDensityLists=plotDensityLists,
                  plotDensityStep=plotDensityStep,
                  plotDensityTexts=plotDensityTexts,
                  xAxisLabelSeqOffset=self.xAxisLabelSeqOffset,
                  graphPathSlopes="{}/{}_slope.png".format(self.graphDir,self.graphPathIdentifier),
                  #graphPathAverages="{}/{}_avg.png".format(self.graphDir,self.graphPathIdentifier),
                  yAxisLabel=self.yAxisLabel)
                  

    xValues = [item for sublist in self.dmp.slopesLists for item in sublist]
    yValues = [item for sublist in self.dmp.yLists for item in sublist]
    
    
    if self.dataType == 'backbone':
      ylim = (0.2,1.2)
      xlim = (-0.2,0.2)
    else:
      ylim = (0.0,0.8)
      xlim = (-0.3,0.3)

    plotGraph("{}/{}_valueSlope.png".format(self.graphDir,self.graphPathIdentifier),xValues,yValues,
              main="{} {} {} values vs slope".format(self.analysisName,self.graphTitleText,self.dataType),xlab='DynaMine profile slope',ylab="DynaMine value",
              pointSize = 'small',ylim=ylim,xlim=xlim)
    
  def plotSharedRegionGraphs(self):
      
    # Hacking into dmp to get this organised

    self.customAxisLabels = {}

    # Reset dmp info
    self.dmp.xLists = []
    self.dmp.yLists = []

    self.dmp.slopesLists = []
    self.dmp.averageLists = []

    self.dmp.xValues = []
    yListsMin = []

    for seqId in self.validSeqIds:      
      self.dmp.xLists.append([])
      self.dmp.yLists.append([])
      yListsMin.append([])

      self.dmp.slopesLists.append([])
      self.dmp.averageLists.append([])

    # Now loop over all the blocks...
    relativeSeqIndex = 0
    rmsInfo = ""
    absInfo = ""
    blockRmsInfo = ""
    seqAlignmentInfo = {}
    for seqId in self.validSeqIds:
      seqAlignmentInfo[seqId] = ""
      
    blockAlignSeqCodes = {}
    
    if len(self.alignedSeqCodeBlocks) == 1:
      oneBlockOnly = True
    else:
      oneBlockOnly = False

    for alignedSeqCodesInBlock in self.alignedSeqCodeBlocks:

      # Set up info for the plots
      plotPredLists = []
      for seqId in self.validSeqIds:
        plotPredLists.append([])

      # Track first/last valid relativeSeqIndex and alignedSeqCode pair
      startSeqCodeInfo = None
      endSeqCodeInfo = None          
      valuesAvailable = 0

      for alignSeqCode in alignedSeqCodesInBlock:

        if alignSeqCode in self.alignSeqCodesToIgnore or alignSeqCode not in self.useSeqAlignRange:
          continue

        valuesAvailable += 1

        # This relative sequence index relates to the x position in the blocked plots!
        relativeSeqIndex += 1

        if not startSeqCodeInfo:
          startSeqCodeInfo = (relativeSeqIndex,alignSeqCode)
        endSeqCodeInfo = (relativeSeqIndex,alignSeqCode)  # Just continually set this; last value will be relevant one
        
        blockAlignSeqCodes[relativeSeqIndex] = alignSeqCode

        for i in range(len(self.validSeqIds)):
          seqId = self.validSeqIds[i]

          self.dmp.xLists[i].append(relativeSeqIndex)

          valueIndexForSeqId = self.infoBySeqId[seqId]['xValues'].index(self.alignSeqCodeToXAxisIndex[alignSeqCode])
          predValue = self.infoBySeqId[seqId]['yValues'][valueIndexForSeqId]
          slope = self.infoBySeqId[seqId]['slopes'][valueIndexForSeqId]
          averageValue = self.infoBySeqId[seqId]['averageValues'][valueIndexForSeqId]

          self.dmp.yLists[i].append(predValue)              
          plotPredLists[i].append(predValue)

          self.dmp.slopesLists[i].append(slope)
          self.dmp.averageLists[i].append(averageValue)

          seqAlignmentInfo[seqId] += self.infoBySeqId[seqId]['aaCodes'][valueIndexForSeqId]

        self.dmp.xValues.append(relativeSeqIndex)

      # Only do something if info available!
      if not valuesAvailable:
        continue

      # x-axis label for start/end of block x-axis
      self.setCustomAxisLabel(startSeqCodeInfo[0],startSeqCodeInfo[1])
      self.setCustomAxisLabel(endSeqCodeInfo[0],endSeqCodeInfo[1])
      if oneBlockOnly:
        seqLen = endSeqCodeInfo[1] - startSeqCodeInfo[1] + 1 
        for indexCut in (200,100,50,20,10,5):
          if (seqLen - 50) / indexCut > 10:
            break
        
        curSeqCode = 0
        while curSeqCode < endSeqCodeInfo[1]:
          if curSeqCode > startSeqCodeInfo[1]:
            labelIndex = curSeqCode -  startSeqCodeInfo[1] + startSeqCodeInfo[0]            
            self.setCustomAxisLabel(blockAlignSeqCodes[labelIndex],curSeqCode)
          
          curSeqCode += indexCut
      
      # Now determine minimization for each (do all residues for now)
      (plotPredListsMinimized,corrections,rmsPerPlot,rmsPerResidue) = self.superimposePlots(plotPredLists)

      for i in range(len(self.validSeqIds)):
        yListsMin[i].extend(plotPredListsMinimized[i])

      # Info for this block
      absCode = self.getCorrectionColourCode(corrections)
      absInfo += absCode * len(rmsPerResidue)

      # Redo based on superimposed graphs to get better idea of local variation - this will recalc the average
      (void,void,rmsPerPlot,rmsPerResidue) = self.superimposePlots(plotPredListsMinimized,applyCorrection=False)
      blockRmsCode = self.getBlockRmsColourCode(rmsPerPlot)
      blockRmsInfo += blockRmsCode * len(rmsPerResidue)

      rmsInfo += self.getResidueRmsColourCodes(rmsPerResidue)

      # Add some space between blocks
      if alignedSeqCodesInBlock != self.alignedSeqCodeBlocks[-1]:
        relativeSeqIndex += 3
        rmsInfo += "   "
        absInfo += "   "
        blockRmsInfo += "   "

        for seqId in self.validSeqIds:
          seqAlignmentInfo[seqId] += self.seqLogoGapCode * 3

    # Leave a hole between conserved blocks!
    # Label with customAxisLabels!
    self.dmp.colourKeys = []

    allAlignFile = "{}/seqLogoCons_{}.align".format(self.alignmentsDir,self.graphPathIdentifier)
    self.writeSeqs(allAlignFile,seqAlignmentInfo)
    seqCodeEntropy = self.dmp.writeSequenceLogo(allAlignFile,
                                                "{}/seqLogoCons_{}.png".format(self.graphDir,self.graphPathIdentifier),
                                                "{} {}".format(self.analysisName,self.graphTitleText),
                                                seqLen=len(seqAlignmentInfo[self.seqIds[0]]),
                                                resolution=600,
                                                logoHeight = 400)

    alignSeqCodes = seqCodeEntropy.keys()
    alignSeqCodes.sort()

    plotDensityLists = [[],[]]
    
    for alignSeqCode in alignSeqCodes:
      plotDensityLists[0].extend([alignSeqCode] * int(10 * seqCodeEntropy[alignSeqCode]))
      
      if alignSeqCode in blockAlignSeqCodes.keys():
        origAlignSeqCode = blockAlignSeqCodes[alignSeqCode]

        # Local covariation
        if origAlignSeqCode in self.refSeqIdCodes.keys():
          localCovarSeqCode = self.refSeqIdCodes[origAlignSeqCode]
          if localCovarSeqCode in self.seqCodeLocalCovariation.keys() and self.seqCodeLocalCovariation[localCovarSeqCode] and not math.isnan(self.seqCodeLocalCovariation[localCovarSeqCode]):
            plotDensityLists[1].extend([alignSeqCode] * int(10 * self.seqCodeLocalCovariation[localCovarSeqCode]))
    
    if self.plotsOn:
      plotDensityStep = 1
      plotDensityTexts = ['AA conservation','local covariation']

      self.dmp.writePlot("{}/{}_conserved_abs.png".format(self.graphDir,self.graphPathIdentifier),
                    "{} {}, conserved, by correction".format(self.analysisName,self.graphTitleText),
                    infoCodes=absInfo,
                    customAxisLabels=self.customAxisLabels,
                    minElementLength=1,
                    plotDensityLists=plotDensityLists,
                    plotDensityStep=plotDensityStep,
                    plotDensityTexts=plotDensityTexts,
                    graphPathSlopes="{}/{}_conserved_abs_slope.png".format(self.graphDir,self.graphPathIdentifier),
                    #graphPathAverages="{}/{}_conserved_abs_avg.png".format(self.graphDir,self.graphPathIdentifier),
                    yAxisLabel=self.yAxisLabel)

      self.dmp.yLists = yListsMin
      self.dmp.writePlot("{}/{}_conserved_blockRms.png".format(self.graphDir,self.graphPathIdentifier),
                    "{} {}, superimposed blocks, by block RMS".format(self.analysisName,self.graphTitleText),
                    infoCodes=blockRmsInfo,
                    customAxisLabels=self.customAxisLabels,
                    plotDensityLists=plotDensityLists,
                    plotDensityStep=plotDensityStep,
                    plotDensityTexts=plotDensityTexts,
                    yAxisLabel=self.yAxisLabel)

      self.dmp.writePlot("{}/{}_conserved_rms.png".format(self.graphDir,self.graphPathIdentifier),
                    "{} {}, superimposed blocks, by residue RMS".format(self.analysisName,self.graphTitleText),
                    infoCodes=rmsInfo,
                    customAxisLabels=self.customAxisLabels,
                    minElementLength=1,
                    plotDensityLists=plotDensityLists,
                    plotDensityStep=plotDensityStep,
                    plotDensityTexts=plotDensityTexts,
                    yAxisLabel=self.yAxisLabel)

  #
  # Colour codes; bit arbitrary at the moment.
  #
  
  def getCorrectionColourCode(self,corrections):
  
    correctionSummary = r.summary(corrections)
    correctionRange = correctionSummary['Max.'] - correctionSummary['Min.']
    if correctionRange > 0.3:
      absCode = '8'
    elif correctionRange > 0.2:
      absCode = '7'
    elif correctionRange > 0.1:
      absCode = '6'
    elif correctionRange > 0.05:
      absCode = '5'
    elif correctionRange > 0.02:
      absCode = '4'
    else:
      absCode = '3'
      
    return absCode

  def getBlockRmsColourCode(self,rmsPerPlot):
  
    valueSummary = r.summary(rmsPerPlot)
    valueAvg = valueSummary['Max.']
    if valueAvg > 0.3:
      absCode = '8'
    elif valueAvg > 0.25:
      absCode = '7'
    elif valueAvg > 0.2:
      absCode = '6'
    elif valueAvg > 0.15:
      absCode = '5'
    elif valueAvg > 0.1:
      absCode = '4'
    else:
      absCode = '3'
      
    return absCode

  def getResidueRmsColourCodes(self,rmsPerResidue):

    rmsBlockInfo = ""
    
    (firstQ,median,thirdQ,high,vhigh) = self.getCutoffs(rmsPerResidue)

    for residueRms in rmsPerResidue:

      if residueRms > high:
        absCode = '8'
      elif residueRms > thirdQ:
        absCode = '7'
      elif residueRms > thirdQ:
        absCode = '6'
      elif residueRms > median:
        absCode = '5'
      elif residueRms > firstQ:
        absCode = '4'
      else:
        absCode = '3'
        
      rmsBlockInfo += absCode
      
    return rmsBlockInfo

  def getWindowResidueRmsColourCodes(self,xAxisIndexes,xAxisPointCount,residueRmsList):
  
    # Determine range of values, exclude single sequence points
    residueRmsListMultipleSeqs = []
    for i in range(len(residueRmsList)):
      if xAxisPointCount[i] > 1:
        residueRmsListMultipleSeqs.append(residueRmsList[i])
  
    (firstQ,median,thirdQ,high,vhigh) = self.getCutoffs(residueRmsListMultipleSeqs)

    # Initialise colour coding
    perResidueRmsCodes = ["3"] * (max(xAxisIndexes) + 1)
    
    for i in range(len(xAxisIndexes)):
      xAxisIndex = xAxisIndexes[i]
      contributingSeqCount = xAxisPointCount[i]
      residueRms = residueRmsList[i]

      if residueRms > high:
        absCode = '8'
      elif residueRms > thirdQ:
        absCode = '7'
      elif residueRms > thirdQ:
        absCode = '6'
      elif residueRms > median:
        absCode = '5'
      elif residueRms > firstQ:
        absCode = '4'
      else:
        absCode = '3'
      
      perResidueRmsCodes[xAxisIndex] = absCode
       
    return perResidueRmsCodes
    
  def getCutoffs(self,valueList):
  
    summary = r.summary(valueList)
    
    firstQ = summary['1st Qu.']
    median = summary['Median']
    thirdQ = summary['3rd Qu.']
    highDiff = summary['Max.'] - thirdQ
    high = thirdQ + (highDiff) / 2.0
    vhigh = thirdQ + (highDiff) / 1.5
    
    return (firstQ,median,thirdQ,high,vhigh)

  def writeSeqs(self,alignmentFilePath,seqAlignmentInfo):
  
    if not os.path.exists(alignmentFilePath):
      fout = open(alignmentFilePath,'w')

      for seqId in self.seqIds:
        fout.write(">{}\n".format(seqId))
        fout.write("{}\n".format(seqAlignmentInfo[seqId]))

      fout.close()
