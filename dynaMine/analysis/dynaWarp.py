#import random
import itertools, os, numpy, subprocess

from dynaMine.analysis.makePlot import MakeDynaMinePlot
from dynaMine.predictor.parsers import DynaMineFileParsers
from dynaMine.predictor.wrappers import RunDynaMinePrediction


class DynaWarp(DynaMineFileParsers,RunDynaMinePrediction):

  """
  DynaMine based tool to suggest a set of mutations in a sequence that will modify it to match a set of target S2 values at specified
  positions. For all other positions the original S2pred will be maintained as much as possible.
  
  Features:

  - original sequence and name (obligatory)
  - range of positions where mutations can be applied (starting at position 1 for the first residue!)
  - type of mutations to be applied (all, blosum, ...)
  - target S2pred value(s) for set of positions (could come from prediction for e.g. wild type sequence)
  """
  
  aaOneLetterCodes = 'ACDEFGHIKLMNPQRSTVWY'
  
  substitutionMatrices = {
  
    'blosum62': """A  R  N  D  C  Q  E  G  H  I  L  K  M  F  P  S  T  W  Y  V  B  Z  X  *
A  4 -1 -2 -2  0 -1 -1  0 -2 -1 -1 -1 -1 -2 -1  1  0 -3 -2  0 -2 -1  0 -4 
R -1  5  0 -2 -3  1  0 -2  0 -3 -2  2 -1 -3 -2 -1 -1 -3 -2 -3 -1  0 -1 -4 
N -2  0  6  1 -3  0  0  0  1 -3 -3  0 -2 -3 -2  1  0 -4 -2 -3  3  0 -1 -4 
D -2 -2  1  6 -3  0  2 -1 -1 -3 -4 -1 -3 -3 -1  0 -1 -4 -3 -3  4  1 -1 -4 
C  0 -3 -3 -3  9 -3 -4 -3 -3 -1 -1 -3 -1 -2 -3 -1 -1 -2 -2 -1 -3 -3 -2 -4 
Q -1  1  0  0 -3  5  2 -2  0 -3 -2  1  0 -3 -1  0 -1 -2 -1 -2  0  3 -1 -4 
E -1  0  0  2 -4  2  5 -2  0 -3 -3  1 -2 -3 -1  0 -1 -3 -2 -2  1  4 -1 -4 
G  0 -2  0 -1 -3 -2 -2  6 -2 -4 -4 -2 -3 -3 -2  0 -2 -2 -3 -3 -1 -2 -1 -4 
H -2  0  1 -1 -3  0  0 -2  8 -3 -3 -1 -2 -1 -2 -1 -2 -2  2 -3  0  0 -1 -4 
I -1 -3 -3 -3 -1 -3 -3 -4 -3  4  2 -3  1  0 -3 -2 -1 -3 -1  3 -3 -3 -1 -4 
L -1 -2 -3 -4 -1 -2 -3 -4 -3  2  4 -2  2  0 -3 -2 -1 -2 -1  1 -4 -3 -1 -4 
K -1  2  0 -1 -3  1  1 -2 -1 -3 -2  5 -1 -3 -1  0 -1 -3 -2 -2  0  1 -1 -4 
M -1 -1 -2 -3 -1  0 -2 -3 -2  1  2 -1  5  0 -2 -1 -1 -1 -1  1 -3 -1 -1 -4 
F -2 -3 -3 -3 -2 -3 -3 -3 -1  0  0 -3  0  6 -4 -2 -2  1  3 -1 -3 -3 -1 -4 
P -1 -2 -2 -1 -3 -1 -1 -2 -2 -3 -3 -1 -2 -4  7 -1 -1 -4 -3 -2 -2 -1 -2 -4 
S  1 -1  1  0 -1  0  0  0 -1 -2 -2  0 -1 -2 -1  4  1 -3 -2 -2  0  0  0 -4 
T  0 -1  0 -1 -1 -1 -1 -2 -2 -1 -1 -1 -1 -2 -1  1  5 -2 -2  0 -1 -1  0 -4 
W -3 -3 -4 -4 -2 -2 -3 -2 -2 -3 -2 -3 -1  1 -4 -3 -2 11  2 -3 -4 -3 -2 -4 
Y -2 -2 -2 -3 -2 -1 -2 -3  2 -1 -1 -2 -1  3 -3 -2 -2  2  7 -1 -3 -2 -1 -4 
V  0 -3 -3 -3 -1 -2 -2 -3 -3  3  1 -2  1 -1 -2 -2  0 -3 -1  4 -3 -2 -1 -4 
B -2 -1  3  4 -3  0  1 -1  0 -3 -4  0 -3 -3 -2  0 -1 -4 -3 -3  4  1 -1 -4 
Z -1  0  0  1 -3  3  4 -2  0 -3 -3  1 -1 -3 -1  0 -1 -3 -2 -2  1  4 -1 -4 
X  0 -1 -1 -1 -2 -1 -1 -1 -1 -1 -1 -1 -1 -1 -2  0  0 -2 -1 -1 -1 -1 -1 -4 
* -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  1"""

  }
    
  dataTypes = ('backbone','helix','sheet')

  def warpSequence(self,runIdentifier,workDir,origSequence,mutationPositions,targetList,
                        mutationNumber=1,mutationScoreCutoff=0,mutationType='all',
                        plotGraphs=False):
  
    self.origSequence = origSequence
    self.runIdentifier = runIdentifier
    
    # Set up mutation scores
    if mutationType != 'all':
      self.readMutationType(mutationType)
    else:
      self.setupAllMutations()
    
    # Set up working directory
    if not os.path.exists(workDir):
      os.mkdir(workDir)
      
    # Create mutated sequences; do all combinations possible
    mutationIndexes = [mutationPos - 1 for mutationPos in mutationPositions]

    # First determine the possible mutations for each position
    mutationPossibilities = {}
    for mutationIndex in mutationIndexes:
      mutationPossibilities[mutationIndex] = ""
      origAa = origSequence[mutationIndex]
      for newAa in self.substitutionScores[origAa].keys():
        if newAa != origAa and self.substitutionScores[origAa][newAa] >= mutationScoreCutoff:
          mutationPossibilities[mutationIndex] += newAa
              
    # Then combine and track the mutated sequences
    allMutationCombinations = itertools.combinations(mutationIndexes,mutationNumber)
    mutatedSequences = []

    for mutationCombination in allMutationCombinations:
      mutationAas = []
      origAas = ""
      for mutationIndex in mutationCombination:
        mutationAas.append(mutationPossibilities[mutationIndex])
        origAas += origSequence[mutationIndex]
      mutationAaCombs = itertools.product(*mutationAas) # Make all aa combinations here for the different positions
      
      for mutationAaComb in mutationAaCombs:
        mutationIdList = []
        mutatedSequenceAaList = list(origSequence)
        for relIndex in range(mutationNumber):
          mutationIndex = mutationCombination[relIndex]
          mutationAa = mutationAaComb[relIndex]
          mutationIdList.append("{}{}{}".format(origAas[relIndex],mutationIndex+1,mutationAa))
          mutatedSequenceAaList[mutationIndex] = mutationAa

        mutatedSequence = "".join(mutatedSequenceAaList)
        mutationId = "_".join(mutationIdList)
        
        mutatedSequences.append((mutationId,mutatedSequence))
    
    # Now write out a fasta file with the original and all mutated sequences, and run dynaMine!

    fastaFileName = os.path.join(workDir,"{}.fasta".format(runIdentifier))
    
    if not os.path.exists(fastaFileName):
      self.writeFasta(fastaFileName,[(runIdentifier,origSequence)] + mutatedSequences)
      
    resultsDir = os.path.join(workDir,runIdentifier)
    
    # Will return filenames even if already ran the prediction
    fileNames = self.runDynaMineAll(fastaFileName,runIdentifier,workDir=resultsDir,separateSsElements=True)
  
    # Analyse the results - which one gives values closest to the original sequence S2preds modified by targetList?
    
        
    # Read predictions for all sequences
    
    predictionValues = self.readPredictionValues(resultsDir,fileNames)
    
    origValues = self.getPredictionValues(predictionValues,runIdentifier)
    
    # Modify them based on info in targetList - always use backbone, but not coil!
    targetValues = {}
    for dataType in self.dataTypes:
      targetValues[dataType] = origValues[dataType][:]
      if dataType in targetList.keys():
        for (seqPos,targetValue) in targetList[dataType]:
          targetValues[dataType][seqPos - 1] = targetValue

    # Now check RMS per mutated one
    mutationRmse = {}
    for (mutationId,mutatedSequence) in mutatedSequences:
      mutatedValues = self.getPredictionValues(predictionValues,mutationId)
      
      # TODO here put higher weight on the targetS2List information? Not really I think, everything has to match after all.
      rmse = 0.0
      for dataType in self.dataTypes:
        rmse += numpy.sqrt(((mutatedValues[dataType] - targetValues[dataType]) ** 2).mean())

      if rmse not in mutationRmse.keys():
        mutationRmse[rmse] = []
      mutationRmse[rmse].append(mutationId)
    
    rmseValues = mutationRmse.keys()
    rmseValues.sort()
    
    bestMutated = []
    for rmseValue in rmseValues:
      print rmseValue, mutationRmse[rmseValue]
      
      if len(bestMutated) < 6:
        bestMutated.extend(mutationRmse[rmseValue])

    # Plot the best ones in comparison to the original, the desired and the best mutations. Interesting for visualisation purposes.
    if plotGraphs:

      for dataType in self.dataTypes:
        
        dmp = MakeDynaMinePlot()
        dmp.setPredictionData(predictionValues[dataType][runIdentifier])
        dmp.addPredictionData(runIdentifier)
        dmp.addOtherData("desired",range(1,len(origSequence)+1),[(None,targetValue) for targetValue in targetValues[dataType]])
      
        for mutationId in bestMutated:
          dmp.setPredictionData(predictionValues[dataType][mutationId])          
          dmp.addPredictionData(mutationId) 
          
        if dataType == 'backbone':
          yAxisLabel = '{} rigidity prediction'.format(dataType)
        else:
          yAxisLabel = '{} propensity prediction'.format(dataType)

        dmp.writePlot(os.path.join(workDir,"mutations_{}_{}.png".format(runIdentifier,dataType)),"Predicted {} scores for {} and mutations".format(dataType,runIdentifier),
                                            yAxisLabel=yAxisLabel)

  def readPredictionValues(self,resultsDir,fileNames):
  
    #
    # Read the prediction values
    #
    
    predictionValues = {}
    
    for dataType in self.dataTypes:
      filePath = os.path.join(resultsDir,fileNames[dataType][0])
      
      predictionValues[dataType] = self.readDynaMineMultiEntry(filePath)
      
    return predictionValues
  
  def getPredictionValues(self,predictionValues,seqId):
  
    seqIdValues = {}
    for dataType in self.dataTypes:
      predInfoForSequence = predictionValues[dataType][seqId]
      seqIdValues[dataType] = numpy.array([predInfo[1] for predInfo in predInfoForSequence]) # Take out the value only, ignore seqIndex and amino acid code
    return seqIdValues    
  
  def readMutationType(self,mutationType):
  
    self.substitutionScores = {}
    
    dataLines = self.substitutionMatrices[mutationType].split("\n")
    toSubst = dataLines[0].split()
    
    for dataLine in dataLines[1:]:
      cols = dataLine.split()
      if cols[0] in "BZX*":
        continue
      self.substitutionScores[cols[0]] = {}
      for i in range(len(toSubst)):
        if toSubst[i] in "BZX*":
          continue
        score = int(cols[1 + i])
        self.substitutionScores[cols[0]][toSubst[i]] = score
        
  def setupAllMutations(self):
  
    self.substitutionScores = {}
    
    for aaName in self.aaOneLetterCodes:
      self.substitutionScores[aaName] = {}
      for toSubstAaName in self.aaOneLetterCodes:
        self.substitutionScores[aaName][toSubstAaName] = 9
      
if __name__ == '__main__':

  dynaWarp = DynaWarp()
  
  """
  # Some test data
  sequence = "AQSVPWGISRVQAPAAHNRGLTGSGVKVAVLDTGISTHPDLNIRGGASFVPGEPSTQDGNGHGTHVAGTIAALNNSIGVLGVAPSAELYAVKVLGASGSGSVSSIAQGLEWAGNNGMHVANLSLGSPSPSATLEQAVNSATSRGVLVVAASGNSGAGSISYPARYANAMAVGATDQNNNRASFSQYGAGLDIVAPGVNVQSTYPGSTYASLNGTSMATPHVAGAAALVKQKNPSWSNVQIRNHLKNTATSLGSTNLYGSGLVNAEAATR"
  seqId = '1svn_leonardo'
  mutationPos = [19,20,21,22,25,26,27,28]
  targetS2List = [(23,0.75),(24,0.75)]
  """
  sequence = "MAYSEKVIDHYENPRNVGSFDNNDENVGSGMVGAPACGDVMKLQIKVNDEGIIEDARFKTYGCGSAIASSSLVTEWVKGKSLDEAQAIKNTDIAEELELPPVKIHCSILAEDAIKAAIADYKSKREAK"
  seqId = 'IscU_run2'
  mutationPos = [71,75,78]   #S71,E75,R78 Could be a charge thing here though!
  targetList = {'helix': [(73,0.4),(74,0.45),(75,0.5),(76,0.45),(77,0.4)],
                'sheet': [(73,0.2),(74,0.2),(75,0.2),(76,0.2),(77,0.2)]}

  mutationNumber = 2 # This is the number of positions to be mutated, so if you've got three and select two only two at a time will change!
  mutationType='blosum62'

  dynaWarp.warpSequence(seqId,"./local",sequence,mutationPos,targetList,mutationNumber=mutationNumber,mutationType=mutationType,plotGraphs=True)
