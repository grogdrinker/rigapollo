
try:
  import rpy
except:
  import rpy2.rpy_classic as rpy
  rpy.set_default_mode(rpy.BASIC_CONVERSION)

class MakeDynaMinePlot:

  """

  This code deals with plotting full sequences.
  Only contains core methods, for development see makePredictionPlot

  """

  def __init__(self):
  
    self.initialiseVars()

    # Hack to import from local code Wim if available, will need to sort this out eventually!
    try:

      from pdbe.analysis.Graphs import plotGraphMultiColour, plotGraph, plotHisto
      from memops.universal.Util import frange

    except:
      from dynaMine.analysis.Util import plotGraphMultiColour, plotGraph, plotHisto, frange

    self.plotGraphMultiColour = plotGraphMultiColour
    self.plotGraph = plotGraph
    self.plotHisto = plotHisto
    self.frange = frange
    
  def initialiseVars(self):

    self.xLists = []
    self.yLists = []

    self.slopesLists = []
    self.averageLists = []

    self.xValues = set()
    self.yValues = set()

    self.aaNames = {}
    self.seqIndexToSeqCode = {}

    self.colourKeys = []

    self.predictionData = {}
  
  def readPredictionData(self,filePath,startIndex=1):

    #
    # Read the prediction data
    #

    fin = open(filePath)
    lines = fin.readlines()
    fin.close()

    seqIndex = startIndex

    for line in lines:
    
      if line.startswith("*"):
        continue

      cols = line.split()

      if cols:
        (aaCode,predS2) = cols

        self.predictionData[seqIndex] = (aaCode,float(predS2))

        seqIndex += 1

  def setPredictionData(self,predictionData):
  
    #
    # This is to set prediction data read in with DynaMineFileParsers method readDynaMineMultiEntry() for new multi-entry files
    #

    for seqIndex in range(len(predictionData)):
      self.predictionData[seqIndex + 1] = predictionData[seqIndex]
  
  def addPredictionData(self,keyName,offSet=0,normalise=False):

    #
    # Add the prediction data to the plot
    #

    self.colourKeys.append(keyName)

    seqIndexes = self.predictionData.keys()
    seqIndexes.sort()

    self.xLists.append([])
    self.yLists.append([])
    
    # Normalise values between 0 and 1
    if normalise and seqIndexes:
      yValues = [self.predictionData[seqIndex][1] for seqIndex in seqIndexes]
      maxValue = max(yValues)
      minValue = min(yValues)
      normFactor = 1.0 / (maxValue - minValue)      

    # seqIndex here starts at 1!
    for seqIndex in seqIndexes:

      seqCode = seqIndex + offSet

      (aaCode,predS2_value) = self.predictionData[seqIndex]

      self.xLists[-1].append(seqIndex)
      
      if not normalise:
        yValue = predS2_value
      else:
        yValue = (predS2_value - minValue) * normFactor
      self.yLists[-1].append(yValue)

      self.aaNames[seqIndex] = aaCode
      self.xValues.add(seqIndex)
      self.yValues.add(yValue)

      self.seqIndexToSeqCode[seqIndex] = seqCode

  def addPredictionDataAsOther(self,dataType):
  
    # This is for adding data read in as normal prediction info as 'other' data, with identifier
    seqIndexes = self.predictionData.keys()
    seqIndexes.sort()
    values = [self.predictionData[seqIndex] for seqIndex in seqIndexes]

    self.addOtherData(dataType,seqIndexes,values)
    
  def addOtherData(self,keyName,seqIndexes,dataValues,offSet=0,scaling=1.0):

    #
    # Add other (non-DynaMine) data to the plot
    #

    # Note: has to be same scale or need to re-scale!
    self.colourKeys.append(keyName)

    self.xLists.append([])
    self.yLists.append([])

    for i in range(len(seqIndexes)):

      self.xLists[-1].append(seqIndexes[i] + offSet)
      self.yLists[-1].append(dataValues[i][1] * scaling)

  def setSlopesAndAvg(self,smoothed=False):

    #
    # Get out information on slopes and averages - currently 3 residue window
    #

    from dynaMine.analysis.Util import getSplineStats

    predictionData = self.predictionData

    seqCodes = predictionData.keys()
    seqCodes.sort()

    firstSeqCode = seqCodes[0]
    lastSeqCode = seqCodes[-1]

    dataValues = [self.predictionData[seqCode][1] for seqCode in seqCodes]
    numDataValues = len(dataValues)
    
    (firstDerivatives,secondDerivatives) = getSplineStats(dataValues,smoothed=smoothed) # runSecondDerivative = True to get second as well, can use below!

    slopes = {}
    averages = {}

    slopes[firstSeqCode] = firstDerivatives[0]
    averages[firstSeqCode] = sum(dataValues[:2]) / 2.0

    for dataIndex in range(1,numDataValues):
      slopes[seqCodes[dataIndex]] = firstDerivatives[dataIndex]
      averages[seqCodes[dataIndex]] = sum(dataValues[dataIndex-1:dataIndex+2]) / 3.0

    slopes[lastSeqCode] = firstDerivatives[-1]
    averages[lastSeqCode] = sum(dataValues[-2:]) / 2.0

    #for seqCode in seqCodes:
    #  print seqCode, predictionData[seqCode], slopes[seqCode], averages[seqCode]

    return (slopes, averages)

  def applyAveragingWindow(self,dataKey="3-res avg",removeOriginal=True):

    # This function replaces the current self.predictionData with window-averaged data
    # First have to run readPredictionData and addPredictionData!

    (slopes,averages) = self.setSlopesAndAvg()
    self.predictionData = averages
    seqCodes = averages.keys()
    seqCodes.sort()
    self.addOtherData(dataKey,seqCodes,[averages[seqCode] for seqCode in seqCodes])

    if removeOriginal:
      self.xLists.pop(0)
      self.yLists.pop(0)
      self.colourKeys.pop(0)

  def writePlot(self,graphPath,title,width=None,height=1000,ylim=(0.4,1),indexCut=None,removeLastFixedIndex=False,connectPointsLineWidth=0.5,resolution=300,
                                     infoCodes=None,infoStartCode=1,legendLocation='topleft',legendRelativeTextSize=0.6,
                                     customAxisLabels=None,minElementLength=3,plotDensityLists=None,plotDensityStep=None,plotDensityTexts=None,
                                     graphPathSlopes=None, graphPathAverages=None, xAxisLabelSeqOffset=0, default_colour='black',
                                     addBioAnnotation=False,yAxisLabel='S2 prediction',colourList=None,drawGrid=False):

    #
    # Define customisation of graph, to indicate secondary structure, ...
    #
    
    arrowList = []
    rectangles = []
    texts = []
    lines = []

    #
    # Write out the plot to graphPath.
    #

    # get the output file type
    fileType = graphPath.split('.')[-1]

    # set width depending on protein sequence size!
    allSeqIndexes = list(self.xValues)
    allSeqIndexes.sort()

    startSeqIndex = allSeqIndexes[0]
    endSeqIndex = allSeqIndexes[-1]

    seqLen = len(allSeqIndexes)

    if not indexCut:
      for indexCut in (200,100,50,20,10,5):
        if (seqLen - 50) / indexCut > 10:
          break

    if not width:
      if indexCut >=50:
        width = 2000
      else:
        width = 1500
    
    # Fix y limits and ranges for entropy/conservation info
    plotDensityYlimits = (ylim[0],0.5)
    if min(self.yValues) < ylim[0]:
      ylim_min = int(min(self.yValues) * 10) / 10.0
      ylimDensity_diff = ylim[0] - ylim_min
      plotDensityYlimits = (plotDensityYlimits[0] - ylimDensity_diff, plotDensityYlimits[1] - ylimDensity_diff)
      
      ylim = (ylim_min,ylim[1])
      
    if max(self.yValues) > ylim[1]:
      ylim = (ylim[0],int(max(self.yValues) * 10 + 1) / 10.0)

    xCustomAxisTicks = []
    xCustomAxisLabels = []

    if not customAxisLabels:
      for seqIndex in allSeqIndexes:
        if seqIndex == startSeqIndex or seqIndex == endSeqIndex or not seqIndex % indexCut:
          aaCode = self.aaNames[seqIndex]
          xCustomAxisLabels.append("{}\n{}".format(aaCode,self.seqIndexToSeqCode[seqIndex] + xAxisLabelSeqOffset))
          xCustomAxisTicks.append(seqIndex)
          if drawGrid:
            lines.append((seqIndex,ylim[0],seqIndex,ylim[1],'grey'))

    else:
      seqIndexes = customAxisLabels.keys()
      seqIndexes.sort()
      for seqIndex in seqIndexes:
        xCustomAxisLabels.append(customAxisLabels[seqIndex])
        xCustomAxisTicks.append(seqIndex)
        if drawGrid:
          lines.append((seqIndex,ylim[0],seqIndex,ylim[1],'grey'))

    if removeLastFixedIndex:
      xCustomAxisLabels[-2] = ""

    if self.colourKeys:
      if len(self.colourKeys) == 1:
        colourKeys = []
        if not colourList:
          colourList = [default_colour]
      else:
        colourKeys = self.colourKeys
    else:
      # If no legend, use same colour for all...
      colourKeys = self.colourKeys
      if not colourList:
        colourList = [default_colour] * 20

    #colourList = ['red']

    if infoCodes:
      # print infoCodes
      elementInfo = None
      numInfoCodes = len(infoCodes)
      for i in range(numInfoCodes):
        seqCode = infoStartCode + i
        infoCode = infoCodes[i]
        #print seqCode, infoCode
        if elementInfo and (elementInfo[1] != infoCode or i == numInfoCodes - 1):
          #print "   ", elementInfo
          elementLength = seqCode - elementInfo[0]

          # Only plot if longer or equal to minimal number of residues
          if elementLength >= minElementLength:
            # Define element
            length = 0
            lineWidth = 0
            if elementInfo[1] == 'H':
              color = (1.0,0.0,0.0,1.0)
              lineWidth = 4
              colorShade = (1.0,0.0,0.0,0.3)
            elif elementInfo[1] == 'h':
              color = (1.0,0.0,0.0,1.0)
              lineWidth = 2
              colorShade = (1.0,0.0,0.0,0.15)
            elif elementInfo[1] == 'E':
              color = (0.0,0.0,1.0,1.0)
              colorShade = (0.0,0.0,1.0,0.3)
              length = 0.05
              lineWidth = 1
            elif elementInfo[1] == '0':
              color = (0.2,0.2,0.2,1.0)
              colorShade = (0.8,0.8,0.8,0.3)
              #lineWidth = 2
            elif elementInfo[1] in '345678':
              # Shading by 'hotness'
              lineWidth = 0
              if elementInfo[1] == '3':
                colorShade = (0.0,0.0,1.0,0.6)
              elif elementInfo[1] == '4':
                colorShade = (0.0,0.0,1.0,0.4)
              elif elementInfo[1] == '5':
                colorShade = (0.0,0.0,1.0,0.2)
              elif elementInfo[1] == '6':
                colorShade = (1.0,0.0,0.0,0.2)
              elif elementInfo[1] == '7':
                colorShade = (1.0,0.0,0.0,0.4)
              elif elementInfo[1] == '8':
                colorShade = (1.0,0.0,0.0,0.6)
              color = colorShade

            if i == numInfoCodes - 1:
              endSeqCode = seqCode + 0.5
            else:
              endSeqCode = seqCode - 0.5

            #print elementInfo[0], endSeqCode

            if lineWidth:
              arrowList.append((elementInfo[0] - 0.5,ylim[0] + 0.02,endSeqCode,ylim[0] + 0.02, length, 30, color, lineWidth))
            rectangles.append((elementInfo[0] - 0.5,ylim[0],endSeqCode,ylim[1],colorShade,color))
            elementInfo = None

          else:
            elementInfo = None

        if not elementInfo and infoCode in 'EHh0345678':
          elementInfo = (seqCode,infoCode)

    # Add annotation to give plot more meaning
    if addBioAnnotation:
      color = (0.9,0.9,0.9,0.8)

      # Currently defined based on analysis of DSSP and STRIDE assignments in NMR ensembles
      yLimHigh = 0.80
      yLimLow = 0.69

      seqRange = endSeqIndex - startSeqIndex
      borderAlignOffset = seqRange * 0.035
      nextToGraphOffset = seqRange * 0.06

      if fileType in ('png',):
        rectangles.append((startSeqIndex-borderAlignOffset,yLimLow,endSeqIndex+borderAlignOffset,yLimHigh,color,color))
      else:
        lines.append((startSeqIndex-borderAlignOffset,yLimLow,endSeqIndex+borderAlignOffset,yLimLow,'black'))
        lines.append((startSeqIndex-borderAlignOffset,yLimHigh,endSeqIndex+borderAlignOffset,yLimHigh,'black'))

      texts.append((endSeqIndex+nextToGraphOffset,0.91,"Rigid",4,'black'))
      texts.append((endSeqIndex+nextToGraphOffset,0.76,"Context",4,'black'))
      texts.append((endSeqIndex+nextToGraphOffset,0.72,"dependent",4,'black'))
      texts.append((endSeqIndex+nextToGraphOffset,0.51,"Flexible",4,'black'))

    # Only takes 20 colours max...
    if len(self.xLists) > 20:
      xLists = self.xLists[:20]
      yLists = self.yLists[:20]
      
      if self.slopesLists:
        slopesLists = self.slopesLists[:20]
      if self.averageLists:
        averageLists = self.averageLists[:20]
        
      print "  Warning: truncating data set for plot, more than 20 data lists."

    else:
      xLists = self.xLists
      yLists = self.yLists
      slopesLists = self.slopesLists
      averageLists = self.averageLists

    self.plotGraphMultiColour(graphPath,xLists,yLists,main=title,xlab='Sequence',ylab=yAxisLabel,colourKeys=colourKeys[:],colourList=colourList,
                         xCustomAxisTicks=xCustomAxisTicks,xCustomAxisLabels=xCustomAxisLabels,textPointsize=7,
                         xCustomAxisOrientation='parallel',xCustomAxisLabelSize=0.5,xCustomAxisPackLabels=True,
                         connectPoints=True,pointSize='small',width=width,height=height,ylim=ylim,connectPointsLineWidth=connectPointsLineWidth,
                         resolution=resolution,arrows=arrowList,rectangles=rectangles,connectPointsMaxSeparationX=1,
                         legendLocation=legendLocation,legendRelativeTextSize=legendRelativeTextSize,legendOutside=True,
                         plotDensityLists=plotDensityLists,plotDensityStep=plotDensityStep,plotDensityTexts=plotDensityTexts,
                         plotDensityStepY = 0.05,plotDensityLimitExtend=1,plotDensityRemoveFirstLast=True,
                         plotDensityYlimits = plotDensityYlimits, fileType=fileType, texts=texts, anyLines=lines)


    if self.slopesLists and graphPathSlopes:

      ylim_slopes = (-0.1,0.1)
      arrowList_slopes = [(arrowData[0],-0.1,arrowData[2],-0.1, arrowData[4], arrowData[5], arrowData[6], arrowData[7]) for arrowData in arrowList]
      rectangles_slopes = [(rectangleData[0], ylim_slopes[0], rectangleData[2], ylim_slopes[1], rectangleData[4], rectangleData[5] ) for rectangleData in rectangles]

      self.plotGraphMultiColour(graphPathSlopes,xLists,slopesLists,main=title,xlab='Sequence',ylab='{} slope'.format(yAxisLabel),colourKeys=colourKeys[:],colourList=colourList,
                                 xCustomAxisTicks=xCustomAxisTicks,xCustomAxisLabels=xCustomAxisLabels,textPointsize=7,
                                 xCustomAxisOrientation='parallel',xCustomAxisLabelSize=0.5,xCustomAxisPackLabels=True,
                                 connectPoints=True,pointSize='small',width=width,height=height,ylim=ylim_slopes,connectPointsLineWidth=connectPointsLineWidth,
                                 resolution=resolution,arrows=arrowList_slopes,rectangles=rectangles_slopes,connectPointsMaxSeparationX=1,
                                 legendLocation=legendLocation,legendRelativeTextSize=legendRelativeTextSize,legendOutside=True,
                                 plotDensityLists=plotDensityLists,plotDensityStep=plotDensityStep,plotDensityTexts=plotDensityTexts,
                                 plotDensityStepY = 0.05,plotDensityLimitExtend=1,plotDensityRemoveFirstLast=True,
                                 plotDensityYlimits = (-0.10,-0.05), fileType=fileType)

    if self.averageLists and graphPathAverages:

      self.plotGraphMultiColour(graphPathAverages,xLists,averageLists,main=title,xlab='Sequence',ylab='{} smoothed'.format(yAxisLabel),colourKeys=colourKeys[:],colourList=colourList,
                                 xCustomAxisTicks=xCustomAxisTicks,xCustomAxisLabels=xCustomAxisLabels,textPointsize=7,
                                 xCustomAxisOrientation='parallel',xCustomAxisLabelSize=0.5,xCustomAxisPackLabels=True,
                                 connectPoints=True,pointSize='small',width=width,height=height,ylim=ylim,connectPointsLineWidth=connectPointsLineWidth,
                                 resolution=resolution,arrows=arrowList,rectangles=rectangles,connectPointsMaxSeparationX=1,
                                 legendLocation=legendLocation,legendRelativeTextSize=legendRelativeTextSize,legendOutside=True,
                                 plotDensityLists=plotDensityLists,plotDensityStep=plotDensityStep,plotDensityTexts=plotDensityTexts,
                                 plotDensityStepY = 0.05,plotDensityLimitExtend=1,plotDensityRemoveFirstLast=True,
                                 plotDensityYlimits = plotDensityYlimits, fileType=fileType)


  def writeCorrelationPlots(self,graphDir='.',diagonal=True):

    #
    # Write out scatter plots showing the correlation between variable lists, if more than one set present
    #

    if len(self.colourKeys) > 1:
      for i in range(len(self.xLists)-1):
        dataKey1 = self.colourKeys[i]
        for j in range(i+1,len(self.xLists)):
          dataKey2 = self.colourKeys[j]

          ylist1 = []
          ylist2 = []
          for l in range(len(self.xLists[i])):
            iSeqIndex = self.xLists[i][l]
            for k in range(len(self.xLists[j])):
              jSeqIndex = self.xLists[j][k]
              if iSeqIndex == jSeqIndex:
                ylist1.append(self.yLists[i][l])
                ylist2.append(self.yLists[j][k])

          self.plotGraph("corr_{}_{}.png".format(dataKey1,dataKey2),ylist1,ylist2,main="Correlation between {} and {}".format(dataKey1,dataKey2),xlab=dataKey1,ylab=dataKey2,xlim=(0,1),ylim=(0,1),pointSize='small',diagonal=diagonal)


  def writeHistograms(self,subsetLegends,subsetSeqCodeLists,colourRgbList=None,ignoreSeqCodes=None,graphSuffix="",
                           useMainList=True,ylim=None,histoPlotKeywds=None):

    #
    # Write out histograms containing subsets of the data (e.g. helix/sheet/other secondary structure). Subsets have to be defined in advance in subsetSeqCodeLists.
    #

    if not ignoreSeqCodes:
      ignoreSeqCodes = []

    for i in range(len(self.colourKeys)):
      dataKey = self.colourKeys[i]

      seqCodes = self.xLists[i]
      values = self.yLists[i]

      fstep = 0.02
      histRange = self.frange(min(values) - fstep,max(values) + fstep,fstep)

      dataLists = []
      numSubsets = len(subsetSeqCodeLists)
      for j in range(numSubsets + 1):
        dataLists.append([])

      for j in range(len(seqCodes)):
        seqCode = seqCodes[j]

        if seqCode in ignoreSeqCodes:
          continue

        seqCodeFound = False
        for k in range(numSubsets):
          if seqCode in subsetSeqCodeLists[k]:
            dataLists[k+1].append(values[j])
            seqCodeFound = True

        if not seqCodeFound and useMainList:
          dataLists[0].append(values[j])

      if useMainList:
        legendTexts = ['Main'] + subsetLegends
      else:
        legendTexts = subsetLegends
        dataLists = dataLists[1:]

      if not histoPlotKeywds:
        histoPlotKeywds = {}

      self.plotHisto("histo_{}{}.png".format(dataKey,graphSuffix),dataLists,histRange,"Histogram distributions for {} with subsets".format(dataKey),'DynaMine S2 prediction','Frequency',
                                               colourRgbList=colourRgbList,legendLocation='topright',legendTexts=legendTexts,legendTitle="Subsets",ylim=ylim,**histoPlotKeywds)

  def writeSequenceLogo(self,alignmentFilePath,sequenceLogoPath,title,resolution=400,seqLen=200,stackWidth=5,logoHeight=80):

    #
    # Write out sequence logo plots based on an alignment file (has to be in correct format!)
    #

    import weblogolib

    # Read alignment for weblogo
    fin = open(alignmentFilePath)
    seqs = weblogolib.read_seq_data(fin)
    data = weblogolib.LogoData.from_seqs(seqs)

    # Get entropy data out as well
    seqCodeEntropy = {}
    lines = str(data).split("\n")
    for line in lines:
      cols = line.split()
      if line.startswith("#"):
        if line.count("Entropy") and line.count("E") and line.count("Weight"):
          entropyCol = cols.index("Entropy")
      elif cols:
        seqCode = int(cols[0])
        entropy= float(cols[entropyCol])
        seqCodeEntropy[seqCode] = entropy

    # Make plot if necessary (won't write if nothing given here)
    if sequenceLogoPath:

      # Set weblogo title
      options = weblogolib.LogoOptions()
      options.title = title

      # Format the graph
      format = weblogolib.LogoFormat(data, options)
      format.resolution = resolution
      format.stacks_per_line = min(seqLen,500)
      format.stack_width = stackWidth
      format.fineprint = ""

      # NOTE: the extra 50 on the width, and the default height, will depend on the resolution as well!!
      format.logo_width = seqLen * stackWidth + 50
      format.logo_height = logoHeight

      fout = open(sequenceLogoPath, 'w')
      weblogolib.png_formatter( data, format, fout)

    return seqCodeEntropy
