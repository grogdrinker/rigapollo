import os

from dynaMine.analysis.makePlot import MakeDynaMinePlot
from dynaMine.analysis.parsers import DynaMineRelatedFileParsers
from dynaMine.predictor.wrappers import RunDynaMinePrediction

from memops.universal.Util import frange

from pdbe.analysis.Graphs import plotImageGraph

try:
  import rpy
except:
  import rpy2.rpy_classic as rpy
  rpy.set_default_mode(rpy.BASIC_CONVERSION)

class MakeDynaMinePlotBeta(MakeDynaMinePlot,DynaMineRelatedFileParsers,RunDynaMinePrediction):
  
  """
  Development version; naming is historical
  """
  
  def readSsPredictionData(self,filePathOrPaths,startIndex=1,setSsAsMainPredictionData=None,normalise=True):

    self.setSsAsMainPredictionData = setSsAsMainPredictionData
    
    #
    # Read data from file or files 
    #
        
    ssPredictionData = {}
    
    if type(filePathOrPaths) == type(""):
      (helixValues,sheetValues,coilValues,predictionValues) = self.readJointSsPredictionFile(filePathOrPaths)
    else:
      predictionValues = None
      (helixValues,sheetValues,coilValues) = self.readSsPredictionFiles(filePathOrPaths)
          
    self.setSsPredictionData(ssPredictionData,helixValues,sheetValues,coilValues,predictionValues,startIndex=startIndex,setSsAsMainPredictionData=setSsAsMainPredictionData,normalise=normalise)
  
    return ssPredictionData
  
  def setSsPredictionData(self,ssPredictionData,helixValues,sheetValues,coilValues,predictionValues=None,startIndex=1,setSsAsMainPredictionData=None,normalise=True):

    self.setSsAsMainPredictionData = setSsAsMainPredictionData
  
    for i in range(len(helixValues)):
      seqIndex = startIndex + i
      
      (aaCode,helix) = helixValues[i]
      (aaCode,sheet) = sheetValues[i]
      (aaCode,coil) = coilValues[i]
      
      if predictionValues:
        prediction = predictionValues[i]
      else:
        prediction = self.highestSsPred(helix,sheet,coil)
      
      self.setSsPredictionValues(aaCode,helix,sheet,coil,prediction,normalise,seqIndex,ssPredictionData)
    
  def setSsPredictionValues(self,aaCode,helix,sheet,coil,prediction,normalise,seqIndex,ssPredictionData):

    # Values can be lower than 0!
    helix = max(0.0,float(helix))
    sheet = max(0.0,float(sheet))
    coil = max(0.0,float(coil))

    if normalise:
      total = helix + sheet + coil

      helix /= total
      sheet /= total
      coil /= total

    values = (helix,sheet,coil)

    if not self.setSsAsMainPredictionData:
      ssPredictionData[seqIndex] = (aaCode,) + values + (prediction,)
    else:
      self.predictionData[seqIndex] = (aaCode,values[self.ssTypes.index(self.setSsAsMainPredictionData)])
 
  def highestSsPred(self,helix,sheet,coil):

    if sheet <= helix >= coil:
      prediction = 'helix'
    elif helix <= sheet >= coil:
      prediction = 'sheet'
    else:
      prediction = 'coil'
      
    return prediction
          
  def addSsPredictionData(self,ssPred,graphPosition=None,multiplyBbPred=False):

    seqIndexes = ssPred.keys()
    seqIndexes.sort()
    
    adder = 0
    multiplier = 1.0
    if graphPosition == 'top': 
      adder = 1.0
      multiplier = 0.4
    elif graphPosition == 'bottom':
      multiplier = 0.4

    for (sspi,ssType) in ((1,'pred_helix'),(2,'pred_sheet'),(3,'pred_coil')):
      values = []
      for i in range(len(seqIndexes)):
        seqIndex = seqIndexes[i]
        
        value = ssPred[seqIndex][sspi]
        if multiplyBbPred:
          if ssType.count('coil'):
            bbMulti = 1.4 - self.yLists[0][i]
          else:
            bbMulti = self.yLists[0][i]

          value = value * bbMulti
        
        value = (value * multiplier) + adder
        values.append((ssPred[seqIndex][0],value))
          
      self.addOtherData(ssType.replace("pred_",""),seqIndexes,values)
  
  # TODO TODO Should combine this with writegraph, if possible
  def plotHydro(self,graphPath,title,width=None,height=1000,ylim=(0.4,1),indexCut=None,removeLastFixedIndex=False,resolution=300,
                                     legendLocation='topleft',legendRelativeTextSize=1,
                                     customAxisLabels=None,minElementLength=3,plotDensityLists=None,plotDensityStep=None,plotDensityTexts=None,
                                     graphPathSlopes=None, graphPathAverages=None):
  
    # set width depending on protein sequence size!
    allSeqIndexes = list(self.xValues)
    allSeqIndexes.sort()
    
    startSeqIndex = allSeqIndexes[0]
    endSeqIndex = allSeqIndexes[-1]
    
    seqLen = len(allSeqIndexes)
    
    if not indexCut:
      for indexCut in (200,100,50,20,10,5):
        if (seqLen - 50) / indexCut > 10:
          break
    
    if not width:
      if indexCut >=50:
        width = 2000
      else:
        width = 1500
        
    if min(self.yValues) < ylim[0]:
      ylim = (int(min(self.yValues) * 10) / 10.0,ylim[1])
    if max(self.yValues) > ylim[1]:
      ylim = (ylim[0],int(max(self.yValues) * 10 + 1) / 10.0)
    
    xCustomAxisTicks = []
    xCustomAxisLabels = []
    
    if not customAxisLabels:
      for seqIndex in allSeqIndexes:
        if seqIndex == startSeqIndex or seqIndex == endSeqIndex or not seqIndex % indexCut:
          aaCode = self.aaNames[seqIndex]
          xCustomAxisLabels.append("{}\n{}".format(aaCode,seqIndex))
          xCustomAxisTicks.append(seqIndex)    
    else:
      seqIndexes = customAxisLabels.keys()
      seqIndexes.sort()
      for seqIndex in seqIndexes:
        xCustomAxisLabels.append(customAxisLabels[seqIndex])
        xCustomAxisTicks.append(seqIndex)    
    
    if removeLastFixedIndex:
      xCustomAxisLabels[-2] = ""
    
    # FROM HERE ON DIFFERENT than writePlot! Also note slightly different keywords at method def
    colourKeys = ['hydrophobic (strong)','hydrophobic (weak)','neutral','hydrophilic (weak)','hydrophilic (strong)']
    colourList = ['blue','lightblue','yellow','orange','red']
    
    assert len(self.xLists) == 1, "Only one a time for hydro"
    
    seqIndexes = self.predictionData.keys()
    seqIndexes.sort()
    
    # Recalculate; TODO TODO might be better to do this differently!!
    offSet = self.xLists[0][0] - seqIndexes[0]
    
    self.xLists = []
    self.yLists = []
    
    for i in range(len(colourKeys)):    
      self.xLists.append([])
      self.yLists.append([])
    
    for seqIndex in seqIndexes:
    
      useSeqIndex = seqIndex + offSet
      
      (aaCode,predS2_value) = self.predictionData[seqIndex]
      
      (listIndex,hydroValue) = self.getHydroInfo(aaCode)
      
      self.xLists[listIndex].append(useSeqIndex)
      self.yLists[listIndex].append(predS2_value)
      
      self.aaNames[useSeqIndex] = aaCode
      self.xValues.add(useSeqIndex)
      self.yValues.add(predS2_value)

    self.plotGraphMultiColour(graphPath,self.xLists,self.yLists,main=title,xlab='Sequence',ylab='S2 prediction',colourKeys=colourKeys[:],colourList=colourList,
                         xCustomAxisTicks=xCustomAxisTicks,xCustomAxisLabels=xCustomAxisLabels,textPointsize=7,
                         xCustomAxisOrientation='parallel',xCustomAxisLabelSize=0.5,xCustomAxisPackLabels=True,
                         width=width,height=height,ylim=ylim,
                         resolution=resolution,
                         legendLocation=legendLocation,legendRelativeTextSize=legendRelativeTextSize,legendOutside=True,
                         plotDensityLists=plotDensityLists,plotDensityStep=plotDensityStep,plotDensityTexts=plotDensityTexts,
                         plotDensityStepY = 0.05,plotDensityLimitExtend=1,plotDensityRemoveFirstLast=True,
                         plotDensityYlimits = (ylim[0],0.5))

  def getHydroInfo(self,oneLetterAaCode):
    
    # TODO THIS NOT INDEPENDENT!!
    from sbb.analysis.Constants import hydrophobicityScale
    from ccp.general.Constants import code1LetterToCcpCodeDict
    
    resLabel = code1LetterToCcpCodeDict['protein'][oneLetterAaCode]
    hydroValue = hydrophobicityScale['pH7'][resLabel]
    
    if hydroValue >= 60:
      hydroIndex = 0
    elif hydroValue >= 10:
      hydroIndex = 1
    elif hydroValue >= -10:
      hydroIndex = 2
    elif hydroValue >= -40:
      hydroIndex = 3
    else:
      hydroIndex = 4

    return (hydroIndex,hydroValue)


  def makeContributionPlot(self,graphPath,title,contactMatrix=None,plotGraph=True,contribType='sum',useContactMatrixOnly=False):
  
    r = rpy.r
  
    seqIds = self.xLists[0] 
    dynaMineValues = self.yLists[0]
    
    if contactMatrix:
      assert len(contactMatrix) == len(dynaMineValues), "Mismatch in sequence lengths, {} <> {}!".format(len(contactMatrix),len(dynaMineValues))
      
      if useContactMatrixOnly:
        self.contactHisto = []
        histoLegendTexts = None
        legendLocation = None
        legendTitle = None
        onlyDensity = False
      else:
        self.contactHisto = [[],[]]
        histoLegendTexts=['No contact','Contact']
        legendLocation = 'topright'
        legendTitle = 'Status'
        onlyDensity = True
    
    #
    # Make a heat plot
    #

    columnLists = self.initColumnList(dynaMineValues)

    for x in range(len(dynaMineValues)):
        
      dynaMineValue1 = dynaMineValues[x]

      for y in range(len(dynaMineValues)):

        dynaMineValue2 = dynaMineValues[y]
        
        if contactMatrix:
          if x < y or useContactMatrixOnly:
            value = contactMatrix[x][y]
            if useContactMatrixOnly and value and value > -0.5:
              self.contactHisto.append(value)
          elif x == y:
            value = -1
          else:
            if contribType == 'sum':
              value = dynaMineValue1 + dynaMineValue2
            elif contribType == 'diff':
              value = abs(dynaMineValue1 - dynaMineValue2)
            
            if contactMatrix[y][x] == -1:
              self.contactHisto[1].append(value)
            else:
              self.contactHisto[0].append(value)
        else:
          value = dynaMineValue1 + dynaMineValue2
         
        columnLists[x][y] += value
      
    #
    # Do some cleanup, create matrix
    # 

    matrix = None

    # Not sure if necessary, but try
    rpy.set_default_mode(rpy.NO_CONVERSION)

    for colIndex in range(len(columnLists)):

      columnList = columnLists[colIndex]        

      if matrix == None:
        matrix = r.cbind(columnList)
      else:
        matrix = r.cbind(matrix,columnList)

 
    rpy.set_default_mode(rpy.BASIC_CONVERSION)

    #
    # Plot graph
    #
    
    if plotGraph:

      if contribType not in ('interaction'):
        numSteps = 8
        palette = r.colorRampPalette(['white','red'])
      else:
        numSteps = 9
        bluePalette = r.colorRampPalette(['blue','white'])
        redPalette = r.colorRampPalette(['white','red'])

      if contactMatrix and contribType not in ('interaction'):
        colourList = ['black']
        breakList = [-1]
        legendTexts = ["Contact"]
      else:
        colourList = ['darkgreen','darkgrey']
        breakList = [-2.5,-1.5]
        legendTexts = ["Special residue","Diagonal"]

      if contribType == 'sum':
        colourList += palette(numSteps)
        breakList += [-0.01,1.2,1.4,1.5,1.6,1.7,1.8,1.9,2.4]
        legendTexts.append("0.00-{:.2f}".format(breakList[1]))
        legendAutoStart = 2
      elif contribType == 'diff':
        colourList += palette(numSteps)
        breakList += [-0.01,0.02,0.05,0.1,0.02,0.3,0.4,0.5,1.0]
        legendTexts.append("0.00-{:.2f}".format(breakList[1]))
        legendAutoStart = 2
      elif contribType == 'interaction':
        #colourList += 
        colourList += bluePalette(numSteps / 2 + 1)[:-1]
        colourList += ['white']
        colourList += redPalette(numSteps / 2 + 1)[1:]
        breakList += [-0.15,-0.05,-0.025,-0.01,-0.005,0.005,0.01,0.025,0.05,0.15]
        legendAutoStart = 2
        
      for i in range(legendAutoStart,len(breakList) - 1):
        legendTexts.append("{:.2f} - {:.2f}".format(breakList[i],breakList[i+1]))

      #
      # Make sure to account for offset in plot itself
      #

      rpy.set_default_mode(rpy.NO_CONVERSION)
      yListOut = r.cbind(seqIds )
      xListOut = r.cbind(seqIds)
      rpy.set_default_mode(rpy.BASIC_CONVERSION)

      #xlim = (self.dynaMineRange[0],self.dynaMineRange[-1])
      #ylim = (self.hydroRange[0],self.hydroRange[-1])

      plotImageGraph(graphPath,xListOut,yListOut, matrix,
                     main = title,# %s %s' % (ccpCode,heavyAtomNameKey),
                     xlab = "seqId",
                     ylab = "seqId",
                     colourList = colourList,
                     breakList = breakList,
                     legendTexts = legendTexts,
                     #xlim = xlim,
                     #ylim = ylim,
                     legendTitle = "Legend",
                     legendLocation = "bottomright")


      if contactMatrix:

        (graphPath,graphFileName) = os.path.split(graphPath)
        graphPath = os.path.join(graphPath,"histo_{}".format(graphFileName))

        if contribType == 'sum':
          histRange = frange(0.0,2.41,0.05)
        elif contribType == 'diff':
          histRange = frange(-0.1,0.6,0.02)
        elif contribType == 'interaction':
          histRange = frange(-0.15,0.15,0.005)

        self.plotHisto(graphPath,self.contactHisto,histRange,"DynaMine residue sum",'DynaMine S2 prediction (summed)','Frequency',
                                                 legendLocation=legendLocation,legendTexts=histoLegendTexts,legendTitle=legendTitle,
                                                 density=True,bandWidth=0.05,onlyDensity=onlyDensity)

  def initColumnList(self,dynaMineValues):
  
    columnLists = []
    for i in range(len(dynaMineValues)):
      columnLists.append([])
      for j in range(len(dynaMineValues)):
        columnLists[-1].append(0)
    
    return columnLists

  def predictAndMakeSingleSequencePlot(self,identifier,fastaFile,dataTypes=('backbone','secStruc'),ssCodes=None,graphSuffix="",indexCut=20,seqId=None,minElementLength=3,xAxisLabelSeqOffset=0):
  
    #,"sidechain" is another dataType option!
  
    predictionFileNames = self.runDynaMineAll(fastaFile,identifier,dataTypes=dataTypes,singleOutputFiles=False) # Can add an overwrite keyword here to replace existing preds
  
    # Single sequence, put all in single plot, can select if multiple separate seqs in fasta as well
    if not seqId:
      seqId = identifier
      
    resultFiles = {}
    for dataType in dataTypes: 
      for fileForDataType in predictionFileNames[dataType]:
        if fileForDataType.count("/{}_".format(seqId)) and os.path.exists(fileForDataType):
          if dataType not in resultFiles.keys():
            resultFiles[dataType] = []
          resultFiles[dataType].append(fileForDataType)

    # Make the plots
    # Backbone info, this is required
    assert len(resultFiles['backbone']) == 1, "Too many backbone file matches, can't run this"
    self.readPredictionData(resultFiles['backbone'][0])
    self.addPredictionData(identifier)

    # Sidechain
    if 'sidechain' in dataTypes:
      assert len(resultFiles['sidechain']) == 1, "Too many sidechain file matches, can't run this"
      self.readPredictionData(resultFiles['sidechain'][0])
      self.addPredictionDataAsOther('sideChain')

    # Secondary structure
    if 'secStruc' in dataTypes:
      ssPred = self.readSsPredictionData(resultFiles['secStruc'])
      self.addSsPredictionData(ssPred,graphPosition='top')
      
    # Plot graph; TODO need adjustment of colour list in case sidechain is included!
    self.writePlot(os.path.join(self.dynaMineResultsDir,"{}{}.png".format(identifier,graphSuffix)),"Predicted S2 scores for {}".format(identifier),
                   indexCut=indexCut,removeLastFixedIndex=False,addBioAnnotation=True,ylim=(0.4,1.4),colourList=['black','red','blue','grey'],
                   drawGrid=False,infoCodes=ssCodes,minElementLength=minElementLength,xAxisLabelSeqOffset=xAxisLabelSeqOffset)

  def predictAndMakeMultipleSequencePlot(self,identifier,fastaFile,dataTypes=('backbone','secStruc','sidechain'),ssCodes=None,graphSuffix="",titleSuffix="",indexCut=20):

    """
    This works only on sequences with same length in a joint FASTA file!
    """

    resultFiles = self.runDynaMineAll(fastaFile,identifier,dataTypes=dataTypes) # Can add an overwrite keyword here to replace existing preds

    #(dynaMineResultsDir,seqIds) = self.runPredictions(identifier,fastaFile,dataTypes,runPrediction)

    # Multiple sequences, make plot per data type
    resultData = {}
    for dataType in dataTypes:
      if dataType != 'secStruc':
        resultData[dataType] = self.readDynaMineMultiEntry(resultFiles[dataType][0])
      else:
        resultData.update(self.readSsPredictionFilesMultiEntry(resultFiles[dataType]))

    seqIds = resultData['backbone'].keys()
    seqIds.sort()
    
    # Now plot for backbone/sidechain, filter by seqId if required
    for dataType in resultData.keys():
      
      addBioAnnotation = False
      yAxisLabelTemplate = None
      if dataType == 'backbone':
        ylim = (0.4,1.0)
        addBioAnnotation = True
        yAxisLabel = "S2 prediction"
      elif dataType == 'sidechain':
        ylim = (0.0,0.7)
        yAxisLabel = "Sidechain dynamics prediction"
      else:
        ylim = (0.0,1.0)
        yAxisLabelTemplate = "{} propensity"
        yAxisLabel = yAxisLabelTemplate.format(dataType.capitalize())

      for seqId in seqIds:
        if self.isValidSeqId(seqId,graphSuffix):
          if dataType != 'secStruc':
            self.setPredictionData(resultData[dataType][seqId])
            self.addPredictionData(seqId)

          else:
            # Bit of overkill here!
            self.setSsPredictionData({},resultData['helix'][seqId],resultData['sheet'][seqId],resultData['coil'][seqId],setSsAsMainPredictionData=dataType)
            self.addPredictionData(seqId)

      self.writePlot(os.path.join(self.dynaMineResultsDir,"{}{}_{}.png".format(identifier,graphSuffix,dataType)),"{} for {}, {}{}".format(yAxisLabel,identifier,dataType,titleSuffix),indexCut=indexCut,removeLastFixedIndex=False,
                     addBioAnnotation=addBioAnnotation,ylim=ylim,yAxisLabel=yAxisLabel)

      self.initialiseVars()

  def isValidSeqId(self,seqId,graphSuffix):
  
    return True

  def addDisorderModifier(self,name,window=4,expFact=3,yListIndex=0):
  
    values = self.yLists[yListIndex]

    # Track min/max for renormalising
    valMin = min(values)
    valMax = max(values)

    #newValues = values
    newValues = []
    valLen = len(values)
    
    iMax = valLen - 1
    for i in range(valLen):  

      value = newValue = values[i]

      # Don't take current value into account in window
      valRange = values[max(0,i-window):i] + values[min(i+1,iMax):min(valLen,i+window+1)]

      maxVal = max(valRange)
      if maxVal > 0.82: # More likely order
        newValue = value * 1.1
      else:
        minVal = min(valRange)
        if minVal < 0.65: # More likely disorder
          newValue = value * 0.9

      newValues.append((None,newValue))
    """    
    for i in range(valLen):
      valRange = values[max(0,i-window):min(valLen,i+window)]

      maxValRange = min(0.8,max(valRange)) # Limit to 0.8 maximum, don't overdo structure part

      newValues.append(values[i] * (maxValRange ** expFact))        



    newValMin = min(newValues)
    newValMax = max(newValues)

    # Renormalise
    factor = (valMax - valMin) / (newValMax - newValMin)

    for i in range(valLen):
      newValues[i] = (None,(newValues[i] - newValMin) * factor + valMin)

    """    

    self.addOtherData(name,self.xLists[0],newValues)

