import os, glob

from dynaMine.predictor.parsers import DynaMineFileParsers

class DynaMineRelatedFileParsers(DynaMineFileParsers):
 
  insertCodeFromFile = '-'
  
  def readDisorderPredictorData(self,predictor,fileName,reverse=False):
    
    # TODO technically not plotting, needs to go elsewhere!

    fin = open(fileName)
    lines = fin.readlines()
    fin.close()

    if predictor == 'ESpritz':
      dataCol = 1
    elif predictor == 'IUPred':
      dataCol = 2
    elif predictor == 'PrDOS':
      dataCol = 2
    elif predictor == 'RONN':
      dataCol = 1
    elif predictor == 'VSL2':
      dataCol = 0
    elif predictor == 'FoldIndex':
      dataCol = 1

    dataValues = []

    for line in lines:
      cols = line.split()
      
      if cols and cols[0] != '#':

        if predictor == 'VSL2' and not cols[0].isdigit():
          # short and long form...
          if line.count("VSL2 Predictor"):
            dataCol = 2
          continue
          
        if predictor == 'PrDOS' and len(cols[0]) > 1:
          continue
          
        dataValue = float(cols[dataCol])
        if reverse:
          if predictor != 'FoldIndex':
            dataValue = 1.0 - dataValue
        dataValues.append(dataValue)

    return dataValues

  def readCoordAndD2dData(self,fileName,offset=0):

    # TODO: also move!
      
    fin = open(fileName)
    expDataLines = fin.readlines()
    fin.close()

    d2dData = {'strucSeqIndexes': [], 'expSeqIndexes': [], 'strucH': [], 'strucE': [], 'expH': [], 'expE': []}

    for line in expDataLines:
      (seqCode,strucH,strucE,expH,expE) = line.split(',')
      seqIndex = int(seqCode) - offset
      if strucH != 'None':
        d2dData['strucSeqIndexes'].append(seqIndex)
        d2dData['strucH'].append(float(strucH) / 100.0)
        d2dData['strucE'].append(float(strucE) / 100.0)

      if expH != 'None':
        d2dData['expSeqIndexes'].append(seqIndex)
        d2dData['expH'].append(float(expH) / 100.0)
        d2dData['expE'].append(float(expE) / 100.0)
    
    return d2dData

  def readAlignments(self,fileName):

    # Read the file    
    fin = open(fileName)
    lines = fin.readlines()
    fin.close()
    
    # Check which type of alignment file    
    fileType = None
    numLines = len(lines)
    if lines[0].count("CLUSTAL"):
      fileType = "CLUSTAL"
    else:
      fastaCount = balibaseCount = psiCount = 0
      for line in lines:
        if line.startswith(">"):
          fastaCount += 1
        elif line.startswith("//"):
          balibaseCount += 1
        elif len(line.split()) == 2:
          psiCount += 1

      if fastaCount > balibaseCount:
        fileType = 'FASTA'
      elif balibaseCount:
        fileType = 'BaliBase'
      elif psiCount == numLines:
        fileType = 'PSI'
      
    assert fileType, "Alignment file not recognised"
    
    self.alignRefSeqID = None
    if fileType == 'CLUSTAL':
      seqAlignments = self.readAlignmentsClustal(lines)
    elif fileType == 'FASTA':
      seqAlignments = self.readAlignmentsFasta(lines)
    elif fileType == 'BaliBase':
      seqAlignments = self.readAlignmentsBalibase(lines)
    elif fileType == 'PSI':
      seqAlignments = self.readAlignmentsPSI(lines)
    
    return seqAlignments

  def readAlignmentsFasta(self,lines):
  
    """ 
    FASTA file alignment
    """
    
    startReading = True
    seqAlignments = {}
    
    for line in lines:        

      cols = line.split()

      if cols:
      
        if cols[0].startswith('>'):
          seqId = self.getSeqIdKey(cols[0][1:])
          if not self.alignRefSeqID:
            self.alignRefSeqID = seqId
        else:
          if seqId not in seqAlignments.keys():
            seqAlignments[seqId] = cols[0]
          else:
            # Multiline FASTA
            seqAlignments[seqId] += cols[0]

      else:
        self.setEmptyLineVars()

    return seqAlignments

  def readAlignmentsBalibase(self,lines):
  
    """
    BaliBase alignment
    """
    
    startReading = False
    seqAlignments = {}
    
    for line in lines:
      
      if line.startswith("//"):
        startReading = True
        continue
        
      if startReading:
        cols = line.split()
        
        if cols:
          seqId = self.getSeqIdKey(cols[0])
          alignment = ''.join(cols[1:])
          
          if not self.alignRefSeqID:
            self.alignRefSeqID = seqId

          if seqId not in seqAlignments.keys():
            seqAlignments[seqId] = ""
          
          seqAlignments[seqId] += alignment

        else:
          self.setEmptyLineVars()

    return seqAlignments

  def readAlignmentsClustal(self,lines):
    
    """
    CLUSTAL files
    """
  
    startReading = False
    seqAlignments = {}
    
    for line in lines:
      
      if line.startswith("CLUSTAL"):
        startReading = True
        continue
        
      if startReading:
        cols = line.split()
        
        if cols:          
          if len(cols) in (2,3):

            # Ignore lines with annotation information
            if cols[0].count('*') or cols[0].isdigit():
              continue
  
            seqId = self.getSeqIdKey(cols[0])
            
            if not self.validSeqId(seqId):
              continue

            if not self.alignRefSeqID:
              self.alignRefSeqID = seqId
            
            alignment = cols[1]
            
            if seqId not in seqAlignments.keys():
              seqAlignments[seqId] = ""
            
            seqAlignments[seqId] += alignment

        else:
          self.setEmptyLineVars()

     
    return seqAlignments

  def readAlignmentsPSI(self,lines):
  
    """ 
    PSI file alignment
    """
    
    seqAlignments = {}
    
    for i in range(len(lines)):
    
      line = lines[i]      

      (seqId,sequence) = line.split()
      
      seqId = self.getSeqIdKey(seqId)

      if not self.alignRefSeqID:
        self.alignRefSeqID = seqId

      # For some reason additional - at beginning of sequence for target protein
      if i == 0 and sequence[0] == self.insertCodeFromFile:
        sequence = sequence[1:]

      seqAlignments[seqId]  = sequence

    return seqAlignments


  def setEmptyLineVars(self):
    
    # Custom function for subclasses
    pass

  def validSeqId(self,seqId):
    
    # Checks whether this is a valid sequence ID, can be used for filtering in subclasses     
    return True
    
  def getSeqIdKey(self,seqId):
  
    # Make a good sequence ID from what's found in the alignment file; used in subclass
    return seqId

  def writeFastaFromAlignment(self,alignFile,outFastaFile):
  
    #
    # Writes a FASTA file from an alignment
    #
  
    seqAlignments = self.readAlignments(alignFile)
  
    fout = open(outFastaFile,'w')
    
    seqIds = seqAlignments.keys()
    for seqId in seqIds:
      fout.write(">{}\n".format(seqId))
      fout.write("{}\n".format(seqAlignments[seqId].replace(self.insertCodeFromFile,'')))
    fout.close()
