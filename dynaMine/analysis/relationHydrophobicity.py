"""
Development code, not for distribution!
"""

import os, math

from dynaMine.analysis.makePlot import MakeDynaMinePlot
from sbb.analysis.ProtScale import ProtScale
from memops.universal.Util import frange

from pdbe.analysis.Graphs import plotImageGraph

try:
  import rpy
except:
  import rpy2.rpy_classic as rpy
  rpy.set_default_mode(rpy.BASIC_CONVERSION)

class DynaMineAndHydrophobicity:

  """
  Normalised here means that for probabilities whole matrix is taken, so ends up as 1.0 total!
  """

  validSeqIds = None

  dynaMineRange = frange(0.0,1.2,0.05)
  dynaMineTotal = len(dynaMineRange)
  
  # Now normalised
  hydroRange = frange(0,1,0.05)
  hydroTotal = len(hydroRange)
  
  hydroInfo = None
  dynaMineInfo = None
  
  graphDir = "graphs"
  graphSuffix = ""
  
  aaNameString = "ACDEFGHIKLMNPQRSTVWY"
    
  def setHydrophobInfo(self,hydrophobScale,windowLength=15,windowEdgeScaling=100,windowDependency='linear'):
  
    self.hydrophobScale = hydrophobScale
    self.windowLength = windowLength
    self.windowEdgeScaling = windowEdgeScaling
    self.windowDependency = windowDependency

    #
    # Initialise hydrophobicity scale, so can normalise over all values
    #
    
    self.getHydrophobProfileAllSeqs()
    
  def getHydrophobProfileAllSeqs(self,normValues=None,normalise=True):
  
    # Normalises on reference set; still problem exists that a new sequence can fall outside of this range
  
    self.hydroInfo = {}
    
    if normalise and not normValues:
      maxValue = -999
      minValue = 999
      
    for seqId in self.seqInfo.keys():
      (seqIndexes,dataValues) = self.getHydrophobProfile(seqId)
      
      if normalise and not normValues:
        if min(dataValues) < minValue:
          minValue = min(dataValues)
        if max(dataValues) > maxValue:
          maxValue = max(dataValues)
          
      self.hydroInfo[seqId] = (seqIndexes,dataValues)
          
    if normalise:
    
      if not normValues:
        multFact = 1.0 / (maxValue - minValue)
        for seqId in self.hydroInfo.keys():
          (seqIndexes,dataValues) = self.hydroInfo[seqId]
          for i in range(len(dataValues)):
            dataValues[i] = (dataValues[i] - minValue) * multFact
    
  def getHydrophobProfile(self,seqId):
  
    if self.hydroInfo and seqId in self.hydroInfo.keys():
      (seqIndexes,dataValues) = self.hydroInfo[seqId]
    
    else:
      ps = ProtScale()
      ps.setSequence(self.seqInfo[seqId]['sequence'])

      seqIndexes = range(1,len(self.seqInfo[seqId]['sequence']) + 1)

      dataValues = ps.calcProfile(self.hydrophobScale,window=self.windowLength,windowEdgeScaling=self.windowEdgeScaling,windowDependency=self.windowDependency)
    
    return (seqIndexes,dataValues)

  def validSeqId(self,seqId):

    # Is this a valid sequence ID?
  
    isValid = True
    if self.validSeqIds and seqId not in self.validSeqIds:
      isValid = False
      
    return isValid
 
  def readFilterFile(self,fileName):
  
    # Read a FASTA file to filter the original set of sequences
    # Activate or subclass where necessary
  
    self.graphSuffix = "_filtered"
  
    fin = open(fileName)
    lines = fin.readlines()
    fin.close()
    
    self.validSeqIds = []
    for line in lines:
      if line.startswith(">"):
        self.validSeqIds.append(line[1:].strip())    

  def initColumnList(self):
  
    columnLists = []
    for i in range(len(self.dynaMineRange)):
      columnLists.append([])
      for j in range(len(self.hydroRange)):
        columnLists[-1].append(0)
    
    return columnLists

  def getHeatPlotCoords(self,dynaMineValue,hydroValue):
  
    xCoord = int((dynaMineValue - self.dynaMineRange[0]) // (self.dynaMineRange[1] - self.dynaMineRange[0]))
    yCoord = int((hydroValue - self.hydroRange[0]) // (self.hydroRange[1] - self.hydroRange[0]))
    
    listPos = xCoord * self.hydroTotal + yCoord
    
    return (xCoord,yCoord) 

  def doFoldingResiduesAnalysis(self):
  
    earlyFoldNbiDataNorm = {'group': [1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 4.0, 5.0, 5.0, 6.0, 7.0, 7.0, 7.0, 7.0, 7.0, 7.0, 7.0, 8.0, 10.0, 10.0, 10.0, 11.0, 11.0, 11.0, 13.0, 13.0, 15.0, 17.0, 17.0, 17.0, 17.0, 17.0, 17.0, 17.0, 19.0, 19.0, 19.0, 19.0, 19.0, 21.0, 21.0, 21.0, 22.0, 22.0, 23.0, 23.0, 23.0, 23.0, 23.0, 24.0, 25.0, 27.0, 27.0, 27.0, 29.0, 29.0, 29.0, 30.0, 33.0, 35.0, 35.0, 35.0, 35.0, 35.0, 35.0, 35.0, 36.0, 36.0, 36.0, 38.0, 40.0],
                            'n': [110.0, 23.0, 32.0, 11.0, 72.0, 7.0, 87.0, 16.0, 35.0, 19.0, 106.0, 5.0, 26.0, 9.0, 38.0, 24.0, 129.0, 22.0, 89.0, 23.0, 28.0, 5.0, 86.0, 10.0, 49.0, 5.0, 50.0, 10.0, 50.0, 12.0, 74.0, 10.0, 89.0, 15.0, 62.0, 25.0, 17.0, 5.0, 44.0, 18.0],
                            'names': ['An', 'Af', 'Cn', 'Cf', 'Dn', 'Df', 'En', 'Ef', 'Fn', 'Ff', 'Gn', 'Gf', 'Hn', 'Hf', 'In', 'If', 'Kn', 'Kf', 'Ln', 'Lf', 'Mn', 'Mf', 'Nn', 'Nf', 'Pn', 'Pf', 'Qn', 'Qf', 'Rn', 'Rf', 'Sn', 'Sf', 'Tn', 'Tf', 'Vn', 'Vf', 'Wn', 'Wf', 'Yn', 'Yf'],
                            'conf': [[0.629916179926237, 0.7834181979912406, 0.6125628565844665, 0.7603538839220506, 0.6364623793660849, 0.7486452525451928, 0.638286155368152, 0.8253423305014905, 0.650309257206283, 0.8124294719592761, 0.5076884677527513, 0.6937230238917831, 0.6093630800702896, 0.6676481118271536, 0.7407578409644567, 0.8024280535870766, 0.668673919855897, 0.6897478579072447, 0.6951970414955706, 0.7835398799290713, 0.6157236714828365, 0.895450713856785, 0.6132134393671455, 0.7417294147579605, 0.5800476879388616, 0.6920357607201587, 0.6243055009999848, 0.7355359589507758, 0.6487507371449703, 0.8069428666458911, 0.5349902024198622, 0.7616084933185953, 0.6158496353870119, 0.6889336607020794, 0.7249379606495517, 0.8416792832629864, 0.6334918849934653, 0.7347344473049284, 0.6325583142232667, 0.8593603266927543], [0.7122933184854655, 0.8793724996831779, 0.8043046132950513, 0.8643600748880177, 0.7286418732608613, 0.9355652737705964, 0.7105510539341737, 0.9050354329388034, 0.8035060038378936, 0.9126890114530465, 0.5859089009289311, 0.7991679713688804, 0.7180801855799599, 0.9641348339092803, 0.8263985894521865, 0.9200211397923493, 0.7240201440710438, 0.8548257855036081, 0.7626342838056341, 0.9191628227736316, 0.752778110360729, 0.9434118453849215, 0.6903055330129627, 0.9039347754973082, 0.6968401839146852, 0.8501329139786366, 0.7113669126069849, 0.8566237120972965, 0.7381224961069439, 0.9786045476049319, 0.6468344299396006, 0.9278892235763817, 0.7053348748635578, 0.8760210220773771, 0.822556436646455, 0.9221761384237609, 0.8471459282183799, 0.8411691671529029, 0.7602299002459845, 0.943628948023185]],
                            'stats': [[0.16247139588100698, 0.6967871485943775, 0.10574018126888195, 0.7117117117117117, 0.24114671163575035, 0.7108433734939759, 0.2986512524084779, 0.7009132420091324, 0.13493975903614447, 0.6506622516556292, 0.04304635761589399, 0.676737160120846, 0.3867276887871854, 0.5858123569794049, 0.49658314350797267, 0.6120481927710845, 0.32494279176201374, 0.5039370078740156, 0.34703196347031967, 0.5502008032128515, 0.3781321184510252, 0.8954468802698144, 0.20853080568720375, 0.708433734939759, 0.10642570281124496, 0.6155606407322655, 0.325740318906606, 0.5679758308157099, 0.38554216867469865, 0.5301204819277109, 0.0, 0.6361445783132529, 0.10068649885583532, 0.48198198198198194, 0.27625570776255703, 0.6270022883295194, 0.54739336492891, 0.763855421686747, 0.16279069767441864, 0.7156398104265402], [0.5377643504531721, 0.7924418604651162, 0.4496608711310297, 0.783797351568436, 0.5816682596756331, 0.7255123211880756, 0.5427378806564842, 0.8089400067408155, 0.5523022321845521, 0.7918709818023317, 0.4216867469879517, 0.7061611374407583, 0.562015503875969, 0.6763565891472869, 0.7073643410852714, 0.7441427035372612, 0.5981735159817351, 0.6425702811244979, 0.6072289156626507, 0.7156809998758433, 0.5673158386972548, 0.8954468802698144, 0.5060240963855421, 0.7500000000000001, 0.4735099337748345, 0.6940639269406393, 0.5671981776765377, 0.7209302325581396, 0.5639269406392693, 0.7761795793832407, 0.4048338368580061, 0.6979405034324943, 0.5009633911368016, 0.5951661631419939, 0.6261261261261263, 0.7991718426501033, 0.6537585421412302, 0.783132530120482, 0.532085006947949, 0.8123569794050342], [0.6711047492058513, 0.8313953488372092, 0.7084337349397589, 0.8123569794050342, 0.6825521263134731, 0.8421052631578946, 0.6744186046511629, 0.865188881720147, 0.7269076305220883, 0.8625592417061613, 0.5467986843408412, 0.7464454976303317, 0.6637216328251248, 0.815891472868217, 0.7835782152083216, 0.861224596689713, 0.6963470319634704, 0.7722868217054264, 0.7289156626506024, 0.8513513513513514, 0.6842508909217827, 0.9194312796208532, 0.6517594861900541, 0.8228320951276343, 0.6384439359267734, 0.7710843373493976, 0.6678362068034849, 0.7960798355240362, 0.6934366166259571, 0.8927737071254115, 0.5909123161797314, 0.8447488584474885, 0.6605922551252849, 0.7824773413897282, 0.7737471986480033, 0.8819277108433736, 0.7403189066059226, 0.7879518072289157, 0.6963941072346256, 0.9014946373579696], [0.8111753371868979, 0.9380686531396896, 0.7929061784897025, 0.8929585893241843, 0.8291900769642672, 0.8820135525872315, 0.7560423430483512, 0.9098173515981735, 0.8391136801541426, 0.930168830150202, 0.6765375854214122, 0.7807757166947724, 0.7374429223744293, 0.9578313253012047, 0.8744292237442921, 0.9264484367537383, 0.7971014492753622, 0.8875968992248062, 0.8085585585585586, 0.9215116279069768, 0.7968170800570054, 0.9293849658314353, 0.7322654462242564, 0.9123222748815167, 0.7322274881516587, 0.8059360730593605, 0.7620137299771168, 0.8421052631578946, 0.7639123102866779, 0.964361068708391, 0.7093023255813954, 0.8643410852713178, 0.7681159420289856, 0.8244656045636448, 0.8693693693693695, 0.9265402843601898, 0.9325301204819276, 0.8584474885844747, 0.800084214053606, 0.9254966887417218], [0.9975903614457832, 0.9922928709055877, 0.9819819819819819, 0.9928909952606635, 1.0, 0.9324324324324326, 1.0, 0.9879518072289156, 1.0, 1.0, 0.8920741989881955, 0.8013245033112583, 0.8906605922551254, 0.9748062015503878, 0.9865092748735245, 1.0, 0.9813664596273292, 0.9864864864864865, 0.9931662870159453, 1.0, 0.9549549549549552, 0.9293849658314353, 0.9184290030211479, 0.9863013698630136, 0.9365558912386707, 0.8060708263069141, 0.927107061503417, 0.8990066225165562, 0.9139966273187184, 1.0, 0.9061784897025171, 0.9360730593607306, 0.9668737060041408, 0.9304635761589404, 1.0, 1.0, 0.9969788519637462, 0.8584474885844747, 1.0, 1.0]],
                            'out': [0.0, 0.0, 0.04833836858006047, 0.027397260273972622, 0.5228915662650603, 0.5709969788519637, 0.10585585585585597, 0.1722054380664651, 0.11180124223602482, 0.4009009009009008, 0.05034324942791766, 0.04328018223234629, 0.015414258188824677, 0.13102119460500963, 0.1946050096339113, 0.0, 0.0, 0.22356495468277934, 0.5675057208237986, 0.26283987915407847, 0.4750656167979001, 0.0, 0.02484472049689443, 0.018549747048903897, 0.29612756264236906, 0.2892938496583144, 0.3507109004739338, 0.19431279620853076, 0.03614457831325278, 0.03661327231121284, 0.21184510250569472, 0.034136546184738985, 0.21396396396396392, 0.03468208092485552, 0.0, 0.0, 0.11389521640091102, 0.16441441441441432, 0.25193798449612415, 0.15165876777251175, 0.0888382687927108, 0.20472440944881878, 0.5253012048192771, 0.9954954954954955, 0.0545023696682465, 0.0, 0.0, 0.09667673716012061, 0.060041407867494755, 0.187311178247734, 0.03424657534246578, 0.20273348519362183, 0.06007751937984501, 0.0, 0.09397590361445766, 0.17218543046357612, 0.22146118721461178, 0.22747747747747746, 0.0, 0.06506024096385522, 0.0, 0.13439635535307506, 0.0, 0.13242009132420102, 0.246376811594203, 0.2545931758530183, 0.5924170616113745, 0.5858123569794049, 0.3051359516616314, 0.99031007751938, 0.5789473684210525]}
                        
                        
    earlyFoldNbiDataOrig = {'group': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 4.0, 5.0, 5.0, 5.0, 5.0, 5.0, 7.0, 7.0, 7.0, 7.0, 7.0, 7.0, 7.0, 8.0, 10.0, 11.0, 11.0, 11.0, 11.0, 15.0, 15.0, 17.0, 17.0, 17.0, 19.0, 19.0, 19.0, 19.0, 21.0, 23.0, 23.0, 24.0, 25.0, 25.0, 25.0, 27.0, 27.0, 29.0, 29.0, 29.0, 30.0, 31.0, 31.0, 31.0, 33.0, 33.0, 33.0, 34.0, 35.0, 35.0, 35.0, 35.0, 35.0, 36.0, 38.0, 38.0, 39.0, 40.0],
                            'n': [110.0, 23.0, 32.0, 11.0, 72.0, 7.0, 87.0, 16.0, 35.0, 19.0, 106.0, 5.0, 26.0, 9.0, 38.0, 24.0, 129.0, 22.0, 89.0, 23.0, 28.0, 5.0, 86.0, 10.0, 49.0, 5.0, 50.0, 10.0, 50.0, 12.0, 74.0, 10.0, 89.0, 15.0, 62.0, 25.0, 17.0, 5.0, 44.0, 18.0], 
                            'names': ['An', 'Af', 'Cn', 'Cf', 'Dn', 'Df', 'En', 'Ef', 'Fn', 'Ff', 'Gn', 'Gf', 'Hn', 'Hf', 'In', 'If', 'Kn', 'Kf', 'Ln', 'Lf', 'Mn', 'Mf', 'Nn', 'Nf', 'Pn', 'Pf', 'Qn', 'Qf', 'Rn', 'Rf', 'Sn', 'Sf', 'Tn', 'Tf', 'Vn', 'Vf', 'Wn', 'Wf', 'Yn', 'Yf'], 
                            'conf': [[0.7714768789316092, 0.823903380773179, 0.7591125834825885, 0.8356528452896993, 0.7894381357858917, 0.7774900616624784, 0.7500278965835104, 0.835985, 0.7749607974118145, 0.8340018512387609, 0.7121636410521917, 0.7878437360319436, 0.7324010570250152, 0.7787, 0.8181087895860305, 0.8479390956298787, 0.7825586520048613, 0.7873294301466133, 0.7860747978503665, 0.8205721850950364, 0.7562901025991098, 0.8658315788568147, 0.7448511483840138, 0.7940367332287264, 0.7124628571428572, 0.7693767234363934, 0.733909956600261, 0.7935226881706839, 0.7754429916573448, 0.8206965817454008, 0.70949007754045, 0.7950266495973113, 0.7600650418698744, 0.7821000263695547, 0.8090227565227448, 0.837772, 0.8120442592700631, 0.8699340251911006, 0.7615122094395692, 0.8394416917145563], [0.8055231210683907, 0.8640966192268209, 0.8498874165174115, 0.9023471547103007, 0.8255618642141085, 0.9005099383375216, 0.7879721034164896, 0.8810150000000001, 0.8270392025881856, 0.8919981487612391, 0.7508363589478084, 0.8641562639680563, 0.7825989429749849, 0.8893, 0.8498912104139694, 0.8840609043701213, 0.8134413479951388, 0.8836705698533865, 0.8239252021496336, 0.8894278149049636, 0.8157098974008903, 0.9661684211431854, 0.7891488516159862, 0.8959632667712736, 0.7675371428571428, 0.8386232765636067, 0.7870900433997389, 0.856477311829316, 0.8165570083426553, 0.918303418254599, 0.75650992245955, 0.8689733504026886, 0.8019349581301256, 0.8538999736304452, 0.8559772434772552, 0.890228, 0.8779557407299369, 0.8840659748088994, 0.817487790560431, 0.8945583082854437]], 
                            'stats': [[0.558, 0.769, 0.577, 0.826, 0.608, 0.688, 0.539, 0.745, 0.602, 0.707, 0.482, 0.766, 0.64, 0.727, 0.719, 0.785, 0.552, 0.673, 0.583, 0.652, 0.592, 0.77, 0.51, 0.8, 0.521, 0.74, 0.546, 0.73, 0.64, 0.772, 0.506, 0.729, 0.515, 0.718, 0.579, 0.727, 0.704, 0.869, 0.615, 0.804], [0.722, 0.823, 0.6910000000000001, 0.8314999999999999, 0.7475, 0.7595000000000001, 0.703, 0.819, 0.7395, 0.8109999999999999, 0.663, 0.789, 0.717, 0.763, 0.798, 0.835, 0.715, 0.739, 0.741, 0.7835000000000001, 0.728, 0.857, 0.694, 0.805, 0.687, 0.779, 0.709, 0.787, 0.757, 0.8089999999999999, 0.67, 0.776, 0.696, 0.739, 0.75, 0.827, 0.807, 0.869, 0.715, 0.84], [0.7885, 0.844, 0.8045, 0.869, 0.8075000000000001, 0.839, 0.769, 0.8585, 0.801, 0.863, 0.7315, 0.826, 0.7575000000000001, 0.834, 0.834, 0.866, 0.798, 0.8354999999999999, 0.805, 0.855, 0.786, 0.916, 0.767, 0.845, 0.74, 0.804, 0.7605, 0.825, 0.796, 0.8694999999999999, 0.733, 0.832, 0.781, 0.818, 0.8325, 0.864, 0.845, 0.877, 0.7895000000000001, 0.867], [0.835, 0.884, 0.8534999999999999, 0.9015, 0.8445, 0.8625, 0.815, 0.876, 0.837, 0.891, 0.789, 0.843, 0.798, 0.868, 0.86, 0.891, 0.826, 0.882, 0.854, 0.888, 0.8274999999999999, 0.928, 0.824, 0.907, 0.809, 0.828, 0.828, 0.85, 0.849, 0.916, 0.798, 0.85, 0.821, 0.827, 0.867, 0.91, 0.893, 0.879, 0.8325, 0.914], [0.966, 0.945, 0.946, 0.947, 0.957, 0.924, 0.931, 0.9, 0.946, 0.983, 0.859, 0.882, 0.911, 0.916, 0.952, 0.959, 0.915, 0.948, 0.956, 0.932, 0.934, 0.952, 0.907, 0.927, 0.899, 0.872, 0.927, 0.941, 0.929, 0.989, 0.891, 0.885, 0.908, 0.882, 0.967, 0.959, 0.945, 0.879, 0.919, 1.002]], 
                            'out': [0.398, 0.542, 0.475, 0.498, 0.378, 0.527, 0.487, 0.451, 0.52, 0.725, 0.731, 0.557, 0.58, 0.502, 0.599, 0.495, 0.469, 0.493, 0.377, 0.437, 0.47, 0.524, 0.441, 0.526, 0.616, 0.629, 0.424, 0.413, 0.453, 0.337, 0.676, 0.971, 0.487, 0.395, 0.387, 0.528, 0.552, 0.57, 0.543, 0.559, 0.369, 0.47, 0.604, 0.431, 0.501, 0.49, 0.444, 0.326, 0.591, 0.502, 0.572, 0.611, 0.475, 0.473, 0.394, 0.477, 0.489, 0.458, 0.96, 0.471, 0.52, 0.564, 0.533, 0.56, 0.643, 0.924, 0.851, 0.497, 0.724]}

    
    
    perAaData = {}
    for i in range(len(earlyFoldNbiDataNorm['names'])):
    
      aaName = earlyFoldNbiDataNorm['names'][i][0]
      foldCode = earlyFoldNbiDataNorm['names'][i][1]
      
      if aaName not in perAaData.keys():
        perAaData[aaName] = {}
        
      # Track lower and upper margin of box
      perAaData[aaName][foldCode] = ([earlyFoldNbiDataNorm['stats'][j][i] for j in range(5)],[earlyFoldNbiDataOrig['stats'][j][i] for j in range(5)])
    
      """
      print earlyFoldNbiData['names'][i], earlyFoldNbiData['out'][i]
      print earlyFoldNbiData['conf'][0][i], earlyFoldNbiData['conf'][1][i]
      # This is lower bar, lower box, average, higher box, upper bar
      print earlyFoldNbiData['stats'][0][i], earlyFoldNbiData['stats'][1][i], earlyFoldNbiData['stats'][2][i], earlyFoldNbiData['stats'][3][i], earlyFoldNbiData['stats'][4][i]
      """

    self.verbose = False
    self.dynaMineInfo = {}        
     
    seqClasses = self.seqClasses
    ignoreSeqIds = []
         
    #
    # Loop over all the protein classes
    #
    
    for seqClass in seqClasses:
    
      print seqClass
      counter = 0

      for seqId in self.seqInfo.keys():
      
        if not self.validSeqId(seqId) or seqId in ignoreSeqIds:
          continue

        if self.seqInfo[seqId]['seqClass'] != seqClass:
          continue
          
        # TODO could in principle extend with other info here as well... secondary structure! Sidechain!
        self.normaliseDynaMine = False   
        (dmpOrig,seqIndexes,dataValuesOrig) = self.getDynaMineAndHydro(seqId)

        self.normaliseDynaMine = True   
        (dmpNorm,seqIndexes,dataValuesNorm) = self.getDynaMineAndHydro(seqId)
        
        if not dmpOrig or not dmpOrig.yLists[0]:
          continue
        
        counter += 1
          
        foldRes = 0
        nonFoldRes = 0
        otherRes = 0
        notSureRes = 0
        binString = ""
        
        """
        TODO this is all a bit hacky... should really put this in a decent machine learning tool, so can also take
        the sequence environment into account. Should label residues with this kind of value though, should help in
        the prediction!
        """
        
        for i in range(len(dmpOrig.xLists[0])):
          seqIndex = dmpOrig.xLists[0][i]
          aaName = dmpOrig.aaNames[seqIndex]
          
          if aaName in self.aaNameString:
            dynaMineValue = dmpOrig.yLists[0][i]
            dynaMineValueNorm = dmpNorm.yLists[0][i]
            
            # Can fall in both categories at the same time!
            foldFact = 0.0
            notFoldFact = 0.0
            
            for j in range(5):
              if perAaData[aaName]['f'][0][j] <= dynaMineValueNorm and perAaData[aaName]['f'][1][j] <= dynaMineValue:
                foldFact += 0.2
            for j in range(4,-1,-1):
              if dynaMineValueNorm <= perAaData[aaName]['n'][0][j] and dynaMineValue <= perAaData[aaName]['n'][1][j]:
                notFoldFact += 0.2
            
            # Only count if exclusive!
            if foldFact > notFoldFact * 2:
              binString += "1"
            elif notFoldFact < foldFact * 2:
              binString += "0"
            else:
              binString += '-'
              
            foldRes += foldFact
            nonFoldRes += notFoldFact

        seqLen = len(dmpOrig.yLists[0])
        print "{:<30s} {:6.2f} {:6.2f} {}".format(seqId, foldRes * 1.0 / seqLen, nonFoldRes * 1.0 / seqLen,  binString)
        
        if counter == 10:
          break
        

  def doAnalysis(self,plotIndividualGraphs=True,plotOverviewGraphs=True,addGraphSuffix="",ignoreSeqIds=None,seqClasses=None,verbose=True,normaliseDynaMine=False):
  
    #
    # Initialise
    #
    
    self.verbose = verbose
    
    self.normaliseDynaMine = normaliseDynaMine
    
    if not self.dynaMineInfo:
      self.dynaMineInfo = {}
    
    if not ignoreSeqIds:
      ignoreSeqIds = []
      
    if not seqClasses:
      seqClasses = self.seqClasses
    
    r = rpy.r
    
    #
    # Initialise plots
    #
    
    if plotOverviewGraphs:
      
      self.graphSuffix += addGraphSuffix
    
      # Should be whatever is lowest/highest
      histoRange = (min(self.dynaMineRange[0],self.hydroRange[0]),max(self.dynaMineRange[-1],self.hydroRange[-1])) 

      histoLegend = []

      histoData = {}
      for histoKey in self.histoKeys:
        histoData[histoKey] = []

      histoDataPerRes = {}
      for aaName in self.aaNameString:
        histoDataPerRes[aaName] = {}
        for histoKey in self.histoKeys:
          histoDataPerRes[aaName][histoKey] = []
                
     
    #
    # Loop over all the protein classes
    #
    
    self.probMatrices = {}
    self.perResProbMatrices = {}
   
    for seqClass in seqClasses:
      
      if self.verbose:
        print seqClass 
    
      allDynaMine = []
      allHydro = []
      allSeqLen = []

      perResDynaMine = {}
      perResHydro = {}
      perResCounts = {}
      
      for aaName in self.aaNameString:
        perResDynaMine[aaName] = []
        perResHydro[aaName] = []
        perResCounts[aaName] = []
              
      if plotOverviewGraphs:
      
        mainGraphPath = os.path.join("{}/{}".format(self.graphDir,seqClass))
        if not os.path.exists(mainGraphPath):
          os.mkdir(mainGraphPath)
      
        if seqClass in self.seqClassesForAnalysis:
          for histoKey in self.histoKeys:
            histoData[histoKey].append([])
            for aaName in self.aaNameString:
              histoDataPerRes[aaName][histoKey].append([])
          histoLegend.append(seqClass)

      count = 0
      for seqId in self.seqInfo.keys():
      
        #if count == 20:
        #  break        
        if not self.validSeqId(seqId) or seqId in ignoreSeqIds:
          continue

        if self.seqInfo[seqId]['seqClass'] != seqClass:
          continue
          
        # TODO could in principle extend with other info here as well... secondary structure! Sidechain!
        (dmp,seqIndexes,dataValues) = self.getDynaMineAndHydro(seqId)
        
        if not dmp or not dmp.yLists[0]:
          continue
        
        count += 1
        
        #
        # Track data for plots
        #
        
        allDynaMine.extend(dmp.yLists[0])
        allHydro.extend(dataValues)     
        allSeqLen.extend([len(dataValues)] * len(dataValues))

        aaNames = dmp.aaNames.values()
        aaCounts = {}
        for aaName in set(aaNames):
          aaCounts[aaName] = aaNames.count(aaName)
          
        for i in range(len(dmp.xLists[0])):
          seqIndex = dmp.xLists[0][i]
          aaName = dmp.aaNames[seqIndex]
          
          if aaName in self.aaNameString:
            perResDynaMine[aaName].append(dmp.yLists[0][i])
            perResHydro[aaName].append(dataValues[i])
            perResCounts[aaName].append(aaCounts[aaName])

        if plotOverviewGraphs:
          # Also add division of the two...
          newDataValues1 = []
          newDataValues2 = []
          for i in range(len(dmp.yLists[0])):
            if dmp.yLists[1][i] != 0:
              divideValue = dmp.yLists[0][i] / dmp.yLists[1][i]
            else:
              divideValue = 1
            newDataValues1.append((None,divideValue))
            newDataValues2.append((None,dmp.yLists[0][i] - dmp.yLists[1][i]))
            
          dmp.addOtherData("Dynamics/hydro",seqIndexes,newDataValues1)
          dmp.addOtherData("diff_Dynamics_hydro",seqIndexes,newDataValues2)

          if seqClass in self.seqClassesForAnalysis:

            histoData['dynaMine'][-1].extend(dmp.yLists[0])
            histoData['hydrophob'][-1].extend(dmp.yLists[1])
            #histoData['dyna_hydroph'][-1].extend(dmp.yLists[2])
            
            for i in range(len(dmp.xLists[0])):
              seqIndex = dmp.xLists[0][i]
              aaName = dmp.aaNames[seqIndex]

              if aaName in self.aaNameString:
                histoDataPerRes[aaName]['dynaMine'][-1].append(dmp.yLists[0][i])
                histoDataPerRes[aaName]['hydrophob'][-1].append(dmp.yLists[1][i])
                #histoDataPerRes[aaName]['dyna_hydroph'][-1].append(dmp.yLists[2][i])

            if plotIndividualGraphs:
              dmp.writePlot("{}/{}_comp.png".format(mainGraphPath,seqId),"{}, {} expression".format(seqId,seqClass.lower()),ylim=(0,1))
        
      #
      # Make a heat plot
      #

      columnLists = self.initColumnList()
      columnListsNorm = self.initColumnList()

      for i in range(len(allDynaMine)):
        
        dynaMineValue = allDynaMine[i]
        hydroValue = allHydro[i]
        
        (x,y) = self.getHeatPlotCoords(dynaMineValue,hydroValue)
        
        columnLists[x][y] += 1
        columnListsNorm[x][y] += 1.0 / allSeqLen[i]
        
      #
      # Make per amino acid heat plots
      #
      
      aaNames = perResDynaMine.keys()
      aaNames.sort()
      
      perResColumnLists = {}
      perResColumnListsNorm = {}
      
      for aaName in aaNames:      

        perResColumnLists[aaName] = self.initColumnList()
        perResColumnListsNorm[aaName] = self.initColumnList()
        
        for i in range(len(perResDynaMine[aaName])):
          dynaMineValue = perResDynaMine[aaName][i]
          hydroValue = perResHydro[aaName][i]
          
          (x,y) = self.getHeatPlotCoords(dynaMineValue,hydroValue)
        
          perResColumnLists[aaName][x][y] += 1
          perResColumnListsNorm[aaName][x][y] += 1.0 / perResCounts[aaName][i]
      
      #
      # Do some cleanup, create matrices and print plots
      # 
      
      self.createAndPlotMatrices(columnLists,columnListsNorm,plotOverviewGraphs,self.graphDir,seqClass,count,probMatrixDict=self.probMatrices)  
      
      for aaName in aaNames:
        graphDir = '{}/{}'.format(self.graphDir,aaName)
        if not os.path.exists(graphDir):
          os.mkdir(graphDir)
          
        if aaName not in self.perResProbMatrices.keys():
          self.perResProbMatrices[aaName] = {}
          
        self.createAndPlotMatrices(perResColumnLists[aaName],perResColumnListsNorm[aaName],plotOverviewGraphs,graphDir,seqClass,count,probMatrixDict=self.perResProbMatrices[aaName]) 
                     
    if plotOverviewGraphs:

      stepSize = 0.05
      histRange = frange(histoRange[0]-stepSize,histoRange[1]+stepSize,stepSize)
      colourRgbList = [(0.0,0.0,0.0,0.4),(0.0,0.0,1.0,0.4),(1.0,1.0,0.0,0.5),(1.0,0.0,1.0,0.5),(0.0,1.0,1.0,0.5),(0.2,0.2,0.2,0.4)]
      histoPlotKeywds = {'density': True, 'onlyDensity': True, 'bandWidth': stepSize}  

      #for i in range(len(self.histoKeys)):
      #  for j in range(i+1,len(self.histoKeys)):

      #    histoKey = self.histoKeys[i]
      #    otherHistoKey = self.histoKeys[j]

      for histoKey in self.histoKeys:

        dmp.plotHisto("{}/histo_{}{}.png".format(self.graphDir,histoKey,self.graphSuffix),histoData[histoKey],histRange,"Histogram distributions for {} {} with subsets".format(histoKey,self.graphSuffix),'{} prediction'.format(histoKey),'Frequency',
                                             colourRgbList=colourRgbList,legendLocation='topright',legendTexts=histoLegend,legendTitle="Subsets",**histoPlotKeywds) #,ylim=(,**)

        for aaName in self.aaNameString:

          dmp.plotHisto("{}/{}/histo_{}{}.png".format(self.graphDir,aaName,histoKey,self.graphSuffix),histoDataPerRes[aaName][histoKey],histRange,"Histogram distributions for {} {}, aa {} with subsets".format(histoKey,self.graphSuffix,aaName),'{} prediction'.format(histoKey),'Frequency',
                                               colourRgbList=colourRgbList,legendLocation='topright',legendTexts=histoLegend,legendTitle="Subsets",**histoPlotKeywds) #,ylim=(,**)


  def createAndPlotMatrices(self,columnLists,columnListsNorm,plotOverviewGraphs,graphDir,seqClass,count,probMatrixDict=None):

    r = rpy.r
    
    matrix = None
    matrixNorm = None

    colMax = 0
    colMaxNorm = 0

    matrixTotal = 0

    # Normalise between 0 and 1 for comparison
    for columnListNorm in columnListsNorm:       
      curMaxNorm = max(columnListNorm)
      if curMaxNorm > colMaxNorm:
        colMaxNorm = curMaxNorm

      for value in columnListNorm:
        matrixTotal += value

    # Not sure if necessary, but try
    rpy.set_default_mode(rpy.NO_CONVERSION)

    fullList  = []
    
    if probMatrixDict != None:
      probMatrix = []

    for colIndex in range(len(columnLists)):

      columnList = columnLists[colIndex]        

      if probMatrixDict != None:
        # Normalise by total matrix value (probabilities)
        columnListProb = [value * 1.0 / matrixTotal for value in columnListsNorm[colIndex]]
        probMatrix.append(columnListProb)

      if plotOverviewGraphs:
        # Normalise to 1.0 as max
        columnListNorm = [value * 1.0 / colMaxNorm for value in columnListsNorm[colIndex]]

        if matrix == None:
          matrix = r.cbind(columnList)
          matrixNorm = r.cbind(columnListNorm)
        else:
          matrix = r.cbind(matrix,columnList)
          matrixNorm = r.cbind(matrixNorm,columnListNorm)

        curMax = max(columnList)
        if curMax > colMax:
          colMax = curMax

        fullList.extend(columnList)

    if probMatrixDict != None:
      probMatrixDict[seqClass] = probMatrix

    """
    tc = 0.0
    for i in range(len(probMatrix)):
      for j in range(len(probMatrix[i])):
        tc += probMatrix[i][j]

    print seqClass, tc
    """
    rpy.set_default_mode(rpy.BASIC_CONVERSION)

    if plotOverviewGraphs:

      #
      # Now print graphs
      # Also set color range...
      #

      numSteps = 7
      palette = r.colorRampPalette(['green','red'])
      colourList = ['white'] + palette(numSteps + 1)

      stepSize = colMax / numSteps
      if not colMax % numSteps:
        stepSize = (colMax - 1) / numSteps
      breakList = [0] + range(0,colMax + stepSize,stepSize)
      if len(breakList) > (numSteps + 3):
        breakList = breakList[:numSteps+3]
      breakList[-1] = colMax

      stepSizeNorm = 1.0 / (numSteps + 1)
      breakListNorm = [0] + frange(0.0,1.0001,stepSizeNorm)

      #print breakList, len(breakList), colMax, stepSize
      #print breakListNorm, len(breakListNorm), colMaxNorm, stepSizeNorm
      #print colourList

      legendTexts = []
      legendTextsNorm = []
      for i in range(len(breakList) - 1):
        legendTexts.append("{:.2f}-{:.2f}".format(breakList[i],breakList[i+1]))
        legendTextsNorm.append("{:.2f}-{:.2f}".format(breakListNorm[i],breakListNorm[i+1]))

      #
      # Make sure to account for offset in plot itself
      #

      rpy.set_default_mode(rpy.NO_CONVERSION)
      yListOut = r.cbind(self.dynaMineRange + [1.2])
      xListOut = r.cbind(self.hydroRange + [1.0])
      rpy.set_default_mode(rpy.BASIC_CONVERSION)

      #xlim = (self.dynaMineRange[0],self.dynaMineRange[-1])
      #ylim = (self.hydroRange[0],self.hydroRange[-1])

      graphFilePath = "{}/heat_{}{}.png".format(graphDir,seqClass,self.graphSuffix)

      plotImageGraph(graphFilePath,xListOut,yListOut, matrix,
                     main = 'Heat plot {}, {} points'.format(seqClass,count),# %s %s' % (ccpCode,heavyAtomNameKey),
                     xlab = "hydrophobicity",
                     ylab = "dynaMine",
                     colourList = colourList,
                     breakList = breakList,
                     legendTexts = legendTexts,
                     #xlim = xlim,
                     #ylim = ylim,
                     plotGrid=True,
                     legendTitle = "Legend",
                     legendLocation = "bottomright")


      graphFilePath = "{}/heat_norm_{}{}.png".format(graphDir,seqClass,self.graphSuffix)

      plotImageGraph(graphFilePath,xListOut,yListOut, matrixNorm,
                     main = 'Heat plot {}, normalised, {} points'.format(seqClass,count),# %s %s' % (ccpCode,heavyAtomNameKey),
                     xlab = "hydrophobicity",
                     ylab = "dynaMine",
                     colourList = colourList,
                     breakList = breakListNorm,
                     legendTexts = legendTextsNorm,
                     #xlim = xlim,
                     #ylim = ylim,
                     plotGrid=True,
                     legendTitle = "Legend",
                     legendLocation = "bottomright")

  def getDynaMineAndHydro(self,seqId):

    (seqIndexes,dataValues) = self.getHydrophobProfile(seqId)      

    if seqId in self.dynaMineInfo:
      dmp = self.dynaMineInfo[seqId]
    
    else:
      dmp = MakeDynaMinePlot()
      self.dmp = dmp

      predictionFileName = self.getPredictionFileName("dynaMineResults",seqId)

      if self.verbose and not os.path.exists(predictionFileName):
        print "Prediction didn't run for {}, ignoring".format(seqId)
        return (None,None,None)

      dmp.readPredictionData(predictionFileName)

      if self.verbose and not dmp.predictionData:
        print "No data for {}, ignoring".format(seqId)
        return (None,None,None)

      dmp.addPredictionData(seqId,normalise=self.normaliseDynaMine)      

      # Add hydrophobicity data: NOTE will fail if running over multiple hydrophobicity values.
      dmp.addOtherData(self.hydrophobScale,seqIndexes,[(None,dataValue) for dataValue in dataValues])
      
      self.dynaMineInfo[seqId] = dmp
    
    return (dmp,seqIndexes,dataValues)
    
  def getSequenceScores(self,seqId):
  
    totalScores = {}
    totalScoresPerAa = {}

    (dmp,seqIndexes,hydroValues) = self.getDynaMineAndHydro(seqId)
    
    if dmp and dmp.yLists[0] and hydroValues:

      dynaMineValues = dmp.yLists[0]
      seqIndexes = dmp.xLists[0]

      for seqClass in self.seqClassesForAnalysis:
        totalScores[seqClass] = 0
        totalScoresPerAa[seqClass] = 0

      for i in range(len(dynaMineValues)):

        dynaMineValue = dynaMineValues[i]
        hydroValue = hydroValues[i]

        (x,y) = self.getHeatPlotCoords(dynaMineValue,hydroValue)

        for seqClass in self.seqClassesForAnalysis:
          #print self.probMatrices[seqClass][x][y],
          totalScores[seqClass] += self.probMatrices[seqClass][x][y]          
          
        aaName = dmp.aaNames[seqIndexes[i]]
        if aaName in self.aaNameString:
          for seqClass in self.seqClassesForAnalysis:
            totalScoresPerAa[seqClass] += self.perResProbMatrices[aaName][seqClass][x][y]
    
    #print
    return (totalScores, totalScoresPerAa)
 
  def getGraphSuffix(self,hydroScaleType,windowLength,windowEdgeScaling,windowDependency):

    graphSuffix = "_{}_{}".format(hydroScaleType.split("_")[1],windowLength)
    if windowEdgeScaling != 100:
      if windowDependency == 'linear':
        wd = 'l'
      else:
        wd = 'e'

      graphSuffix += "_{}{}".format(wd,windowEdgeScaling)

    return graphSuffix


  def runJackknife(self,outputFileName,refAnalSeqClass=None,testMode=False,normaliseDynaMine=False):
  
    fout = open(outputFileName,'w')

    from sbb.analysis.ProtScale import ProtScale

    """
'hydropathy_KyteDoolittle', 'hydrophilicity_Hopp', 'hydrophobicity_Aboderin', 'hydrophobicity_Abraham', 'hydrophobicity_Black',
'hydrophobicity_Bull', 'hydrophobicity_Chothia', 'hydrophobicity_Eisenberg', 'hydrophobicity_Fauchere', 'hydrophobicity_Janin',
'hydrophobicity_Manavalan', 'hydrophobicity_Meek', 'hydrophobicity_Miyazawa', 'hydrophobicity_Parker', 'hydrophobicity_Rao',
'hydrophobicity_Rose', 'hydrophobicity_Roseman', 'hydrophobicity_Sweet', 'hydrophobicity_Tanford', 'hydrophobicity_Welling',
'hydrophobicity_Wilson', 'hydrophobicity_Wolfenden']   
    """

    hydroScaleTypes = ProtScale.scores.keys()
    hydroScaleTypes.sort()
    
    if testMode:
      hydroScaleTypes = hydroScaleTypes[:1]

    for hydroScaleType in hydroScaleTypes:
      for windowLength in (1,3,5,9,15):
        for windowEdgeScaling in (10,50,100):
          for windowDependency in ('linear','exponential'):

            graphSuffix = self.getGraphSuffix(hydroScaleType,windowLength,windowEdgeScaling,windowDependency)
            print graphSuffix

            # This initialises all hydrophocity scale values for all sequences in pool
            self.setHydrophobInfo(hydroScaleType,windowLength=windowLength,windowEdgeScaling=windowEdgeScaling,windowDependency=windowDependency)

            #self.writeFastaFile('seqs.fasta')

            seqIds = self.seqInfo.keys()

            histoValuesByClass = {}

            counts = {'all': {}, 'perAa': {}}
            truePositives = {'all': 0, 'perAa': 0} # Total count always the same!

            seqClassesForDiff = []
            seqClassesForAnalysis = self.seqClassesForAnalysis

            for seqClass in seqClassesForAnalysis:
              for scoreType in counts.keys():
                counts[scoreType][seqClass] = {}
              histoValuesByClass[seqClass] = []
              for analSeqClass in seqClassesForAnalysis:
                for scoreType in counts.keys():
                  counts[scoreType][seqClass][analSeqClass] = 0
                if analSeqClass != refAnalSeqClass:
                  histoValuesByClass[seqClass].append([])
                  if analSeqClass not in seqClassesForDiff:
                    seqClassesForDiff.append(analSeqClass)

            # Full jackknife, each sequence separately
            for seqId in seqIds:

              print "  " + seqId
              seqClass = self.seqInfo[seqId]['seqClass']

              if seqClass not in seqClassesForAnalysis:
                continue

              # Filter by sequence identity here as well!!
              if not self.validSeqId(seqId):
                continue

              self.doAnalysis(plotIndividualGraphs=False,plotOverviewGraphs=False,addGraphSuffix=graphSuffix,ignoreSeqIds=[seqId],seqClasses=seqClassesForAnalysis,verbose=False,normaliseDynaMine=normaliseDynaMine)

              seqLen = len(self.seqInfo[seqId]['sequence'])
              (totalScores,totalScoresPerAa) = self.getSequenceScores(seqId)

              for (scoreType,scores) in (('all',totalScores),('perAa',totalScoresPerAa)):
                if scores:
                  #print seqClass, scores
                  seqClassFound = (None,0)
                  for analSeqClass in scores.keys():
                    if scores[analSeqClass] > seqClassFound[1]:
                      #print seqId, seqClass, analSeqClass, scores[analSeqClass], seqClassFound[1]
                      seqClassFound = (analSeqClass,scores[analSeqClass])

                  counts[scoreType][seqClass][seqClassFound[0]] += 1

                  if seqClass == seqClassFound[0]:
                    truePositives[scoreType] += 1

                  refValue = scores[refAnalSeqClass]
                  for ascIndex in range(len(seqClassesForDiff)):
                    analSeqClass = seqClassesForDiff[ascIndex]
                    histoValuesByClass[seqClass][ascIndex].append((scores[analSeqClass] - refValue) / seqLen)

            scoreTypes = truePositives.keys()
            scoreTypes.sort()
            for scoreType in scoreTypes:
              fout.write("{:<5s} {:<40s} {:5d}  {}\n".format(scoreType,graphSuffix,truePositives[scoreType],str(counts[scoreType])))
            fout.flush()

            #for seqClass in histoValuesByClass.keys():
            #  for histList in histoValuesByClass[seqClass]:
            #    print max(histList), min(histList)

            """

            Why is it that poor is overpredicted this way?!? Because the more common areas of the plot have higher values, relatively speaking, because more
            'concentrated'? But shouldn't that be cancelled out by the distribution? Could also be because of sequence identity issues, I guess.

            Several issues here: the residues in effect are 'subfragments' of the sequence (since sequence windows used), also no distinction 
            about the residue type, this could be important?

            Also possible that some regions of the plots are more predictive than others, but how to quantify or use this information? Need
            a relatively complicated machine learning method?

            Have to incorporate 10 fold cross validation, running over different hydro scales, and use difference between poor/good to have
            some indication of 'confidence'?

            """


            """

            histRange = frange(-0.01,0.01,0.0005)
            colourRgbList = [(0.0,0.0,0.0,0.4),(0.0,0.0,1.0,0.4),(0.2,0.2,0.2,0.4),(1.0,1.0,0.0,0.5),(1.0,0.0,1.0,0.5),(0.0,1.0,1.0,0.5)]

            for seqClass in seqClassesForDiff:
              histValues = histoValuesByClass[seqClass]
              self.dmp.plotHisto("{}/scoreDist_{}{}.png".format(self.graphDir,seqClass,graphSuffix),histValues,histRange,"Histogram distributions for {} with subsets".format(seqClass),'Score difference with {}'.format(refAnalSeqClass),'Frequency',
                                                       colourRgbList=colourRgbList,legendLocation='topright',legendTexts=seqClassesForDiff,legendTitle="Subsets") 
            """

            # Only do once, windowDependency not relevant
            if windowLength == 1 or windowLength == 3:
              break

          # Only do once, window edge scaling not important
          if windowLength == 1:
            break


    fout.close()

  #
  # TODO TODO get this from elsewhere?
  #
  
  def getPredictionFileName(self,dynaMineResultsDir,seqId):
    
    return os.path.join(dynaMineResultsDir,self.identifier,"results_{}.txt".format(seqId))
  

