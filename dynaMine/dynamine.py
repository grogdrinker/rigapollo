#!/usr/bin/env python
# encoding: utf-8
"""
dynamine.py

Created by Elisa Cilia on 2013-03-28.
Copyright (c) 2013 Elisa Cilia. All rights reserved.
"""

import sys
import os
import argparse

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), '../'))
from dynaMine.predictor import DynaMine, ExtendedDynaMine


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='DynaMine predictions.')
    parser.add_argument('infile', help='input file containing sequence/s in FASTA format.')
    parser.add_argument('-s', '--side-chain', action="store_true", help='include also predictions of the side-chain dynamics.')
    parser.add_argument('-p', '--secondary-structure', action="store_true", help='include also predictions of secondary structure populations.')
    parser.add_argument('-l', '--light-implementation', action="store_true", help='run a lighter DynaMine version.') # TBD
    parser.add_argument('-n', '--onefile', action="store_true", help='write the output predictions of all input sequences in a single file.')
    parser.add_argument('-a','--add-to-directory', action='store_true', help='add information to existing directory')
    parser.add_argument('--version', action='version', version='%(prog)s 2.0')
    args = parser.parse_args()
    # print args
    fbname = ''.join(args.infile.rsplit('.', 1)[:-1])

    if os.path.exists(fbname):
        if not args.add_to_directory:
            print "Error: the directory %s exists! Use option -a to overwrite it." % (fbname)
            sys.exit(1)
    else:
        os.mkdir(fbname)
        
    if args.side_chain:
        dynamine = ExtendedDynaMine(args.secondary_structure, args.light_implementation, args.onefile)
    else:
        if args.secondary_structure:
          backbone = False
        else:
          backbone = True
        dynamine = DynaMine(args.secondary_structure, args.light_implementation, args.onefile, backbone)
    if not dynamine.predict(args.infile, fbname):
        print 'The uploaded sequence/s is/are too short.'

