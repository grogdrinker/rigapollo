#!/usr/bin/env python
# encoding: utf-8
"""
runChunks.py

Created by Elisa Cilia on 2013-11-11.
Copyright (c) 2013 Elisa Cilia. All rights reserved.
"""

import sys
import time
import subprocess


if __name__ == '__main__':
        chunkSize = 5000
        count = 0
        data = []
        f = open(sys.argv[1])
        for line in f:
                if line.startswith('>'):
                        if count % chunkSize == 0 and count > 0:
                                ftmp = open('sp_%d.fasta' % (count), 'w')
                                for e in data:
                                        ftmp.write(e)
                                ftmp.close()
                                data = []
                                cmd = 'python ~/Documents/DynaMine/python/dynaMine/dynamine.py -sc sp_%d.fasta' % (count)
                                #print cmd
                                p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                                for l in p.stdout.readlines():
                                       print l,
                                retval = p.wait()
                                time.sleep(1)
                        count+=1
                data.append(line)
        f.close()