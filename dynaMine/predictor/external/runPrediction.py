import os, glob, subprocess

"""


THIS IS NOW OBSOLETE AND CAN BE REMOVED!

Look in dynaMine/predictor/wrappers for equivalent code



"""

class RunDynaMinePrediction:
  
  """

  This version for speed; does not use FC but custom parser. No dictionaries either, just lists.

  """

  # Info for ULB server
  dynaMineHostConnectUlb = "wvranken@depinf-pc3.ulb.ac.be"
  dynaMineRemoteRunDirUlb = "/Users/wvranken/Desktop/running_predictions"

  # Info for cluster
  dynaMineHostConnectVub = "wvranken@nmrclust1.vub.ac.be"
  dynaMineRemoteRunDirVub = "/home/users/wvranken/dynaMineTest"
  
  # This local
  dynaMineResultsDir = 'dynaMineResults'

  def __init__(self,identifier,predType=None,windowSize=51,runSc=True):
  
    """
    If no prediction type given, will run both backbone/sidechain prediction from cluster with
    variable window type
    
    If prediction type is given, will run specific code on ULB cluster
    """
   
    self.identifier = identifier
    
    self.predType = predType
    self.runSc = runSc # Only relevant for VUB!

    # Use ULB server if specific prediction types
    self.dynaMineHostConnect = self.dynaMineHostConnectUlb
    self.dynaMineRemoteRunDir = self.dynaMineRemoteRunDirUlb
    self.remoteLocation = "ULB"

    if predType == 'fixedWindow':
      windowSize = windowSize
    elif predType == 'perAminoAcid':
      self.dynaMineRemoteRunDir += "_paa"
      windowSize = 25
    elif predType == 'secStruc':
      self.dynaMineRemoteRunDir += "_ss"
      windowSize = 17
    else:
      # If no specific prediction type, use VUB cluster
      self.dynaMineHostConnect = self.dynaMineHostConnectVub
      self.dynaMineRemoteRunDir = self.dynaMineRemoteRunDirVub
      self.remoteLocation = "VUB"
    
    self.windowSize = windowSize
    if self.windowSize != 51:
      self.dynaMineRemoteRunDir += "_{}".format(windowSize)

  def combineFasta(self,fastaDir,filePattern):
    
    """
    Combines multiple FASTA files into one big file to send to server
    """
  
    fastaFiles = glob.glob(os.path.join(fastaDir,filePattern))
    
    allSeqs = []
    for fastaFile in fastaFiles:
      allSeqs.extend(self.readFasta(fastaFile))
   
    allSeqs.sort()
   
    combinedFile = "{}.fasta".format(self.identifier)

    fout = open(combinedFile,'w')
    
    for (seqId,sequence) in allSeqs:
      fout.write(">{} {}\n".format(seqId,seqId))
      fout.write("{}\n\n".format(sequence.upper().replace(" ","")))    

    fout.close()
    
    return combinedFile

  def readFasta(self,fileName):
    
    """
    Reads a FASTA file
    """
  
    print "Reading FASTA file {}...".format(fileName)

    # Bypassing FC; much faster to do like this...
    fin = open(fileName)
    lines = fin.readlines()
    fin.close()

    # Quick FASTA read, handles multiple line sequences
    seqs = []
    seqId = None
    sequence = ""
    for line in lines:
      if line.startswith(">"):
        # Consolidate
        if seqId:
          seqs.append((seqId,sequence))
        cols = line.split()
        seqId = self.convertSeqIdForFile(cols[0][1:])
        sequence = ""
      elif line.strip():
        sequence += line.strip()
    if seqId:
      # WARNING this is to get the filename out; has to match with scripts on server side!
      # Should have a function for this!! -> TODO with Elisa!
      seqId = self.convertSeqIdForFile(seqId)
      seqs.append((seqId,sequence))

    return seqs

  def convertSeqIdForFile(self,seqId):
  
    return seqId.replace("|","_").replace(":","_").replace(";","_").replace("-","_")

  def runDynaMineInChunks(self,fastaFileName,chunkSize=5000,removeDataOnServer=True):
    
    """
    Run DynaMine in sections of chunkSize sequences. Necessary when FASTA file too large.
    """
    
    seqs = self.readFasta(fastaFileName)
    numSeqs = len(seqs)
    
    (fastaFilePath,fastaFileBaseName) = os.path.split(fastaFileName)
    
    for i in range(0,numSeqs,chunkSize):
      tmpFastaFileName = os.path.join(fastaFilePath,"tmp_{}".format(fastaFileBaseName))

      fout = open(tmpFastaFileName,'w')
    
      for j in range(i,i+chunkSize):
        if j >= numSeqs:
          break
        (seqId,sequence) = seqs[j]
        fout.write(">{} {}\n".format(seqId,seqId))
        fout.write("{}\n\n".format(sequence))    

      fout.close()
      
      self.runDynaMine(tmpFastaFileName,fileBaseName=fastaFileBaseName,removeDataOnServer=removeDataOnServer)
        
    
  def runDynaMine(self,fastaFileName,fileBaseName=None,removeDataOnServer=False):
    
    """
    Run DynaMine on the remote server and pull back the results
    """
  
    (fastaFilePath,fastaFileBaseName) = os.path.split(fastaFileName)
    
    if not fileBaseName:
      fileBaseName = "{}.fasta".format(self.identifier)
    self.fileBaseName = fileBaseName
  
    print 'Copying {} to {} server...'.format(fastaFileName,self.remoteLocation)
    subprocess.call(["scp",fastaFileName,"{}:{}/{}".format(self.dynaMineHostConnect,self.dynaMineRemoteRunDir,self.fileBaseName)])

    print 'Running DynaMine remotely...'
       
    if not self.predType:
      options = ['-np']
      if self.runSc:
        options.append('-sc')
      runCommand = "cd {}; python dynamine.py {} {}".format(self.dynaMineRemoteRunDir,' '.join(options),self.fileBaseName)
   
    elif self.predType == 'fixedWindow':
      runCommand = "cd {}; ./run.sh {} {}".format(self.dynaMineRemoteRunDir,fileBaseName,self.windowSize)
    else:
      runCommand = "cd {}; python wrapper_predict.py {}".format(self.dynaMineRemoteRunDir,fileBaseName)
    
    subprocess.call(["ssh",self.dynaMineHostConnect,runCommand])
      
    self.dynaMineOutDir = '.'.join(self.fileBaseName.split('.')[:-1])
    remoteTarArchiveName = "{}.tgz".format(self.dynaMineOutDir)
 
    print 'Creating archive...'
    if not self.predType or self.predType in ('secStruc','fixedWindow'):
      mainRemoteDir = self.dynaMineRemoteRunDir
    else:
      mainRemoteDir = os.path.join(self.dynaMineRemoteRunDir,"results")
      
    #subprocess.call(["ssh",self.dynaMineHostConnect,"cd {}; tar cvfz {} --exclude 'map.txt' --exclude 'targets.txt' --exclude 'dataset.arff' --exclude 'predictions_test.txt' --exclude '?' {} ".format(mainRemoteDir,remoteTarArchiveName,self.dynaMineOutDir)])
    subprocess.call(["ssh",self.dynaMineHostConnect,"cd {}; tar cvfz {} --exclude 'dataset.arff' --exclude '*.txt' --exclude '?' {} ".format(mainRemoteDir,remoteTarArchiveName,self.dynaMineOutDir)])
   
    print 'Fetching results...'
    if not os.path.exists(self.dynaMineResultsDir):
      os.mkdir(self.dynaMineResultsDir)
    subprocess.call(["scp","-r","{}:{}/{}".format(self.dynaMineHostConnect,mainRemoteDir,remoteTarArchiveName),self.dynaMineResultsDir])

    if removeDataOnServer:
      print 'Removing remote data'
      subprocess.call(["ssh",self.dynaMineHostConnect,"cd {}; rm -r {} ".format(self.dynaMineRemoteRunDir,self.dynaMineOutDir)])
      subprocess.call(["ssh",self.dynaMineHostConnect,"cd {}; rm {} ".format(self.dynaMineRemoteRunDir,fileBaseName)])
      subprocess.call(["ssh",self.dynaMineHostConnect,"cd {}; rm {} ".format(self.dynaMineRemoteRunDir,remoteTarArchiveName)])

    print 'Unpacking archived results...'
    subprocess.call(["tar","xvfz",os.path.join(self.dynaMineResultsDir,remoteTarArchiveName),'-C',self.dynaMineResultsDir])

  def runSingleDynaMineWithPickle(self,fastaFileName,pickleFileName,sequenceDict):

    try:
      from pdbe.analysis.Util import getPickledDict, createPickledDict
      from dynaMine.analysis.makePlot import MakeSimpleDynaMinePlot
    except:
      raise("Requires dependencies!")
  
    if os.path.exists(pickleFileName):
      # Read pickled dictionary!
      infoDict = getPickledDict(pickleFileName)
      
    else:
      sequenceIds = sequenceDict.keys()
      sequenceIds.sort()
      
      fout = open(fastaFileName,'w')
      for sequenceId in sequenceIds:  
        fout.write(">{} temp\n".format(sequenceId))
        fout.write("{}\n\n".format(sequenceDict[sequenceId]))
      fout.close()
      
      self.runDynaMine(fastaFileName,removeDataOnServer=True)
      
      infoDict = {}
      
      for sequenceId in sequenceIds:
        resultsFilePath = os.path.join(self.dynaMineResultsDir,self.dynaMineOutDir,"results_{}.txt".format(sequenceId))
      
        mdp = MakeSimpleDynaMinePlot()
        mdp.readPredictionData(resultsFilePath)
      
        infoDict[sequenceId] = mdp.predictionData
      
      createPickledDict(pickleFileName,infoDict)

    return infoDict


class RunFullPrediction:

  def __init__(self,identifier,fastaFile,runChunkSize=None):
  
    self.identifier = identifier
    self.fastaFile = fastaFile
    self.runChunkSize = runChunkSize

  def runPredictions(self,dataTypes,runPrediction=True,removeDataOnServer=True,dynaMineResultsDir=None,runBackboneAndSc=True):
    
    from dynaMine.predictor.external.runPrediction import RunDynaMinePrediction

    rdm = RunDynaMinePrediction(self.identifier)
    if runPrediction:
    
      if dynaMineResultsDir:
        rdm.dynaMineResultsDir = dynaMineResultsDir

      if runBackboneAndSc:
        # Backbone (variable window length) and sidechain prediction
        if self.runChunkSize:
          rdm.runDynaMineInChunks(self.fastaFile,chunkSize=self.runChunkSize,removeDataOnServer=removeDataOnServer)
        else:
          rdm.runDynaMine(self.fastaFile,removeDataOnServer=removeDataOnServer)

      if 'secStruc' in dataTypes:
        # Secondary structure
        rdm = RunDynaMinePrediction(self.identifier,predType='secStruc')

        if dynaMineResultsDir:
          rdm.dynaMineResultsDir = dynaMineResultsDir

        if self.runChunkSize:
          rdm.runDynaMineInChunks(self.fastaFile,chunkSize=self.runChunkSize,removeDataOnServer=removeDataOnServer)
        else:
          rdm.runDynaMine(self.fastaFile,removeDataOnServer=removeDataOnServer)

    # Identifiers for the output files
    seqIds = rdm.readFasta(self.fastaFile)
  
    return (rdm.dynaMineResultsDir,seqIds)
