import os, glob

class DynaMineFileParsers:

  ssTypes = ('helix','sheet','coil')
  
  #
  # DynaMine file parsers
  #
  
  def readDynaMineMultiEntry(self,filePath,convertSeqIds=True):
  
    predictions = {}  

    fin = open(filePath)
    lines = fin.readlines()
    fin.close()

    seqId = None

    for line in lines:

      if line.startswith("* for "):
        seqId = line[6:].strip()
        if convertSeqIds:
          seqId = self.convertSeqIdForFile(seqId)
        predictions[seqId] = []

      elif not line.startswith("*"):

        cols = line.split()

        if cols:
          (aaCode,predictionValue) = cols

          # Keep this minimal, sequence index follows from position in list (+1)
          predictions[seqId].append((aaCode,float(predictionValue)))
          
    return predictions

  def convertSeqIdForFile(self,seqId):
  
    return seqId.replace("|","_").replace(":","_").replace(";","_").replace("-","_").replace(",","_").replace(".","_").replace("__","_")

  def readJointSsPredictionFile(self,filePath):
  
    helixValues = []
    sheetValues = []
    coilValues = []
    predictionValues = []
  
    fin = open(filePath)
    lines = fin.readlines()
    fin.close()
    
    for line in lines:
      cols = line.split()
      if cols and len(cols) == 5:
        (aaCode,helix,sheet,coil,prediction) = cols
        helixValues.append((aaCode,helix))
        sheetValues.append((aaCode,sheet))
        coilValues.append((aaCode,coil))
        predictionValues.append(prediction)
    
    return (helixValues,sheetValues,coilValues,predictionValues)

  def readSsPredictionFiles(self,filePaths):

    assert len(filePaths) == 3, "Need file for helix, sheet, coil" 
  
    ssValues = []
   
    for ssType in self.ssTypes:
      for filePath in filePaths:
        if filePath.count(ssType):

          ssValues.append([])
        
          fin = open(filePath)
          lines = fin.readlines()
          fin.close()
          
          for line in lines:

            if not line.startswith("*"):
              cols = line.split()

              if cols:
                (aaCode,ssValue) = cols
                ssValues[-1].append((aaCode,ssValue))

    assert len(ssValues) == 3, "Need values for helix, sheet, coil" 
    
    return ssValues 

  def readSsPredictionFilesMultiEntry(self,filePaths):

    assert len(filePaths) == 3, "Need file for helix, sheet, coil" 
  
    ssValueDict = {}
   
    for ssType in self.ssTypes:
      for filePath in filePaths:
        if filePath.count(ssType):

          ssValueDict[ssType] = self.readDynaMineMultiEntry(filePath)

    assert len(ssValueDict) == 3, "Need values for helix, sheet, coil" 
    
    return ssValueDict 
    
  #
  # FASTA specific code
  #

  def readFasta(self,fileName):
    
    """
    Reads a FASTA file
    """
  
    print "Reading FASTA file {}...".format(fileName)

    # Bypassing FC; much faster to do like this...
    fin = open(fileName)
    lines = fin.readlines()
    fin.close()

    # Quick FASTA read, handles multiple line sequences
    seqs = []
    seqId = None
    sequence = ""
    for line in lines:
      if line.startswith(">"):
        # Consolidate
        if seqId:
          seqs.append((seqId,sequence))
        cols = line.split()
        seqId = self.convertSeqIdForFile(cols[0][1:])
        sequence = ""
      elif line.strip():
        sequence += line.strip()
    if seqId:
      # WARNING this is to get the filename out; has to match with scripts on server side!
      # Should have a function for this!! -> TODO with Elisa!
      seqId = self.convertSeqIdForFile(seqId)
      seqs.append((seqId,sequence))

    return seqs

  def combineFasta(self,fastaDir,filePattern):
    
    """
    Combines multiple FASTA files into one big file to run DynaMine on, if required
    """
  
    fastaFiles = glob.glob(os.path.join(fastaDir,filePattern))
    
    allSeqs = []
    for fastaFile in fastaFiles:
      allSeqs.extend(self.readFasta(fastaFile))
   
    allSeqs.sort()
   
    combinedFile = "{}.fasta".format(self.identifier)

    fout = open(combinedFile,'w')
    
    for (seqId,sequence) in allSeqs:
      fout.write(">{} {}\n".format(seqId,seqId))
      fout.write("{}\n\n".format(sequence.upper().replace(" ","")))    

    fout.close()
    
    return combinedFile
        
  def writeFasta(self,fastaFileName,sequences):
    
    fout = open(fastaFileName,'w')
    
    for (seqId,sequence) in sequences:
      fout.write(">{} {}\n".format(seqId,seqId))
      fout.write("{}\n\n".format(sequence))
    
    fout.close()
