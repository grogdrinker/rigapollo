#!/usr/bin/env python
# encoding: utf-8
"""
utils.py

Created by Elisa Cilia on 2014-08-14.
Copyright (c) 2014 Elisa Cilia. All rights reserved.
"""

import bz2
import cPickle as pickle

class Pickle:
	def __init__(self):
		#print "LOAD"		
		pass

	def dump_data(self, filename, data):
		# compression 9 is default
		with bz2.BZ2File(filename, 'wb') as f:
			pickle.dump(data, f, protocol=pickle.HIGHEST_PROTOCOL)

	def load_data(self, filename):
		f =  bz2.BZ2File(filename, 'rb')
		data = pickle.load(f)
		return data
		return None
