#!/usr/bin/env python
# encoding: utf-8
"""
predictor.py

Created by Elisa Cilia on 2013-03-28.
Copyright (c) 2013 Elisa Cilia. All rights reserved.
"""

import os
import subprocess
from datetime import datetime

from produce_input import *
from print_predictions import *
import config
from pkl_utils import Pickle
from utils import *
#from dynaMine.analysis.makePlot import MakeDynaMinePlot


class _DynaMineGeneric:
	def __init__(self, ssp=False, light=True, extended=False, allinoneout=False, backbone=False):
		self.windowsize = 51
		self.extended = extended
		self.backbone = backbone
		self.ssp = ssp
		self.light = light
		self.allinone = allinoneout
		#self.model = {} # Modified to avoid extraneous pickle loading
		self.modelDict = {}
		self.modelBias = {}
		self.input = False
		
		self.version = '3.0'

	def checkReadModel(self, type_='backbone'):
		modelLoaded = False
		
		modelKey = (type_,self.windowsize)
		
		if modelKey in self.modelDict.keys():
		  modelLoaded = True
		else:
		  pklfile = '%s/read%d.model.pkl.bz2' % (os.path.join(config.model_path, type_), self.windowsize)

		  if os.path.isfile(pklfile):
			  p = Pickle()
			  model = p.load_data(pklfile)
			  
			  self.modelDict[modelKey] = []
			  self.modelBias[modelKey] = model['bias']
			  
			  # Precalculate for faster retrieval
			  for predPos in model.keys():
				if predPos == 'bias':
				  continue
				self.modelDict[modelKey].append({})
				for aaCode in "ACDEFGHIKLMNPQRSTVWXY-":
				  self.modelDict[modelKey][-1][aaCode] = 0.0
				  
				for (correction,aaCodes) in model[predPos]:
				  for aaCode in aaCodes:
					self.modelDict[modelKey][-1][aaCode] += correction
			  
			  modelLoaded = True
		
		return modelLoaded

	def predictSeqs(self, seqList,jobdir, dataType_tuo='backbone',force_fileid="", force_windowsize=None, overwrite=False):
		
		self.outputfilenames = []
		predictions = {}
		
		count = 0 
		#print seqList   
		seqs = {}		
		for seq in seqList:
			aas = []
			for aa in seq:
				aas.append([aa, -1])        
			seqs[count] = aas
			count +=1	
		#print seqs	
		#origdir = os.getcwd()
		#os.chdir(jobdir)
		count_short = 0

		idlist_ = sorted(seqs.iterkeys())

		# if not self.checkFilesExist(idlist_, overwrite, force_fileid=force_fileid):

		for id_ in idlist_:
			#print 'Predicting %s...' % (id_),
			s = seqs[id_]

			if force_windowsize:
				self.windowsize = force_windowsize
			else:
			  if len(s) > 51:
				  self.windowsize = 51
			  elif len(s) >= 25:
				  self.windowsize = 25
			  elif len(s) == 5:
				  self.windowsize = 5
			  elif len(s) < 5:
				  self.windowsize = 5
				  count_short += 1
				  print 'skipping too short.'
				  continue
			  else:
				  if len(s) % 2 == 0:
					  self.windowsize = len(s) - 1
				  else:
					  self.windowsize = len(s)

			################################
			# Backbone dynamics prediction #
			################################
			if self.backbone:
				if dataType_tuo=='backbone':
					self.checkReadModel(type_='backbone')
					preds = self.computePredictions(id_, s,'backbone')
					c=[]
					for p in preds:
						c+=[p[1]]
					
					return c
				#self.writePredictions(id_, preds, type_='backbone', force_fileid=force_fileid)

			#############################################
			# Secondary Structure Population prediction #
			#############################################
			if self.ssp:
				if dataType_tuo!='sidechain':
					#print 'data',dataType_tuo
					self.checkReadModel(type_=dataType_tuo)
					preds = self.computePredictions(id_, s, dataType_tuo)
					
					c=[]
					for p in preds:
						c+=[p[1]]
					
					return c
					#self.writePredictions(id_, preds, type_=sse, force_fileid=force_fileid)

			##################################
			# Side-chain dynamics prediction #
			##################################
			if self.extended:
				if dataType_tuo=='sidechain':
				  self.checkReadModel(type_='sidechain')
				  preds = self.computePredictions(id_, s, 'sidechain')
				 # print preds
				  #self.writePredictions(id_, preds, type_='sidechain', force_fileid=force_fileid)
				  c=[]
				  for p in preds:
					c+=[p[1]]
				  return c
				  
				  
				
			#########################
			# Plotting and cleaning #
			#########################
			if not self.light:
				self.makePlot(id_)
			else:
				if self.input:
					cmd = "rm *.txt *.arff"
					p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
					for line in p.stdout.readlines():
						print line,
					retval = p.wait()			
		return predictions

	def predictFasta(self, fastafilename, jobdir, force_fileid="", force_windowsize=None, overwrite=False):

		self.outputfilenames = []
		predictions = {}
		seqs = getFastaInputsequences(fastafilename)
		#origdir = os.getcwd()
		#os.chdir(jobdir)
		count_short = 0

		idlist_ = sorted(seqs.iterkeys())

		# if not self.checkFilesExist(idlist_, overwrite, force_fileid=force_fileid):

		for id_ in idlist_:
			#print 'Predicting %s...' % (id_),
			s = seqs[id_]

			if force_windowsize:
				self.windowsize = force_windowsize
			else:
			  if len(s) > 51:
				  self.windowsize = 51
			  elif len(s) >= 25:
				  self.windowsize = 25
			  elif len(s) == 5:
				  self.windowsize = 5
			  elif len(s) < 5:
				  self.windowsize = 5
				  count_short += 1
				  print 'skipping too short.'
				  continue
			  else:
				  if len(s) % 2 == 0:
					  self.windowsize = len(s) - 1
				  else:
					  self.windowsize = len(s)

			################################
			# Backbone dynamics prediction #
			################################
			if self.backbone:
				self.checkReadModel(type_='backbone')
				preds = self.computePredictions(id_, s,'backbone')
				predictions[id_] = preds
				#self.writePredictions(id_, preds, type_='backbone', force_fileid=force_fileid)

			#############################################
			# Secondary Structure Population prediction #
			#############################################
			if self.ssp:
				predictions[id_]={}
				for sse in ['coil', 'helix', 'sheet']:
					self.checkReadModel(type_=sse)
					preds = self.computePredictions(id_, s, sse)
					if not predictions.has_key(id_):
						predictions[id_] = [preds]
					#predictions[id_] += [preds]
					predictions[id_][sse]=preds
					#print 'culo',preds
					#self.writePredictions(id_, preds, type_=sse, force_fileid=force_fileid)

			##################################
			# Side-chain dynamics prediction #
			##################################
			if self.extended:
			  self.checkReadModel(type_='sidechain')
			  preds = self.computePredictions(id_, s, 'sidechain')
			  #self.writePredictions(id_, preds, type_='sidechain', force_fileid=force_fileid)

			#########################
			# Plotting and cleaning #
			#########################
			if not self.light:
				self.makePlot(id_)
			else:
				if self.input:
					cmd = "rm *.txt *.arff"
					p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
					for line in p.stdout.readlines():
						print line,
					retval = p.wait()			
		return predictions
	
	def formatInput(self, id_, s):
		self.input = True
		printheader(self.windowsize, 'dataset.arff')
		getinputsegments(id_, s, self.windowsize, 'dataset.arff')

	def runPredictor(self, id_, type_='backbone'):
		cmd = "%s -T dataset.arff -l %s/data%d.model -p 1-%d > predictions_%s_%s.txt" % (config.javacmd, os.path.join(config.model_path, type_), self.windowsize, self.windowsize, id_, type_)
		p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		for line in p.stdout.readlines():
			print line,
		retval = p.wait()

	def getPredictions(self, id_, s, type_='backbone'):
		"""
		Returns a list of lists (one for each amino acid in the sequence) containing two strings ['aa type one-letter code', 'prediction']
		"""
		predictions = readPredictions('predictions_%s_%s.txt' % (id_, type_))
		preds = getinputsegments_preds(id_, s, self.windowsize, predictions)
		return preds

	def computePredictions(self, id_, s, type_):
		"""
		Returns a list of lists (one for each amino acid in the sequence) containing two strings ['aa type one-letter code', 'prediction']
		"""
		
		# Wim speedup attempt
		emptyTailLen = ((self.windowsize - 1) / 2)
		
		s_app = correctFragment("-" * emptyTailLen + "".join([x[0] for x in s]) + "-" * emptyTailLen)
		#print s_app
		
		modelKey = (type_,self.windowsize)

		seqLen = len(s_app)
		preds = []
		for seqPos in range(seqLen):
		  preds.append([s_app[seqPos],self.modelBias[modelKey]])
		
		for seqPos in range(seqLen):

		  aaCode = s_app[seqPos]
		  
		  for otherSeqPos in range(max(0,seqPos - emptyTailLen),min(seqLen,seqPos+emptyTailLen+1)):
			predPos = emptyTailLen - (otherSeqPos - seqPos)        
				
			preds[otherSeqPos][1] += self.modelDict[modelKey][predPos][aaCode]
			
		return preds[emptyTailLen:-emptyTailLen]
		
	def getfilename(self,id_,force_fileid,type_):

		if self.allinone:
			if force_fileid:
			  filename = '%s_%s.pred' % (force_fileid,type_)
			else:
			  filename = '%s.pred' % (type_)
		else:
			filename = '%s_%s.pred' % (id_, type_)
			
		return filename
		
	def writePredictions(self, id_, preds, type_='backbone', force_fileid=""):

		filename = self.getfilename(id_,force_fileid,type_)

		if self.allinone:
			f = open(filename, 'a')
		else:
			f = open(filename, 'w')
		
		# Track filenames for easy recovery
		if filename not in self.outputfilenames:
			self.outputfilenames.append(filename)   
		 
		f.write('*********************************************\n\
	* Predictions generated by DynaMine (%s)   *\n\
	* http://dynamine.ibsquare.be               *\n\
	*                                           *\n\
	* for %s\n\
	* on %s\n\
	*                                           *\n\
	* If you use these data please cite:        *\n\
	* - doi: 10.1038/ncomms3741 (2013)          *\n\
	* - doi: 10.1093/nar/gku270 (2014)          *\n\
	*********************************************\n' % (self.version, id_, datetime.now()))
		for e in preds:
			f.write("{}\t{:5.3f}\n".format(e[0],e[1]))
		f.close()

	def checkFilesExist(self, idlist_, overwrite, force_fileid=None):
		
		if self.backbone:
			types_= ['backbone']
		elif self.ssp:
			types_= ['coil', 'helix', 'sheet']
		elif self.extended:
			types_=['sidechain']

		for type_ in types_:
			if self.allinone:

				filename = self.getfilename(None,force_fileid,type_)
				if os.path.exists(filename) and filename not in self.outputfilenames:
					self.outputfilenames.append(filename)

					# Have to delete otherwise will overwrite endlessly!
					if overwrite:
						os.remove(filename)
				  
			else:
				for id_ in idlist_:
					filename = self.getfilename(id_,force_fileid,type_)
					if os.path.exists(filename) and filename not in self.outputfilenames:
						self.outputfilenames.append(filename)
		 
		if self.outputfilenames and not overwrite:
			filesExist = True
		else:
			filesExist = False

		return filesExist 
		
	def makePlot(self, id_):
		if self.backbone:
			backbone_pred_file = '%s_backbone.pred' % (id_)
			prot = backbone_pred_file.rsplit('.', 1)[0].rsplit('_', 1)[0]
			plot = MakeDynaMinePlot()
			plot.readPredictionData(backbone_pred_file)
			plot.addPredictionData('backbone')
			plot.writePlot(prot+'_plot.png', 'DynaMine predictions for %s' % (prot), default_colour='#0EBC5F', addBioAnnotation=True)
			plot.writePlot(prot+'_plot.eps', 'DynaMine predictions for %s' % (prot), default_colour='#0EBC5F', addBioAnnotation=True)
		if self.extended:
			sidechain_pred_file = '%s_sidechain.pred' % (id_)
			prot = sidechain_pred_file.rsplit('.', 1)[0]
			plot = MakeDynaMinePlot()
			plot.readPredictionData(sidechain_pred_file)
			plot.addPredictionData('sidechain')
			plot.writePlot(prot+'_plot.png', 'DynaMine predictions for %s' % (prot), default_colour='#196CE8', addBioAnnotation=True)
			plot.writePlot(prot+'_plot.eps', 'DynaMine predictions for %s' % (prot), default_colour='#196CE8', addBioAnnotation=True)
		if self.ssp:
			plot = MakeDynaMinePlot()
			prot = id_
			for sse in ['coil', 'helix', 'sheet']:
				sse_pred_file = '%s_%s.pred' % (id_, sse)
				plot.readPredictionData(sse_pred_file)
				if sse == 'coil':
					plot.addPredictionData('coil')
				else:
					plot.addOtherData(sse, plot.predictionData.keys(), plot.predictionData.values())
			plot.writePlot(prot+'_sse_plot.png', 'DynaMine predictions for %s' % (prot), ylim=(0,1), addBioAnnotation=True)
			plot.writePlot(prot+'_sse_plot.eps', 'DynaMine predictions for %s' % (prot), ylim=(0,1), addBioAnnotation=True)
			#plot.writePlot(prot+'_plot.pdf', 'DynaMine predictions for %s' % (prot), ylim=(0,1), addBioAnnotation=True)


class ExtendedDynaMine(_DynaMineGeneric):
    def __init__(self, ssp_, light_, out):
        _DynaMineGeneric.__init__(self, ssp=ssp_, light=light_, extended=True, allinoneout=out)


class DynaMine(_DynaMineGeneric):
    def __init__(self, ssp_, light_, out, backbone):
        _DynaMineGeneric.__init__(self, ssp=ssp_, light=light_, extended=False, allinoneout=out, backbone=backbone)
