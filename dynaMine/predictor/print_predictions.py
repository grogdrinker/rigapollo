#!/usr/bin/env python
# encoding: utf-8
"""
print_predictions.py

Created by Elisa Cilia on 2013-01-14.
Copyright (c) 2013 Elisa Cilia. All rights reserved.
"""


import math
from utils import *


def readPredictions(filename):
    predictions = {}
    f = open(filename, 'r')
    for l in [x.strip().split() for x in f.readlines() if x.endswith(')\n') and 'inst' not in x]:
        predictions[l[-1].strip('(').strip(')').replace(',', '')] = l[2]  # [float(n) for n in l[1:4]]
    f.close()
    return predictions


def getinputsegments_preds(id_, s, windowsize, predictions):
    pred = []
    if len(s) >= windowsize:
        s_app = "-" * ((windowsize - 1) / 2) + "".join([x[0] for x in s]) + "-" * ((windowsize - 1) / 2)
        #print s_app
        for i in range(len(s_app) - windowsize + 1):
            seq = correctFragment(s_app[i:i + windowsize])
            #print s_app[i + int(math.floor(windowsize / 2))]
            #print seq, predictions[seq]
            pred.append([s_app[i + int(math.floor(windowsize / 2))], predictions[seq]])
    return pred
