#!/usr/bin/env python
# encoding: utf-8
"""
produce_input.py

Created by Elisa Cilia on 2012-07-27.
Copyright (c) 2012 Elisa Cilia. All rights reserved.
"""

import config
import utils

def readFASTA(seqFile):
	ifp = open(seqFile, "r")
	sl = []
	i = 0
	line = ifp.readline()
	while len(line) != 0:
		if line[0] == '>':                      
			sl.append([line.strip().replace("|","-").replace(">","")[:12],""])
			line = ifp.readline()
			while len(line) > 0 and line[0] != '>':
					sl[i][1] = sl[i][1] + line.strip()
					line = ifp.readline()
			i = i + 1
		else:
			raise Exception("Syntax error in the fasta file at line %d!" % i)       
	return sl

def getFastaInputsequences(filename):    
    seqs = {}
    sequences = readFASTA(filename)
    #print sequences
    for seq in sequences:
        aas = []
        for aa in seq[1]:
            aas.append([aa, -1])        
        seqs[seq[0]] = aas
    #print seqs
   # raw_input()
	#print seqs
    return seqs


def getinputsegments(id_, s, windowsize, outputfile):
    dataset = {}
    mapping = {}
    if len(s) >= windowsize:
        s_app = "-" * ((windowsize - 1) / 2) + "".join([x[0] for x in s]) + "-" * ((windowsize - 1) / 2)
        for i in range(len(s_app) - windowsize + 1):
            seq = ''
            for j in s_app[i:i + windowsize]:
                if not j[0] in config.aas:
                    seq += 'X,'
                else:
                    seq += j[0] + ','
            value = s[i][1]
            #if not (seq[0] == '-' and seq[-2] == '-'):
            mapping[seq] = [id_, str(i + 1), s[i][0]]
            if seq in dataset:
                dataset[seq].append(value)
            else:
                dataset[seq] = [value]
    else:
        print "Skipping %s. The sequence is too short!" % (id_)
    f = open(outputfile, 'a')
    fd = open('targets.txt', 'w')
    fdm = open('map.txt', 'w')
    for frag, values in dataset.iteritems():
        f.write(frag + '?\n')
        fdm.write("\t".join(mapping[frag]) + '\t' + '?\t' + frag[:-1] + '\n')
        fd.write(frag + ",".join([str(x) for x in values]) + '\n')
    f.close()
    fd.close()
    fdm.close()


def printheader(windowsize, outputfile, task='regression'):
    f = open(outputfile, 'w')
    f.write('@RELATION disorder\n\n')
    if task == 'classification':
        for i in range(windowsize):
            f.write('@ATTRIBUTE ' + 'aainpos%d ' % (i) + '{A,C,D,E,F,G,H,I,K,L,M,N,P,Q,R,S,T,V,W,Y,X}\n')
        for i in range(windowsize):
            f.write('@ATTRIBUTE ' + 'rciinpos%d ' % (i) + 'NUMERIC\n')
        f.write('@ATTRIBUTE class {-1,+1}\n\n')
    else:
        for i in range(windowsize):
            f.write('@ATTRIBUTE ' + 'aainpos%d ' % (i) + '{-,A,C,D,E,F,G,H,I,K,L,M,N,P,Q,R,S,T,V,W,Y,X}\n')
        f.write('@ATTRIBUTE rci NUMERIC\n\n')
    f.write('@DATA\n')
    f.close()
