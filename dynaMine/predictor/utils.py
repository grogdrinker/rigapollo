#!/usr/bin/env python
# encoding: utf-8
"""
utils.py

Created by Elisa Cilia on 2014-08-14.
Copyright (c) 2014 Elisa Cilia. All rights reserved.
"""

import config


def correctFragment(frag):
    seq = ''
    for j in frag:
        if not j[0] in config.aas:
            seq += 'X'
        else:
            seq += j[0]
    return seq