import os

#from dynaMine.predictor.predictor import _DynaMineGeneric
from predictor import _DynaMineGeneric
class RunDynaMinePrediction:
  
  """

  This version for speed; does not use FC but custom parser. No dictionaries either, just lists.
  Only does one prediction at a time, and produces by default a single output file.

  """
  
  dynaMineResultsDir = 'dynaMineResults'

  def runDynaMineFasta(self,fastaFileName,identifier,dataType='backbone',windowSize=None,workDir=None,overWrite=False,singleOutputFiles=True): # ,predType='general' can add to do per amino acid predictions!
    
    """
    Run DynaMine locally, now fast enough
    
    dataTypes = ('backbone','secStruc','sidechain')
    predTypes = ('general','perAminoAcid')  
    """        
    extended=False
    backbone=False
    secStrucPred=False
    if dataType == 'sidechain':
      extended = True
    else:
      if dataType == 'backbone':
        backbone = True
      else:
        secStrucPred = True

    if windowSize:
      identifier += "_{}".format(windowSize)

    dynaMine = _DynaMineGeneric(ssp=secStrucPred, light=True, extended=extended, allinoneout=singleOutputFiles, backbone=backbone)

    return dynaMine.predictFasta(fastaFileName, workDir, force_fileid=identifier, force_windowsize=windowSize,overwrite=overWrite) 
    
    
  def runDynaMineSeqs(self,seqs,identifier,dataType='backbone',windowSize=None,workDir=None,overWrite=False,singleOutputFiles=True): # ,predType='general' can add to do per amino acid predictions!
    
    """
    Run DynaMine locally, now fast enough
    
    dataTypes = ('backbone','secStruc','sidechain')
    predTypes = ('general','perAminoAcid')  
    """        
    extended=False
    backbone=False
    secStrucPred=False
    if dataType == 'sidechain':
      extended = True
    else:
      if dataType == 'backbone':
        backbone = True
      else:
        secStrucPred = True

    if windowSize:
      identifier += "_{}".format(windowSize)

    dynaMine = _DynaMineGeneric(ssp=secStrucPred, light=True, extended=extended, allinoneout=singleOutputFiles, backbone=backbone)
    if type(seqs) == list:
	  return dynaMine.predictSeqs(seqs, workDir, force_fileid=identifier, force_windowsize=windowSize,overwrite=overWrite)    
    elif type(seqs) == str:
	 # print dataType
	  return dynaMine.predictSeqs([seqs], workDir, dataType_tuo=dataType, force_fileid=identifier, force_windowsize=windowSize,overwrite=overWrite)

  def runDynaMineAll(self,fastaFileName,identifier,dataTypes=('backbone','secStruc','sidechain'),windowSize=None,workDir=None,overWrite=False,separateSsElements=False,singleOutputFiles=True): # ,predType='general' can add to do per amino acid predictions!

    fileNames = {}

    for dataType in dataTypes:
      
      # Note: secStruc will give back helix, coil and sheet files!
      outputFileNames = self.runDynaMineFasta(fastaFileName,identifier,dataType=dataType,windowSize=windowSize,workDir=workDir,overWrite=overWrite,singleOutputFiles=singleOutputFiles)
      
      if dataType == 'secStruc' and separateSsElements:
        for ssType in ('helix','coil','sheet'):
          fileNames[ssType] = []
          for outputFileName in outputFileNames:
            if outputFileName.endswith("{}.pred".format(ssType)):
              fileNames[ssType].append(outputFileName)
      else:
        fileNames[dataType] = outputFileNames
  
    return fileNames
#D = RunDynaMinePrediction()
#d= D.runDynaMineSeqs('AAACCCAAACCLLLL','2',dataType='sidechain')
#print d,len('AAACCCAAACCLLLL')
