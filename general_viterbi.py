#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  general_viterbi.py
#  
#  Copyright 2015 Unknown <scimmia@scimmia_host>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import numpy
from utils import topol,reverse_topol
from  math import log
verbosity=0
def fai_mat(l,m,val):
	tot=[]
	a=[val]*m
	for i in range(l):
		tot+=[a[:]]
	return tot
	
def general_viterbi(s1,s2,trans,emis):
	topo=topol()
	rev_topol=reverse_topol(topo)
	########################
	#   matrix definition  #
	########################
	mat={}
	mat_path={}
	mat_stato={}
	for i in topo.keys():
		if i!='BEGIN':
			mat[i]=fai_mat(len(s1)+1,len(s2)+1,None)
			mat_path[i]=fai_mat(len(s1)+1,len(s2)+1,None)
			mat_stato[i]=fai_mat(len(s1)+1,len(s2)+1,None)
		
	########################
	#    initialiation     #
	########################
	for k in mat.keys():
		#mat_path[k][0][0]='BEGIN'
		if not topo[k].i and topo[k].j:
			if trans['BEGIN'].has_key(k):
				mat[k][0][0]=0
				mat[k][0][1]=trans['BEGIN'][k]+emis[k][0] 
				mat_path[k][0][1]=('BEGIN',0,0)
				mat_stato[k][0][1]=k
			else:
				mat[k][0][1]=-float('inf')
			for i in range(2,len(s2)+1):
				mat[k][0][i]=trans[k][k]+mat[k][0][i-1]+emis[k][i-1] 
				mat_stato[k][0][i]=k
				mat_path[k][0][i]=(k,0,i-1)
				
		elif topo[k].i and not topo[k].j:
			
			if trans['BEGIN'].has_key(k):
				mat[k][0][0]=0
				mat[k][1][0]=trans['BEGIN'][k]+emis[k][0]
				mat_path[k][1][0]=('BEGIN',0,0)
				mat_stato[k][1][0]=k
			else:
				mat[k][1][0]=-float('inf')
			for i in range(2,len(s1)+1):
				mat[k][i][0]=trans[k][k]+mat[k][i-1][0]+emis[k][i-1] 
				mat_stato[k][i][0]=k
				mat_path[k][i][0]=(k,i-1,0)
		elif topo[k].j and topo[k].i:
			
			mat[k][0][0]=0
	for i in mat_stato.keys():
		mat_stato[i][0][0]='BEGIN'
		
	########################
	#      real algorithm  #
	########################
	
	#return
	for i in range(1,len(s1)+1):
		for j in range(1,len(s2)+1):
			if verbosity==float('inf'):
							print 'inizio',i,j
			for katt in mat.keys():
				best=None
				score=-float('inf')
				for kold in rev_topol[katt]:
					
					if not trans[kold].has_key(katt):
						if verbosity==float('inf'):
							print 'transizione inesistente tra',kold,katt
						continue
					if topo[katt].i and not topo[katt].j:
						if kold!='BEGIN':				
							temp=mat[kold][i-1][j]
						elif i-1==0 and j==0:
							temp=0.0
						else:
							temp=None
						if temp==None:
							if verbosity==float('inf'):
								print '1mai stato in',kold,i-1,j
							continue
						state_temp=kold
					
						meglio=temp+trans[kold][katt]+emis[katt][i-1]
					elif not topo[katt].i and topo[katt].j:
						if kold!='BEGIN':
							temp=mat[kold][i][j-1]
						elif i==0 and j-1<=0:
							temp=0.0
						else:
							temp=None
							
						if temp==None:
							if verbosity==float('inf'):
								print '2mai stato in',kold,i,j-1
							continue
							
						state_temp=kold

						meglio=temp+trans[kold][katt]+emis[katt][j-1]

					else:
						if kold!='BEGIN':
							temp=mat[kold][i-1][j-1] ### here if you want to add silent states
						elif i-1<=0 and j-1<=0:
							temp=0.0
						else:
							temp=None
						
						if temp==None:
							if verbosity==float('inf'):
								print '3mai stato in',kold,i-1,j-1
							continue
						state_temp=kold
						meglio=temp+trans[kold][katt]+emis[katt][(i-1,j-1)]

					if meglio>score:
						score=meglio
						best=state_temp
				if topo[katt].i and not topo[katt].j:
					mat[katt][i][j]=score
					if (i-1,j)==(0,0):
						best='BEGIN'
					mat_path[katt][i][j]=(best,i-1,j)
					mat_stato[katt][i][j]=best
				elif not topo[katt].i and topo[katt].j:
					mat[katt][i][j]=score
					if (i,j-1)==(0,0):
						best='BEGIN'
					mat_path[katt][i][j]=(best,i,j-1)
					mat_stato[katt][i][j]=best
				else:
					mat[katt][i][j]=score
					if (i-1,j-1)==(0,0):
						best='BEGIN'
					mat_path[katt][i][j]=(best,i-1,j-1)
					mat_stato[katt][i][j]=best
	############################
	#      END STATE           #
	############################
	#print mat_path['IS1'][1]
	be=None
	for k in rev_topol['END']:
		if not trans[mat_stato[k][i][j]].has_key('END'):
			continue
		a=mat[k][i][j]+trans[mat_stato[k][i][j]]['END']
		#print k,a
		if be==None:
			be=a
			st=k
		if a>be:
			be=a
			st=k
	final=st #FINAL STATE
	final_score=be # FINAL SCORE
	
	pa=path_recon((st,i,j),mat_path,mat)
	s1a,s2a=allin(pa,topo,s1,s2)
	
	return final_score,s1a,s2a
def allineo_rozzo(best,mat_path,mat,s1,s2,topo):
	path=['END']
	g=best
	
	j=g[2]
	i=g[1]
	p=g[0]
	s1a=''
	s2a=''
	attuale=p
	while ((j!=1 or i!=1) and  g!=-float('inf')):
		p=g[0]
		i=g[1]
		j=g[2]
		path+=[attuale]
		if topo[attuale].i and topo[attuale].j:
			s1a+=s1[i-1]
			s2a+=s2[j-1]
		elif not topo[attuale].i and topo[attuale].j:
			s1a+='-'
			s2a+=s2[j-1]
		elif topo[attuale].i and not topo[attuale].j:
			s1a+=s1[i-1]
			s2a+='-'
		g=mat_path[p][i][j]
		attuale=p
	if -float('inf') in path:
		path.remove(-float('inf'))
	if 'BEGIN' in path:
		path.remove('BEGIN')
	if 'END' in path:
		path.remove('END')
	#print len(path)
	path.reverse()
	return s1a[::-1],s2a[::-1]
def path_recon(best,mat_path,mat):
	path=[]
	g=best
	j=g[2]
	i=g[1]
	p=g[0]
	attuale='END'
	
	while p!='BEGIN':
		#print p,i,j,'ora'
		path+=[attuale]
		attuale=p
		g=mat_path[p][i][j]
		p=g[0]
		i=g[1]
		j=g[2]
		#print p,i,j,'prossimo'
	path+=[attuale]
	path.remove('END')
	path.reverse()
	return path
def allin(p,topo,s1,s2):
	i=0
	j=0
	s1a=''
	s2a=''

	for k in p:
	
		if topo[k].j and topo[k].i:
			s1a+=s1[i]
			s2a+=s2[j]
			i+=1
			j+=1
		
		elif topo[k].i:
			s1a+=s1[i]
			s2a+='-'
			i+=1
		elif topo[k].j:
			s2a+=s2[j]
			s1a+='-'
			j+=1
	return s1a,s2a
def main(args):
	s1='AACA'
	s2='AACAA'
	stati=['M','I1','I2']
	emiss={}
	from vettore import fai_vettore_tuo
	for k in stati:
		emiss[k]={}
		for i in range(len(s1)):
			for j in range(len(s2)):
				if k=='M' and s1[i]==s2[j]:
					emiss[k][(i,j)]=log(0.8)
				elif k=='M':
					emiss[k][(i,j)]=log(0.2)
				elif i!=j:
					emiss[k][(i,j)]=log(0.8)
				else:
					emiss[k][(i,j)]=log(0.2)
	trans={'I1': {'M': log(0.8),'I1':log(0.1),'I2':log(0.1),'END':log(0.1)}, 'I2': {'END':log(0.1),'I1': log(0.4),'I2':log(0.1),'M':log(0.5)},'M': {'END':log(0.1),'I1': log(0.4),'I2':log(0.1),'M':log(0.8)},'BEGIN': {'M': log(0.8),'I1':log(0.1),'I2':log(0.1),'END':log(0.1)}}	
	general_viterbi(s1,s2,trans,emiss)
	return 0

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
