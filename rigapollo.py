#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  rigapollo.py
#  
#  Copyright 2016 Gabriele Orlando <orlando.gabriele89@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from general_viterbi import general_viterbi
from multiprocessing import Pool
from utils import *
from math import log
from build_vector import buildVector,compare
import numpy as np
import pickle
class rigapollo:
	def __init__(self,PARAMS):
		self.nodes={}
		self.topo=topol()
		self.TYPE_MATCH=PARAMS[0]
		self.TYPE_GAP=PARAMS[1]
		self.SW_MATCH=PARAMS[2]
		self.SW_GAP=PARAMS[3]
		self.CPU=1
		self.model_gap,self.model,start_trans,self.trans=pickle.load(open('models/main_model.m','r'))
		return
	def predict(self,s1,s2,aln1=None,aln2=None):
		def paral(inpu):
			k,vet1,vet2,model=inpu
			X=[]
			for k1 in k:
				if k1>=len(vet1):
					continue
				for l in range(len(vet2)):
					X+=[compare(vet1[k1],vet2[l])]
			for i in X:
				#print len(i)
				if not len(i)==len(X[0]):
					raw_input()
			v=model.predict_proba(X)
			cont=0
			r=[]
			for k1 in k:
				if k1>=len(vet1):
					continue
				for l in range(len(vet2)):
					r+=[(k1,l,log(v[cont][1]))]
					cont+=1
			return r
		print 'building vectors'
		v1=buildVector(s1,self.TYPE_MATCH,self.SW_MATCH,args={'align_file':aln1})
		v2=buildVector(s2,self.TYPE_MATCH,self.SW_MATCH,args={'align_file':aln2})
		v1GAP=buildVector(s1,self.TYPE_GAP,self.SW_GAP,args={'align_file':aln1})
		v2GAP=buildVector(s2,self.TYPE_GAP,self.SW_GAP,args={'align_file':aln2})
		print '\tdone'
		x=[]
		states=self.trans.keys()
		states.remove('BEGIN')
		pool = Pool(processes=self.CPU)
		X=[]
		step=len(s1)/self.CPU
		print 'predicting svm,cpu=',self.CPU	
		if self.CPU>1:
			raise(NotImplementedError,"for next commit will provide a multicore version")
			if len(s1)%self.CPU!=0:
				step+=1
			inp=[]
			for k in range(0,len(s1),step):
				inp+=[(range(k,k+step),v1,v2, self.model)]
			#print inp[0]
			result_list=pool.map(paral,inp)
			pool.close()
			pool.join()
		else:
			result_list=[]
			inp=(range(len(s1)),v1,v2, self.model)
			result_list=[paral(inp)]
		print '\tdone'
		print 'building mat'
		mat_tot=np.zeros((len(s1),len(s2)))
		for p in result_list:
			for q in p:
				mat_tot[q[0]][q[1]]=q[2]
		pair_emis={}
		p=0
		g1svm=self.model_gap.predict_log_proba(v1GAP)
		g2svm=self.model_gap.predict_log_proba(v2GAP)
		for state in states:
				pair_emis[state]={}
		for i in range(len(v1)):
			for j in range(len(v2)):
				for state in states:
					if self.topo[state].i and self.topo[state].j:
						pair_emis[state][(i,j)]=mat_tot[i][j]
					elif self.topo[state].i:
						pair_emis[state][i]=g1svm[i][1]
					elif self.topo[state].j:
						pair_emis[state][j]=g2svm[j][1]
		print '\tdone,starting viterbi'
		score,s1a,s2a=general_viterbi(s1,s2,self.trans,pair_emis)
		return score,s1a,s2a
def trim(aln):
	a=open(aln,'r').readlines()
	for i in range(len(a)):
		a[i]=list(a[i])
	a1=np.array(a)
	a=a1.transpose()
	#assert a!=a1
	gg=[]
	for i in a:
		#print i
		#raw_input()
		if i[0]!='-':
			gg+=[i]
	a=np.array(gg).transpose()
	f=open(aln+'_trimmed','w')
	for i in a:
		f.write(''.join(list(i)))
	f.close()
	return ''.join(list(a[0])).strip()
def standalone(a1,a2):
	aligner=rigapollo(['pssm,dyna_coil,dyna_helix,dyna_sheet,plosone_ind','pssm',3,3])
	f=open(a1,'r')
	s1=f.readline().strip('\n')
	f.close()
	if '-' in s1:
		s1=trim(a1)
		a1=a1+'_trimmed'
	f=open(a2,'r')
	s2=f.readline().strip('\n')
	f.close()
	if '-' in s2:
		s2=trim(a2)
		a2=a2+'_trimmed'
	score,a1,a2=aligner.predict(s1,s2,a1,a2)
	return score,a1,a2
def main(args):
	outfile=None
	if len(args)<2:
		print 'ERROR, no inputs. try python rigapollo.py --help'
		return
	for i in range(1,len(args)):
		if args[i]=='-o':
			outfile=args[i+1]
		if args[i]=='--help' or args[i]=='-h':
			print 'usage: python rigapollo.py path_to_hmmer_align1 path_to_align2 [options]'
			print 'currently implementes options:'
			print '-o output file name'
			print 'the alignments must be provided in compact format (see input_example folder)'
			return
	print 'STARTING THE ALIGNMENT!'
	score,a1,a2=standalone(a1=args[1],a2=args[2])
	if outfile==None:
		print '############# OUTPUT ON SCREEN ##############'
		print 'Viterbi log probability of the alignment',score
		print 'sequence 1:'
		print a1
		print 'sequence 2:'
		print a2
	else:
		f=open(outfile,'w')
		f.write('>seqence1\n'+a1+'\n>seqence2\n'+a2)

		print 'everything ok'
		print 'output written in ',outfile
		print 'the monkeys are listening...'
		


if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
