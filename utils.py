#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  utils.py
#  
#  Copyright 2015 gabriele <gabriele@stud9>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import math,os
def fai_hidden(s1,s2):
	hidden=[]
	s1c=''
	s2c=''
	for i in xrange(len(s1)):

		if (s1[i]!='-' and s2[i]=='-'):
			if s2[:i+1].strip('-')=='':
				hidden+=['IS_1']
			elif s2[i:].strip('-')=='':
				hidden+=['IF_1']
			else:
				hidden+=['I_1']
			s1c+=s1[i]
			s2c+=s2[i]
		elif (s2[i]!='-' and s1[i]=='-'):
			if s1[:i+1].strip('-')=='':
				hidden+=['IS_2']
			elif s1[i:].strip('-')=='':
				hidden+=['IF_2']
			else:
				hidden+=['I_2']
			s1c+=s1[i]
			s2c+=s2[i]
		elif (s2[i]!='-' and s1[i]!='-'):
			hidden+=['M']
			s1c+=s1[i]
			s2c+=s2[i]
	return hidden,s1c,s2c
def leggifasta(database): #legge un file fasta e lo converte in un dizionario
	f=open(database)
	uniprot=f.readlines()
	f.close()
	dizio={}
	for i in uniprot:
		#c=re.match(">(\w+)",i)  4
	
		if i[0]=='>':
				uniprotid=i.strip('>\n')
				dizio[uniprotid]=''
		else:
			dizio[uniprotid]=dizio[uniprotid]+i.strip('\n')
	return dizio
def leggi_clustal(fil):
	f=open(fil,'r')
	diz={}
	for i in f.readlines():
		if i.strip()=='' or 'CLUSTAL format multiple sequence ' in i:
			continue
		a=i.split()
		if diz.has_key(a[0]):
			diz[a[0]]+=a[1].strip()
		else:
			diz[a[0]]=a[1].strip()

	return diz		
		
def fai_hidden_SIMPLE(s1,s2):
	hidden=[]
	s1c=''
	s2c=''
	assert(len(s1))==len(s2)
	for i in xrange(len(s1)):
		if (s1[i]=='-' and s2[i]=='-'):
			continue
		if (s1[i]!='-' and s2[i]=='-'):
			if s2[:i+1].strip('-')=='':
				hidden+=['I_1']
			elif s2[i:].strip('-')=='':
				hidden+=['I_1']
			else:
				hidden+=['I_1']
			s1c+=s1[i]
			s2c+=s2[i]
		elif (s2[i]!='-' and s1[i]=='-'):
			if s1[:i+1].strip('-')=='':
				hidden+=['I_2']
			elif s1[i:].strip('-')=='':
				hidden+=['I_2']
			else:
				hidden+=['I_2']
			s1c+=s1[i]
			s2c+=s2[i]
		elif (s2[i]!='-' and s1[i]!='-'):
			hidden+=['M']
			s1c+=s1[i]
			s2c+=s2[i]
	return hidden,s1c,s2c

def fai_hidden_IFIS(s1,s2):
	hidden=[]
	s1c=''
	s2c=''
	assert(len(s1))==len(s2)
	for i in xrange(len(s1)):
		if (s1[i]=='-' and s2[i]=='-'):
			continue
		if (s1[i]!='-' and s2[i]=='-'):
			if s2[:i+1].strip('-')=='':
				hidden+=['I1']
			elif s2[i:].strip('-')=='':
				hidden+=['I1']
			else:
				hidden+=['I1']
			s1c+=s1[i]
			s2c+=s2[i]
		elif (s2[i]!='-' and s1[i]=='-'):
			if s1[:i+1].strip('-')=='':
				hidden+=['I2']
			elif s1[i:].strip('-')=='':
				hidden+=['I2']
			else:
				hidden+=['I2']
			s1c+=s1[i]
			s2c+=s2[i]
		elif (s2[i]!='-' and s1[i]!='-'):
			hidden+=['M']
			s1c+=s1[i]
			s2c+=s2[i]
	inizio1=True
	inizio2=True
	hidden1=hidden[:]
	for i in range(len(hidden)):
		if inizio1 and  hidden[i]=='I1':
			hidden1[i]='IS1'
		else:
			inizio1=False
		if inizio2 and hidden[i]=='I2':
			hidden1[i]='IS2'
		else:
			inizio2=False
	hidden=hidden1[:]
	inizio1=True
	inizio2=True
	hidden1=hidden[:]
	for i in range(len(hidden)-1,-1,-1):
		if inizio1 and hidden[i]=='I1':
			hidden1[i]='IF1'
		else:
			inizio1=False
		if inizio2 and hidden[i]=='I2':
			hidden1[i]='IF2'
		else:
			inizio2=False
			
	return hidden1,s1c,s2c
def trans_prob(hidden,topo):           ###METTI UN CARATTERE PER DEFINIRE L'END
	mat={}
	start={}
	tot=0.0
	tot_start=0.0
	
	for i in hidden.keys():
		primo=True
		hidden[i]+=['END']
		for j in hidden[i]:
			if primo:
				if not start.has_key(j):
					start[j]=0.0
					if topo[j].coupled!=None:
						for co in topo[j].coupled:
							start[co]=0.0
				if topo[j].coupled!=None:
						for co in topo[j].coupled:
							start[co]+=1.0
							tot_start+=1.0
				start[j]+=1.0
				tot_start+=1.0
				primo=False
				nuovo=j
			else:
				prec=nuovo
				nuovo=j
				if not mat.has_key(prec):
					mat[prec]={}
				if not mat[prec].has_key(nuovo):
					mat[prec][nuovo]=0
				if nuovo!='END' and topo[nuovo].coupled!=None and topo[prec].coupled==None:
					for co in topo[nuovo].coupled:
						if not mat[prec].has_key(co):
							mat[prec][co]=0.0
						mat[prec][co]+=1.0
				mat[prec][nuovo]+=1.0
	somme={}
	for i in mat.keys():
		for j in mat[i].keys():
			if not somme.has_key(i):
				somme[i]=0.0
			somme[i]+=mat[i][j]
	
	for i in mat.keys():
		for j in mat[i].keys():
			mat[i][j]=math.log(mat[i][j]/somme[i])	
	for i in start.keys():
		start[i]=math.log(start[i]/tot_start)
	return start,mat
	

def emiss(hidden,vets):
	
	emiss={}
	for i in hidden.keys():
		if 'END' in hidden[i]:
			hidden[i].remove('END')
		assert len(hidden[i])==len(vets[i])
		for j in range(len(hidden[i])):
			if not emiss.has_key(hidden[i][j]):
				emiss[hidden[i][j]]=[]
				
			emiss[hidden[i][j]]+=[vets[i][j]]
	return emiss
def leggi_contralign(dataset):
	dafare=os.listdir(dataset)
	diz={}
	for i in dafare:
		if '.fasta' in i and len(i.split('.'))==2:
			lis=[]
			f=open(dataset+i)
			lin=f.readlines()
			s=''
			for j in lin:
				if j[0]!='>':
					s+=j.strip('\n')
				else:
					if s!='':
						lis+=[s]
						s=''
			lis+=[s]
			diz[i.replace('.fasta','')]=lis
	return diz
def calculate_SI(dire):
	s=[]
	for i in os.listdir(dire):
		a=leggifasta(dire+'/'+i)
		assert len(a.keys())==2 #solo pairwise è un SI
		nome=a.keys()
		sc=0.0
		tot=0.0
		for k in range(len(a[nome[0]])):
			if a[nome[0]][k]!='-' and a[nome[1]][k]!='-':
				if a[nome[0]][k]==a[nome[1]][k]:
					sc+=1.0
					tot+=1
				else:
					tot+=1
		if tot>0:
			s+=[sc/tot]
		else:
			s+=[0]
	return sum(s)/len(s)
class state:
	def __init__(self,name,i_increases,j_increases,trans,coupled=None):
		self.name=name
		self.i=i_increases
		self.j=j_increases
		self.edge=trans
		self.coupled=coupled
def topol():
	##################################################
	# CHANGE HERE TO DEFINE THE STRUCTURE OF THE HMM #
	#    OUTPUT MUST BE A DICTIONARY WITH ALL THE    #
	#           STATES EXCEPT THE END ONE            #
	##################################################
	topo={}
	topo['M']=state('M',True,True,['M','I1','I2','END','IF1','IF2'])
	topo['I1']=state('I1',True,False,['M','I1','I2'],coupled=['I2'])
	topo['IF1']=state('IF1',True,False,['IF1','END'],coupled=['IF2'])
	topo['IS1']=state('IS1',True,False,['IS1','M'],coupled=['IS2'])
	
	topo['IF2']=state('IF2',False,True,['IF2','END'],coupled=['IF1'])
	topo['IS2']=state('IS2',False,True,['IS2','M'],coupled=['IS1'])
	
	topo['I2']=state('I2',False,True,['M','I1','I2'],coupled=['I1'])
	topo['BEGIN']=state('BEGIN',None,None,['M','IS1','IS2'])
	
	return topo
def reverse_topol(topo):
	rev={}
	for i in topo.keys():
		for j in topo[i].edge:
			if not j in rev.keys():
				rev[j]=[]
			rev[j]+=[i]
	return rev
if __name__ == '__main__':
	marshalla_pssm('datasets/both_hard')
